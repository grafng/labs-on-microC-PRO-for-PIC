
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab20_1.c,4 :: 		void main( )
;Lab20_1.c,6 :: 		UART1_Init(9600);
	PUSH       R2
	PUSH       R3
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;Lab20_1.c,7 :: 		UART1_Write_Text("Hello, student!");                                       // инициализация  UART
	LDI        R27, #lo_addr(?lstr1_Lab20_1+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_Lab20_1+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;Lab20_1.c,9 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
