
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab20_6.c,1 :: 		void  main( )
;Lab20_6.c,6 :: 		UART1_Init(9600);
	PUSH       R2
	PUSH       R3
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;Lab20_6.c,7 :: 		while(1)
L_main0:
;Lab20_6.c,9 :: 		UART1_Write_Text("To receive message press key A");
	LDI        R27, #lo_addr(?lstr1_Lab20_6+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_Lab20_6+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;Lab20_6.c,10 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;Lab20_6.c,11 :: 		Delay_ms(2000);
	LDI        R18, 82
	LDI        R17, 43
	LDI        R16, lo_addr(R0)
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
	NOP
	NOP
	NOP
;Lab20_6.c,13 :: 		if(UART1_Data_Ready( ) == 1)
	CALL       _UART1_Data_Ready+0
	CPI        R16, 1
	BREQ       L__main14
	JMP        L_main4
L__main14:
;Lab20_6.c,15 :: 		symbol = UART1_Read( );
	CALL       _UART1_Read+0
	LDI        R17, 0
	MOV        R18, R17
	MOV        R19, R17
	CALL       _float_ulong2fp+0
; symbol start address is: 20 (R20)
	MOVW       R20, R16
	MOVW       R22, R18
;Lab20_6.c,16 :: 		UART1_Write(symbol);                                 // ���
	PUSH       R23
	PUSH       R22
	PUSH       R21
	PUSH       R20
	CALL       _float_fpint+0
	POP        R20
	POP        R21
	POP        R22
	POP        R23
	MOV        R2, R16
	CALL       _UART1_Write+0
;Lab20_6.c,17 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;Lab20_6.c,18 :: 		if(symbol == 'A' || symbol == 'a')     // ���� ������ �����������
	PUSH       R23
	PUSH       R22
	PUSH       R21
	PUSH       R20
	LDI        R16, 0
	LDI        R17, 0
	LDI        R18, 130
	LDI        R19, 66
	CALL       _float_op_equ+0
	OR         R0, R0
	LDI        R16, 0
	BREQ       L__main15
	LDI        R16, 1
L__main15:
	POP        R20
	POP        R21
	POP        R22
	POP        R23
	TST        R16
	BREQ       L__main16
	JMP        L__main12
L__main16:
	LDI        R16, 0
	LDI        R17, 0
	LDI        R18, 194
	LDI        R19, 66
	CALL       _float_op_equ+0
	OR         R0, R0
	LDI        R16, 0
	BREQ       L__main17
	LDI        R16, 1
L__main17:
; symbol end address is: 20 (R20)
	TST        R16
	BREQ       L__main18
	JMP        L__main11
L__main18:
	JMP        L_main7
L__main12:
L__main11:
;Lab20_6.c,20 :: 		UART1_Write_Text("Hello, student!");
	LDI        R27, #lo_addr(?lstr2_Lab20_6+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr2_Lab20_6+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;Lab20_6.c,21 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;Lab20_6.c,22 :: 		Delay_ms(500);
	LDI        R18, lo_addr(R21)
	LDI        R17, 75
	LDI        R16, 191
L_main8:
	DEC        R16
	BRNE       L_main8
	DEC        R17
	BRNE       L_main8
	DEC        R18
	BRNE       L_main8
	NOP
;Lab20_6.c,23 :: 		}
L_main7:
;Lab20_6.c,25 :: 		}
L_main4:
;Lab20_6.c,26 :: 		}
	JMP        L_main0
;Lab20_6.c,27 :: 		}
L_end_main:
	JMP        L_end_main
; end of _main
