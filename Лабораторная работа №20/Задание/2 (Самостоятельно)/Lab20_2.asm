
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab20_2.c,4 :: 		void main( )
;Lab20_2.c,7 :: 		UART1_Init(9600);
	PUSH       R2
	PUSH       R3
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;Lab20_2.c,8 :: 		while(1){
L_main0:
;Lab20_2.c,9 :: 		UART1_Write_Text("Hello, student! ");                                       // инициализация  UART
	LDI        R27, #lo_addr(?lstr1_Lab20_2+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_Lab20_2+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;Lab20_2.c,10 :: 		Delay_ms(2000);
	LDI        R18, 82
	LDI        R17, 43
	LDI        R16, 0
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
	NOP
	NOP
	NOP
;Lab20_2.c,11 :: 		}
	JMP        L_main0
;Lab20_2.c,12 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
