
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 1
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;Lab20_4.c,5 :: 		void  main( )
;Lab20_4.c,8 :: 		UART1_Init(9600);
	PUSH       R2
	PUSH       R3
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;Lab20_4.c,9 :: 		while(1)
L_main0:
;Lab20_4.c,11 :: 		UART1_Write_Text("To receive message press key A.");
	LDI        R27, #lo_addr(?lstr1_Lab20_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_Lab20_4+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;Lab20_4.c,12 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;Lab20_4.c,13 :: 		Delay_ms(2000);
	LDI        R18, 82
	LDI        R17, 43
	LDI        R16, 0
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
	NOP
	NOP
	NOP
;Lab20_4.c,14 :: 		if(UART1_Data_Ready( ) == 1)
	CALL       _UART1_Data_Ready+0
	CPI        R16, 1
	BREQ       L__main17
	JMP        L_main4
L__main17:
;Lab20_4.c,16 :: 		symbol = UART1_Read( );
	CALL       _UART1_Read+0
	STD        Y+0, R16
;Lab20_4.c,17 :: 		UART1_Write(symbol);                                 // ���
	MOV        R2, R16
	CALL       _UART1_Write+0
;Lab20_4.c,18 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;Lab20_4.c,19 :: 		if(symbol == 'A' || symbol == 'a')     // ���� ������ "����������"
	LDD        R16, Y+0
	CPI        R16, 65
	BRNE       L__main18
	JMP        L__main15
L__main18:
	LDD        R16, Y+0
	CPI        R16, 97
	BRNE       L__main19
	JMP        L__main14
L__main19:
	JMP        L_main7
L__main15:
L__main14:
;Lab20_4.c,21 :: 		UART1_Write_Text("Hello, student!");
	LDI        R27, #lo_addr(?lstr2_Lab20_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr2_Lab20_4+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;Lab20_4.c,22 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;Lab20_4.c,23 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_main8:
	DEC        R16
	BRNE       L_main8
	DEC        R17
	BRNE       L_main8
	DEC        R18
	BRNE       L_main8
	NOP
;Lab20_4.c,24 :: 		}
	JMP        L_main10
L_main7:
;Lab20_4.c,27 :: 		UART1_Write_Text("Symbol is wrong! Repeat enter.");
	LDI        R27, #lo_addr(?lstr3_Lab20_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr3_Lab20_4+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;Lab20_4.c,28 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;Lab20_4.c,29 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_main11:
	DEC        R16
	BRNE       L_main11
	DEC        R17
	BRNE       L_main11
	DEC        R18
	BRNE       L_main11
	NOP
;Lab20_4.c,30 :: 		}
L_main10:
;Lab20_4.c,31 :: 		}
L_main4:
;Lab20_4.c,32 :: 		}
	JMP        L_main0
;Lab20_4.c,33 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
