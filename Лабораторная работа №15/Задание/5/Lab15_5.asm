
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

	MOVLW      255
	MOVWF      PORTD+0
	MOVF       _n+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt0
	MOVF       _n+0, 0
	ADDLW      _displayRAM+0
	MOVWF      FSR
	MOVLW      128
	IORWF      INDF+0, 0
	MOVWF      PORTC+0
	GOTO       L_interrupt1
L_interrupt0:
	MOVF       _n+0, 0
	ADDLW      _displayRAM+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
L_interrupt1:
	COMF       _select+0, 0
	MOVWF      PORTD+0
	RLF        _select+0, 1
	BCF        _select+0, 0
	INCF       _n+0, 1
	MOVF       _n+0, 0
	XORLW      4
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt2
	CLRF       _n+0
	MOVLW      1
	MOVWF      _select+0
L_interrupt2:
	MOVLW      178
	MOVWF      TMR0+0
	BCF        T0IF_bit+0, BitPos(T0IF_bit+0)
L_end_interrupt:
L__interrupt9:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

	CALL       _init+0
L_main3:
	MOVF       _channel+0, 0
	MOVWF      FARG_get_volts_channel+0
	CALL       _get_volts+0
	GOTO       L_main3
L_end_main:
	GOTO       $+0
; end of _main

_init:

	MOVLW      1
	MOVWF      TRISA+0
	CLRF       TRISC+0
	CLRF       TRISD+0
	MOVLW      255
	MOVWF      TRISB+0
	MOVLW      255
	MOVWF      PORTD+0
	CALL       _ADC_Init+0
	MOVLW      7
	MOVWF      OPTION_REG+0
	MOVLW      178
	MOVWF      TMR0+0
	BSF        INTCON+0, 5
	BSF        INTCON+0, 7
L_end_init:
	RETURN
; end of _init

_get_volts:

	MOVF       FARG_get_volts_channel+0, 0
	MOVWF      FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVLW      0
	BTFSC      R0+1, 7
	MOVLW      255
	MOVWF      R0+2
	MOVWF      R0+3
	MOVLW      136
	MOVWF      R4+0
	MOVLW      19
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_S+0
	MOVF       R0+0, 0
	MOVWF      FLOC__get_volts+0
	MOVF       R0+1, 0
	MOVWF      FLOC__get_volts+1
	MOVF       R0+2, 0
	MOVWF      FLOC__get_volts+2
	MOVF       R0+3, 0
	MOVWF      FLOC__get_volts+3
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	MOVF       FLOC__get_volts+0, 0
	MOVWF      R0+0
	MOVF       FLOC__get_volts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+0
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       FLOC__get_volts+0, 0
	MOVWF      R0+0
	MOVF       FLOC__get_volts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+1
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       FLOC__get_volts+0, 0
	MOVWF      R0+0
	MOVF       FLOC__get_volts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+2
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       FLOC__get_volts+0, 0
	MOVWF      R0+0
	MOVF       FLOC__get_volts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+3
	CLRF       get_volts_j_L1+0
L_get_volts5:
	MOVLW      4
	SUBWF      get_volts_j_L1+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_get_volts6
	MOVF       get_volts_j_L1+0, 0
	ADDLW      _displayRAM+0
	MOVWF      R2+0
	MOVF       get_volts_j_L1+0, 0
	ADDLW      get_volts_volts_L1+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	ADDLW      _table+0
	MOVWF      R0+0
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      R0+0
	MOVF       R2+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
	INCF       get_volts_j_L1+0, 1
	GOTO       L_get_volts5
L_get_volts6:
L_end_get_volts:
	RETURN
; end of _get_volts
