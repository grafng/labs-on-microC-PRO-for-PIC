
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

	MOVLW      255
	MOVWF      PORTD+0
	MOVF       _n+0, 0
	ADDLW      _displayRAM+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
	COMF       _select+0, 0
	MOVWF      PORTD+0
	RLF        _select+0, 1
	BCF        _select+0, 0
	INCF       _n+0, 1
	MOVF       _n+0, 0
	XORLW      4
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt0
	CLRF       _n+0
	MOVLW      1
	MOVWF      _select+0
L_interrupt0:
	MOVLW      178
	MOVWF      TMR0+0
	BCF        T0IF_bit+0, BitPos(T0IF_bit+0)
L_end_interrupt:
L__interrupt7:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

	CALL       _init+0
L_main1:
	MOVLW      1
	MOVWF      main_counter_L1+0
	CLRF       main_j_L1+0
L_main3:
	MOVLW      4
	SUBWF      main_j_L1+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main4
	MOVF       main_j_L1+0, 0
	ADDLW      _displayRAM+0
	MOVWF      R2+0
	MOVF       main_counter_L1+0, 0
	ADDLW      _table+0
	MOVWF      R0+0
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      R0+0
	MOVF       R2+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
	INCF       main_j_L1+0, 1
	GOTO       L_main3
L_main4:
	GOTO       L_main1
L_end_main:
	GOTO       $+0
; end of _main

_init:

	CLRF       TRISC+0
	CLRF       TRISD+0
	MOVLW      7
	MOVWF      OPTION_REG+0
	MOVLW      178
	MOVWF      TMR0+0
	BSF        INTCON+0, 5
	BSF        INTCON+0, 7
L_end_init:
	RETURN
; end of _init
