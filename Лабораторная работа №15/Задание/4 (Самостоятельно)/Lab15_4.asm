
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Lab15_4.c,16 :: 		void  interrupt( )                  // �������-���������� ���������� �� TMR0
;Lab15_4.c,18 :: 		T0IF_bit = 0;                 // �������� ���� ���������� T0IF
	BCF        T0IF_bit+0, 2
;Lab15_4.c,19 :: 		PORTC = table[t/1000];
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	MOVF       _t+0, 0
	MOVWF      R0+0
	MOVF       _t+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      _table+0
	ADDWF      R0+0, 1
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	ADDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      PORTC+0
;Lab15_4.c,20 :: 		PORTD = ~(1<<0);
	MOVLW      254
	MOVWF      PORTD+0
;Lab15_4.c,21 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_interrupt0:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt0
	DECFSZ     R12+0, 1
	GOTO       L_interrupt0
	NOP
	NOP
;Lab15_4.c,22 :: 		PORTD = 0x0F;
	MOVLW      15
	MOVWF      PORTD+0
;Lab15_4.c,23 :: 		PORTC = table[(t/100)%10];
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _t+0, 0
	MOVWF      R0+0
	MOVF       _t+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      _table+0
	ADDWF      R0+0, 1
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	ADDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      PORTC+0
;Lab15_4.c,24 :: 		PORTD = ~(1<<1);
	MOVLW      253
	MOVWF      PORTD+0
;Lab15_4.c,25 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;Lab15_4.c,26 :: 		PORTD = 0x0F;
	MOVLW      15
	MOVWF      PORTD+0
;Lab15_4.c,27 :: 		PORTC = table[(t/10)%10];
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _t+0, 0
	MOVWF      R0+0
	MOVF       _t+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      _table+0
	ADDWF      R0+0, 1
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	ADDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      PORTC+0
;Lab15_4.c,28 :: 		PORTD = ~(1<<2);
	MOVLW      251
	MOVWF      PORTD+0
;Lab15_4.c,29 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_interrupt2:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt2
	DECFSZ     R12+0, 1
	GOTO       L_interrupt2
	NOP
	NOP
;Lab15_4.c,30 :: 		PORTD = 0x0F;
	MOVLW      15
	MOVWF      PORTD+0
;Lab15_4.c,31 :: 		PORTC = table[t%10];
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _t+0, 0
	MOVWF      R0+0
	MOVF       _t+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      _table+0
	ADDWF      R0+0, 1
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	ADDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      PORTC+0
;Lab15_4.c,32 :: 		PORTD = ~(1<<3);
	MOVLW      247
	MOVWF      PORTD+0
;Lab15_4.c,33 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_interrupt3:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt3
	DECFSZ     R12+0, 1
	GOTO       L_interrupt3
	NOP
	NOP
;Lab15_4.c,34 :: 		PORTD = 0x0F;
	MOVLW      15
	MOVWF      PORTD+0
;Lab15_4.c,35 :: 		}
L__interrupt7:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;Lab15_4.c,37 :: 		void main() {
;Lab15_4.c,38 :: 		TRISD = 0;                 // ��������� ������
	CLRF       TRISD+0
;Lab15_4.c,39 :: 		TRISC = 0;
	CLRF       TRISC+0
;Lab15_4.c,40 :: 		OPTION_REG = 0x07;         // ������� ����������� 256 ������������
	MOVLW      7
	MOVWF      OPTION_REG+0
;Lab15_4.c,41 :: 		TMR0 = 248;                // ������������ 100 ��
	MOVLW      248
	MOVWF      TMR0+0
;Lab15_4.c,42 :: 		INTCON.B5 = 1;             // ��������� ���������� �� TMR0
	BSF        INTCON+0, 5
;Lab15_4.c,43 :: 		INTCON.B7 = 1;             // ����� ���������� ����������
	BSF        INTCON+0, 7
;Lab15_4.c,45 :: 		for(t=0;t<10000;t++){
	CLRF       _t+0
	CLRF       _t+1
L_main4:
	MOVLW      128
	XORWF      _t+1, 0
	MOVWF      R0+0
	MOVLW      128
	XORLW      39
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main8
	MOVLW      16
	SUBWF      _t+0, 0
L__main8:
	BTFSC      STATUS+0, 0
	GOTO       L_main5
;Lab15_4.c,46 :: 		vdelay_ms(50);
	MOVLW      50
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVLW      0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
;Lab15_4.c,45 :: 		for(t=0;t<10000;t++){
	INCF       _t+0, 1
	BTFSC      STATUS+0, 2
	INCF       _t+1, 1
;Lab15_4.c,47 :: 		}
	GOTO       L_main4
L_main5:
;Lab15_4.c,49 :: 		}
	GOTO       $+0
; end of _main
