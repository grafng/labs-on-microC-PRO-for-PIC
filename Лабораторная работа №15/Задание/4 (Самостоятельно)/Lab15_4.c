int t = 0;
code char table[] =
{
     0x3F,                   // ��� ����� "0"
     0x06,                   // ��� ����� "1"
     0x5B,                   // ��� ����� "2"
     0x4F,                   // ��� ����� "3"
     0x66,                   // ��� ����� "4"
     0x6D,                   // ��� ����� "5"
     0x7D,                   // ��� ����� "6"
     0x07,                   // ��� ����� "7"
     0x7F,                   // ��� ����� "8"
     0x6F                    // ��� ����� "9"
};

void  interrupt( )                  // �������-���������� ���������� �� TMR0
{
      T0IF_bit = 0;                 // �������� ���� ���������� T0IF
      PORTC = table[t/1000];
      PORTD = ~(1<<0);
      delay_ms(1);
      PORTD = 0x0F;
      PORTC = table[(t/100)%10];
      PORTD = ~(1<<1);
      delay_ms(1);
      PORTD = 0x0F;
      PORTC = table[(t/10)%10];
      PORTD = ~(1<<2);
      delay_ms(1);
      PORTD = 0x0F;
      PORTC = table[t%10];
      PORTD = ~(1<<3);
      delay_ms(1);
      PORTD = 0x0F;
}

void main() {
     TRISD = 0;                 // ��������� ������
     TRISC = 0;
     OPTION_REG = 0x07;         // ������� ����������� 256 ������������
     TMR0 = 248;                // ������������ 100 ��
     INTCON.B5 = 1;             // ��������� ���������� �� TMR0
     INTCON.B7 = 1;             // ����� ���������� ����������

     for(t=0;t<10000;t++){
          vdelay_ms(50);
     }

}
