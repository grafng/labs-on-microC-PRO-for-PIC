
_main:

	CLRF       main_counter_L0+0
	CLRF       TRISC+0
	CLRF       TRISD+0
L_main0:
	MOVLW      255
	MOVWF      PORTD+0
	MOVF       main_counter_L0+0, 0
	ADDLW      _table+0
	MOVWF      R0+0
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      PORTC+0
	MOVLW      240
	MOVWF      PORTD+0
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
	INCF       main_counter_L0+0, 1
	MOVLW      10
	SUBWF      main_counter_L0+0, 0
	BTFSS      STATUS+0, 0
	GOTO       L_main3
	CLRF       main_counter_L0+0
L_main3:
	GOTO       L_main0
L_end_main:
	GOTO       $+0
; end of _main
