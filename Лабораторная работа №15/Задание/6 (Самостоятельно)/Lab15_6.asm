
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Lab15_6.c,26 :: 		void  interrupt( )                      // ������� ��������� ���������� �� �������
;Lab15_6.c,28 :: 		PORTD = 0xFF;                        // ��������� ���������� �������
	MOVLW      255
	MOVWF      PORTD+0
;Lab15_6.c,29 :: 		if( n == 0 )                           // ���� 1-� ��������� (����� �����)
	MOVF       _n+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt0
;Lab15_6.c,30 :: 		PORTC = displayRAM[n] | 0b10000000;   // ����� ���� �����
	MOVF       _n+0, 0
	ADDLW      _displayRAM+0
	MOVWF      FSR
	MOVLW      128
	IORWF      INDF+0, 0
	MOVWF      PORTC+0
	GOTO       L_interrupt1
L_interrupt0:
;Lab15_6.c,33 :: 		PORTC = displayRAM[n];
	MOVF       _n+0, 0
	ADDLW      _displayRAM+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
L_interrupt1:
;Lab15_6.c,34 :: 		PORTD = ~select;                             // ��������� ���������� ��������-��
	COMF       _select+0, 0
	MOVWF      PORTD+0
;Lab15_6.c,35 :: 		select = select << 1;         // ��������� ���������� ���� ������������
	RLF        _select+0, 1
	BCF        _select+0, 0
;Lab15_6.c,36 :: 		n++;                                                // ��������� ���������
	INCF       _n+0, 1
;Lab15_6.c,37 :: 		if( n == 4 )                                      // ���� ��������� ��� ���������
	MOVF       _n+0, 0
	XORLW      4
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt2
;Lab15_6.c,39 :: 		n = 0;                                       // ������ ������������ �������
	CLRF       _n+0
;Lab15_6.c,40 :: 		select = 0b00000001;              // � ������
	MOVLW      1
	MOVWF      _select+0
;Lab15_6.c,41 :: 		}
L_interrupt2:
;Lab15_6.c,42 :: 		TMR0 = 178;                                 // ������������� TMR0
	MOVLW      178
	MOVWF      TMR0+0
;Lab15_6.c,43 :: 		T0IF_bit = 0;                         // �������� ���� ���������� �� �������
	BCF        T0IF_bit+0, 2
;Lab15_6.c,44 :: 		}
L__interrupt9:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;Lab15_6.c,46 :: 		void  main( )
;Lab15_6.c,48 :: 		init( );                                             // ����� ������� ������������� ��
	CALL       _init+0
;Lab15_6.c,49 :: 		while(1){
L_main3:
;Lab15_6.c,50 :: 		if (PORTB.B0 == 0){
	BTFSC      PORTB+0, 0
	GOTO       L_main5
;Lab15_6.c,51 :: 		get_volts(channel);             // ����� ������� ��������� ����������
	MOVF       _channel+0, 0
	MOVWF      FARG_get_volts_channel+0
	CALL       _get_volts+0
;Lab15_6.c,52 :: 		}
L_main5:
;Lab15_6.c,53 :: 		}
	GOTO       L_main3
;Lab15_6.c,54 :: 		}
	GOTO       $+0
; end of _main

_init:

;Lab15_6.c,56 :: 		void  init( )                             // ������� ������������� ��
;Lab15_6.c,58 :: 		TRISA = 0x01;                 // ��������� ����� RA0 ����� � �� ����
	MOVLW      1
	MOVWF      TRISA+0
;Lab15_6.c,59 :: 		TRISC = 0;                       // ��������� ����� ����� � �� �����
	CLRF       TRISC+0
;Lab15_6.c,60 :: 		TRISD = 0;
	CLRF       TRISD+0
;Lab15_6.c,61 :: 		TRISB = 0xFF;                      // ��������� ����� ����� D �� �����
	MOVLW      255
	MOVWF      TRISB+0
;Lab15_6.c,62 :: 		PORTD = 0xFF;                     // ��������� ���������� �������
	MOVLW      255
	MOVWF      PORTD+0
;Lab15_6.c,63 :: 		ADC_Init( );                     // ������������� ������ ���
	CALL       _ADC_Init+0
;Lab15_6.c,64 :: 		OPTION_REG = 0x07;    // ���������� ����������� ������� K = 256
	MOVLW      7
	MOVWF      OPTION_REG+0
;Lab15_6.c,65 :: 		TMR0 = 178;                    // ��������� � ������ ��������� ��������
	MOVLW      178
	MOVWF      TMR0+0
;Lab15_6.c,66 :: 		INTCON.B5 = 1;              // ��������� ���������� �� TMR0
	BSF        INTCON+0, 5
;Lab15_6.c,67 :: 		INTCON.B7 = 1;              // ���������� ���������� ����������
	BSF        INTCON+0, 7
;Lab15_6.c,68 :: 		}
	RETURN
; end of _init

_get_volts:

;Lab15_6.c,70 :: 		void  get_volts(char  channel)                 // ������� ��������� ����������
;Lab15_6.c,74 :: 		res_ADC = ADC_Read(channel);  // ������ ���� ��� ���������� ������
	MOVF       FARG_get_volts_channel+0, 0
	MOVWF      FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
;Lab15_6.c,75 :: 		mvolts = ((long)res_ADC * 5000) / 0x03FF;   // �������������� ���� ���
	MOVLW      0
	BTFSC      R0+1, 7
	MOVLW      255
	MOVWF      R0+2
	MOVWF      R0+3
	MOVLW      136
	MOVWF      R4+0
	MOVLW      19
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_S+0
	MOVF       R0+0, 0
	MOVWF      get_volts_mvolts_L0+0
	MOVF       R0+1, 0
	MOVWF      get_volts_mvolts_L0+1
;Lab15_6.c,80 :: 		volts[0] = mvolts / 1000;               // ���������� ������ �����
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+0
;Lab15_6.c,81 :: 		volts[1] = (mvolts / 100) % 10;     // ���������� ������� ����� �����
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       get_volts_mvolts_L0+0, 0
	MOVWF      R0+0
	MOVF       get_volts_mvolts_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+1
;Lab15_6.c,82 :: 		volts[2] = (mvolts / 10) % 10;       // ���������� ����� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       get_volts_mvolts_L0+0, 0
	MOVWF      R0+0
	MOVF       get_volts_mvolts_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+2
;Lab15_6.c,83 :: 		volts[3] = mvolts % 10;                 // ���������� �������� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       get_volts_mvolts_L0+0, 0
	MOVWF      R0+0
	MOVF       get_volts_mvolts_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      get_volts_volts_L1+3
;Lab15_6.c,84 :: 		for( j = 0; j < 4; j++ )                     // ������ �������������� �����
	CLRF       get_volts_j_L1+0
L_get_volts6:
	MOVLW      4
	SUBWF      get_volts_j_L1+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_get_volts7
;Lab15_6.c,86 :: 		displayRAM[j] = table[volts[j]] ;
	MOVF       get_volts_j_L1+0, 0
	ADDLW      _displayRAM+0
	MOVWF      R2+0
	MOVF       get_volts_j_L1+0, 0
	ADDLW      get_volts_volts_L1+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	ADDLW      _table+0
	MOVWF      R0+0
	MOVLW      hi_addr(_table+0)
	BTFSC      STATUS+0, 0
	ADDLW      1
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      ___DoICPAddr+0
	MOVF       R0+1, 0
	MOVWF      ___DoICPAddr+1
	CALL       _____DoICP+0
	MOVWF      R0+0
	MOVF       R2+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;Lab15_6.c,84 :: 		for( j = 0; j < 4; j++ )                     // ������ �������������� �����
	INCF       get_volts_j_L1+0, 1
;Lab15_6.c,86 :: 		displayRAM[j] = table[volts[j]] ;
	GOTO       L_get_volts6
L_get_volts7:
;Lab15_6.c,88 :: 		}
	RETURN
; end of _get_volts
