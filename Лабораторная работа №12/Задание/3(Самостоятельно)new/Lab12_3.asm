
_main:

;Lab12_3.c,33 :: 		void  main( )
;Lab12_3.c,36 :: 		init( );                                           // ����� ������� �������������
	CALL       _init+0
;Lab12_3.c,37 :: 		while(1)
L_main0:
;Lab12_3.c,39 :: 		get_sb = PORTB & 0b00000011;    // ������ ����� ����� � �
	MOVLW      3
	ANDWF      PORTB+0, 0
	MOVWF      _get_sb+0
;Lab12_3.c,41 :: 		switch( get_sb )
	GOTO       L_main2
;Lab12_3.c,43 :: 		case  0b00000011 :                 // ��� ������ SB1, SB2 �� ������
L_main4:
;Lab12_3.c,44 :: 		invite( );             // ����� �� ��� ������ �����������
	CALL       _invite+0
;Lab12_3.c,45 :: 		break;
	GOTO       L_main3
;Lab12_3.c,46 :: 		case  0b00000010 :                // ������ ������ SB1
L_main5:
;Lab12_3.c,47 :: 		out_U1( );                // ����� �� ��� ���������� ��������� U1
	CALL       _out_U1+0
;Lab12_3.c,48 :: 		break;
	GOTO       L_main3
;Lab12_3.c,49 :: 		case  0b00000001 :                // ������ ������ SB2
L_main6:
;Lab12_3.c,50 :: 		out_U2( );                // ����� �� ��� ���������� ��������� U2
	CALL       _out_U2+0
;Lab12_3.c,51 :: 		break;
	GOTO       L_main3
;Lab12_3.c,52 :: 		default :
L_main7:
;Lab12_3.c,53 :: 		out_error( );             // ����� �� ��� ��������� �� ������
	CALL       _out_error+0
;Lab12_3.c,54 :: 		}
	GOTO       L_main3
L_main2:
	MOVF       _get_sb+0, 0
	XORLW      3
	BTFSC      STATUS+0, 2
	GOTO       L_main4
	MOVF       _get_sb+0, 0
	XORLW      2
	BTFSC      STATUS+0, 2
	GOTO       L_main5
	MOVF       _get_sb+0, 0
	XORLW      1
	BTFSC      STATUS+0, 2
	GOTO       L_main6
	GOTO       L_main7
L_main3:
;Lab12_3.c,55 :: 		}
	GOTO       L_main0
;Lab12_3.c,56 :: 		}
	GOTO       $+0
; end of _main

_init:

;Lab12_3.c,58 :: 		void  init( )                     // ������� �������������
;Lab12_3.c,60 :: 		TRISA=0x03;                     // RA0 � RA1 �� ����
	MOVLW      3
	MOVWF      TRISA+0
;Lab12_3.c,61 :: 		TRISB=0x03;                     // RB0 � RB1 �� ����
	MOVLW      3
	MOVWF      TRISB+0
;Lab12_3.c,62 :: 		OPTION_REG.B7=0;                // ����������� ������������� ����������
	BCF        OPTION_REG+0, 7
;Lab12_3.c,63 :: 		ADC_Init( );                   // ������������� ������ ���
	CALL       _ADC_Init+0
;Lab12_3.c,64 :: 		Lcd_Init( );                    // ������������� ������ ���
	CALL       _Lcd_Init+0
;Lab12_3.c,65 :: 		Lcd_Cmd(_LCD_CLEAR);            // ������� �������
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_3.c,66 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);       // ���������� �������
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_3.c,68 :: 		}
	RETURN
; end of _init

_invite:

;Lab12_3.c,70 :: 		void  invite( )
;Lab12_3.c,72 :: 		Lcd_Out(1, 2, "Press only one");       // ����� ������ � 1-� ������
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_Lab12_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_3.c,73 :: 		Lcd_Out(2, 2, "key: SB1 or SB2");     // ����� ������ �� 2-� �����
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_Lab12_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_3.c,74 :: 		Delay_ms(2000);                        // �������� ������ �� ������� 2�
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_invite8:
	DECFSZ     R13+0, 1
	GOTO       L_invite8
	DECFSZ     R12+0, 1
	GOTO       L_invite8
	DECFSZ     R11+0, 1
	GOTO       L_invite8
	NOP
;Lab12_3.c,75 :: 		Lcd_Cmd(_LCD_CLEAR);                  // ������� �������
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_3.c,77 :: 		}
	RETURN
; end of _invite

_out_U1:

;Lab12_3.c,79 :: 		void  out_U1( )            // ������� ��������� ���������� U1 � ������
;Lab12_3.c,81 :: 		res_ADC = ADC_Read( 0 );                                // ������ ���� ���
	CLRF       FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVF       R0+0, 0
	MOVWF      _res_ADC+0
	MOVF       R0+1, 0
	MOVWF      _res_ADC+1
;Lab12_3.c,82 :: 		mvolts = ((long)res_ADC  * 5000) / 0x03FF;     // ��������������
	MOVLW      0
	BTFSC      R0+1, 7
	MOVLW      255
	MOVWF      R0+2
	MOVWF      R0+3
	MOVLW      136
	MOVWF      R4+0
	MOVLW      19
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_S+0
	MOVF       R0+0, 0
	MOVWF      _mvolts+0
	MOVF       R0+1, 0
	MOVWF      _mvolts+1
;Lab12_3.c,84 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_3.c,87 :: 		Lcd_Out(1, 4, "U1 = ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_Lab12_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_3.c,88 :: 		num2 = mvolts / 1000;                 // ���������� ������ �����
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	MOVWF      _num2+0
;Lab12_3.c,89 :: 		Lcd_Chr_Cp(48 + num2);          // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,90 :: 		Lcd_Chr_Cp('.');                      // ����������� ���������� �����
	MOVLW      46
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,91 :: 		num2 = (mvolts / 100) % 10;      // ���������� ������� ����� �����
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num2+0
;Lab12_3.c,92 :: 		Lcd_Chr_Cp(48 + num2);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,93 :: 		num2 = (mvolts / 10) % 10;        // ���������� ����� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num2+0
;Lab12_3.c,94 :: 		Lcd_Chr_Cp(48 + num2);          //  ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,95 :: 		num2 = mvolts % 10;                  // ���������� �������� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num2+0
;Lab12_3.c,96 :: 		Lcd_Chr_Cp(48 + num2);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,97 :: 		Lcd_Out_Cp(" V");
	MOVLW      ?lstr4_Lab12_3+0
	MOVWF      FARG_Lcd_Out_CP_text+0
	CALL       _Lcd_Out_CP+0
;Lab12_3.c,98 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_out_U19:
	DECFSZ     R13+0, 1
	GOTO       L_out_U19
	DECFSZ     R12+0, 1
	GOTO       L_out_U19
	DECFSZ     R11+0, 1
	GOTO       L_out_U19
	NOP
;Lab12_3.c,101 :: 		Lcd_Out(2, 4, "U1 = ");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_Lab12_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_3.c,102 :: 		num = mvolts / 1000;                 // ���������� ������ �����
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,103 :: 		Lcd_Chr_Cp(48 + num);          // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,104 :: 		Lcd_Chr_Cp('.');                      // ����������� ���������� �����
	MOVLW      46
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,105 :: 		num = (mvolts / 100) % 10;      // ���������� ������� ����� �����
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,106 :: 		Lcd_Chr_Cp(48 + num);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,107 :: 		num = (mvolts / 10) % 10;        // ���������� ����� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,108 :: 		Lcd_Chr_Cp(48 + num);          //  ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,109 :: 		num = mvolts % 10;                  // ���������� �������� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,110 :: 		Lcd_Chr_Cp(48 + num);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,111 :: 		Lcd_Out_Cp(" V");
	MOVLW      ?lstr6_Lab12_3+0
	MOVWF      FARG_Lcd_Out_CP_text+0
	CALL       _Lcd_Out_CP+0
;Lab12_3.c,112 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_out_U110:
	DECFSZ     R13+0, 1
	GOTO       L_out_U110
	DECFSZ     R12+0, 1
	GOTO       L_out_U110
	DECFSZ     R11+0, 1
	GOTO       L_out_U110
	NOP
;Lab12_3.c,113 :: 		}
	RETURN
; end of _out_U1

_out_U2:

;Lab12_3.c,115 :: 		void  out_U2( )             // ������� ��������� ���������� U2 � ������
;Lab12_3.c,118 :: 		res_ADC = ADC_Read(1 );                                // ������ ���� ���
	MOVLW      1
	MOVWF      FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVF       R0+0, 0
	MOVWF      _res_ADC+0
	MOVF       R0+1, 0
	MOVWF      _res_ADC+1
;Lab12_3.c,119 :: 		mvolts = ((long)res_ADC  * 5000) / 0x03FF;     // ��������������
	MOVLW      0
	BTFSC      R0+1, 7
	MOVLW      255
	MOVWF      R0+2
	MOVWF      R0+3
	MOVLW      136
	MOVWF      R4+0
	MOVLW      19
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_S+0
	MOVF       R0+0, 0
	MOVWF      _mvolts+0
	MOVF       R0+1, 0
	MOVWF      _mvolts+1
;Lab12_3.c,121 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_3.c,127 :: 		Lcd_Out(2, 4, "U2 = ");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_Lab12_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_3.c,130 :: 		num = mvolts / 1000;                 // ���������� ������ �����
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,131 :: 		Lcd_Chr_Cp(48 + num);          // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,132 :: 		Lcd_Chr_Cp('.');                      // ����������� ���������� �����
	MOVLW      46
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,133 :: 		num = (mvolts / 100) % 10;      // ���������� ������� ����� �����
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,134 :: 		Lcd_Chr_Cp(48 + num);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,135 :: 		num = (mvolts / 10) % 10;        // ���������� ����� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,136 :: 		Lcd_Chr_Cp(48 + num);          //  ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,137 :: 		num = mvolts % 10;                  // ���������� �������� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _mvolts+0, 0
	MOVWF      R0+0
	MOVF       _mvolts+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _num+0
;Lab12_3.c,138 :: 		Lcd_Chr_Cp(48 + num);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;Lab12_3.c,139 :: 		Lcd_Out_Cp(" V");
	MOVLW      ?lstr8_Lab12_3+0
	MOVWF      FARG_Lcd_Out_CP_text+0
	CALL       _Lcd_Out_CP+0
;Lab12_3.c,140 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_out_U211:
	DECFSZ     R13+0, 1
	GOTO       L_out_U211
	DECFSZ     R12+0, 1
	GOTO       L_out_U211
	DECFSZ     R11+0, 1
	GOTO       L_out_U211
	NOP
;Lab12_3.c,141 :: 		}
	RETURN
; end of _out_U2

_out_error:

;Lab12_3.c,143 :: 		void  out_error( )
;Lab12_3.c,145 :: 		Lcd_Out(1, 2, "Error! Two keys");     // ����� ������ � 1-� ������
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_Lab12_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_3.c,146 :: 		Lcd_Out(2, 2, "are pressed");       // ����� ������ �� 2-� ������
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_Lab12_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_3.c,147 :: 		Delay_ms(2000);                     // �������� ������ �� ������� 2�
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_out_error12:
	DECFSZ     R13+0, 1
	GOTO       L_out_error12
	DECFSZ     R12+0, 1
	GOTO       L_out_error12
	DECFSZ     R11+0, 1
	GOTO       L_out_error12
	NOP
;Lab12_3.c,148 :: 		Lcd_Cmd(_LCD_CLEAR);              // ������� �������
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_3.c,150 :: 		}
	RETURN
; end of _out_error
