#line 1 "C:/����\������������ ������ �12/�������/3 (��������������)/Lab12_3.c"
#line 5 "C:/����\������������ ������ �12/�������/3 (��������������)/Lab12_3.c"
sbit LCD_RS at RC2_bit;
sbit LCD_EN at RC3_bit;
sbit LCD_D4 at RC4_bit;
sbit LCD_D5 at RC5_bit;
sbit LCD_D6 at RC6_bit;
sbit LCD_D7 at RC7_bit;

sbit LCD_RS_Direction at TRISC2_bit;
sbit LCD_EN_Direction at TRISC3_bit;
sbit LCD_D4_Direction at TRISC4_bit;
sbit LCD_D5_Direction at TRISC5_bit;
sbit LCD_D6_Direction at TRISC6_bit;
sbit LCD_D7_Direction at TRISC7_bit;

int res_ADC;
int mvolts;
char num;
char string[10];
char get_sb;

void init( );
void out_U1( );
void out_U2( );
void invite( );

void out_error( );

void main( )
{

 init( );
 while(1)
 {
 get_sb = PORTB & 0b00000011;

 switch( get_sb )
 {
 case 0b00000011 :
 invite( );
 break;
 case 0b00000010 :
 out_U1( );
 break;
 case 0b00000001 :
 out_U2( );
 break;
 default :
 out_error( );
 }
 }
}

void init( )
{
 TRISA=0x03;
 TRISB=0x03;
 OPTION_REG.B7=0;
 ADC_Init( );
 Lcd_Init( );
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);

}

void invite( )
{
 Lcd_Out(1, 2, "Press only one");
 Lcd_Out(2, 2, "key: SB1 or SB2");
 Delay_ms(2000);
 Lcd_Cmd(_LCD_CLEAR);

}

void out_U1( )
{
 res_ADC = ADC_Read( 0 );
 mvolts = ((long)res_ADC * 5000) / 0x03FF;

 Lcd_Cmd(_LCD_CLEAR);
#line 100 "C:/����\������������ ������ �12/�������/3 (��������������)/Lab12_3.c"
 Lcd_Out(2, 4, "U1 = ");
 num = mvolts / 1000;
 Lcd_Chr_Cp(48 + num);
 Lcd_Chr_Cp('.');
 num = (mvolts / 100) % 10;
 Lcd_Chr_Cp(48 + num);
 num = (mvolts / 10) % 10;
 Lcd_Chr_Cp(48 + num);
 num = mvolts % 10;
 Lcd_Chr_Cp(48 + num);
 Lcd_Out_Cp(" V");
 Delay_ms(2000);
}

void out_U2( )
{

 res_ADC = ADC_Read(1 );
 mvolts = ((long)res_ADC * 5000) / 0x03FF;

 Lcd_Cmd(_LCD_CLEAR);





 Lcd_Out(2, 4, "U2 = ");


 num = mvolts / 1000;
 Lcd_Chr_Cp(48 + num);
 Lcd_Chr_Cp('.');
 num = (mvolts / 100) % 10;
 Lcd_Chr_Cp(48 + num);
 num = (mvolts / 10) % 10;
 Lcd_Chr_Cp(48 + num);
 num = mvolts % 10;
 Lcd_Chr_Cp(48 + num);
 Lcd_Out_Cp(" V");
 Delay_ms(2000);
}

void out_error( )
{
 Lcd_Out(1, 2, "Error! Two keys");
 Lcd_Out(2, 2, "are pressed");
 Delay_ms(2000);
 Lcd_Cmd(_LCD_CLEAR);

}
