
_main:

;Lab12_1.c,19 :: 		void  main(void)
;Lab12_1.c,24 :: 		TRISA = 0x01;                        // ��������� ����� RA0 �� ����
	MOVLW      1
	MOVWF      TRISA+0
;Lab12_1.c,25 :: 		ADC_Init( );                            // ������������� ������ ���
	CALL       _ADC_Init+0
;Lab12_1.c,26 :: 		Lcd_Init( );                              // ������������� ���
	CALL       _Lcd_Init+0
;Lab12_1.c,27 :: 		Lcd_Cmd(_LCD_CLEAR);             // �������� ���
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_1.c,28 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);   // ��������� ����������� �������
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_1.c,29 :: 		while( 1 )
L_main0:
;Lab12_1.c,31 :: 		res_ADC = ADC_Read( 0 );                   // ������ ���� ���
	CLRF       FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
;Lab12_1.c,32 :: 		mvolts = ((long)res_ADC  * 500) / 0x03FF;       //��������������
	MOVLW      0
	BTFSC      R0+1, 7
	MOVLW      255
	MOVWF      R0+2
	MOVWF      R0+3
	MOVLW      244
	MOVWF      R4+0
	MOVLW      1
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_S+0
;Lab12_1.c,34 :: 		IntToStr( mvolts, string );             // �������������� ����� � ������
	MOVF       R0+0, 0
	MOVWF      FARG_IntToStr_input+0
	MOVF       R0+1, 0
	MOVWF      FARG_IntToStr_input+1
	MOVLW      main_string_L0+0
	MOVWF      FARG_IntToStr_output+0
	CALL       _IntToStr+0
;Lab12_1.c,35 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab12_1.c,36 :: 		Lcd_Out(1, 4, "U1 =");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_Lab12_1+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab12_1.c,37 :: 		Lcd_Out_Cp( string );                  // ����� �������� ����������
	MOVLW      main_string_L0+0
	MOVWF      FARG_Lcd_Out_CP_text+0
	CALL       _Lcd_Out_CP+0
;Lab12_1.c,38 :: 		Lcd_Out_Cp( " mV");
	MOVLW      ?lstr2_Lab12_1+0
	MOVWF      FARG_Lcd_Out_CP_text+0
	CALL       _Lcd_Out_CP+0
;Lab12_1.c,39 :: 		Delay_ms(2000) ;                        // �������� �� 2 �
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;Lab12_1.c,40 :: 		}
	GOTO       L_main0
;Lab12_1.c,41 :: 		}
	GOTO       $+0
; end of _main
