/*******************************************************************
voltmeter.c - i?ia?aiia ecia?aiey aaoo iai?y?aiee n auaiaii ia ?EA
*******************************************************************/
// I?eniaaeiaiea auaiaia ?EA
sbit  LCD_RS  at  RC2_bit;
sbit  LCD_EN  at  RC3_bit;
sbit  LCD_D4  at  RC4_bit;
sbit  LCD_D5  at  RC5_bit;
sbit  LCD_D6  at  RC6_bit;
sbit  LCD_D7  at  RC7_bit;
// Iai?aaeaiea auaiaia
sbit  LCD_RS_Direction  at TRISC2_bit;
sbit  LCD_EN_Direction  at TRISC3_bit;
sbit  LCD_D4_Direction  at  TRISC4_bit;
sbit  LCD_D5_Direction  at  TRISC5_bit;
sbit  LCD_D6_Direction  at  TRISC6_bit;
sbit  LCD_D7_Direction  at  TRISC7_bit;

int  res_ADC;      // ia?aiaiiay aey o?aiaiey eiaa AOI
int  mvolts;          // ia?aiaiiay aey o?aiaiey iai?y?aiey a ieeeeaieuoao
char  num;           // ia?aiaiiay aey o?aiaiey oeo? iai?y?aiey a aieuoao
char  num2;
char  string[10];  // ia?aiaiiay aey o?aiaiey no?iee
char  get_sb;       // ia?aiaiiay aey o?aiaiey ninoiyiey eiioaeoia
// eiiiie SB1, SB2
void  init( );                  // i?ioioei ooieoee eieoeaeecaoee
void  out_U1( );           // i?ioioei ooieoee auaiaa ia ?EA iai?y?aiey U1
void  out_U2( );           // i?ioioei ooieoee auaiaa ia ?EA iai?y?aiey U2
void  invite( );       // i?ioioei ooieoee auaiaa ia ?EA
// oaenoa i?eaeaoaiey
void  out_error( );        // i?ioioei ooieoee auaiaa ia ?EA
// oaenoa niiauaiey ia ioeaea
void  main( )
{

    init( );                                           // aucia ooieoee eieoeaeecaoee
    while(1)
    {
        get_sb = PORTB & 0b00000011;    // ?oaiea eeiee ii?oa E e
        // auaaeaiea ?ac?yaia RB1, RB0
        switch( get_sb )
        {
        case  0b00000011 :                 // iaa eiiiee SB1, SB2 ia ia?aou
            invite( );             // auaia ia ?EA oaenoa i?eaeaoaiey
            break;
        case  0b00000010 :                // ia?aoa eiiiea SB1
            out_U1( );                // auaia ia ?EA ?acoeuoaoa ecia?aiey U1
            break;
        case  0b00000001 :                // ia?aoa eiiiea SB2
            out_U2( );                // auaia ia ?EA ?acoeuoaoa ecia?aiey U2
            break;
        default :
            out_error( );             // auaia ia ?EA niiauaiey ia ioeaea
        }
    }
}

void  init( )                     // ooieoey eieoeaeecaoee
{
    TRISA=0x03;                     // RA0 e RA1 ia aaia
    TRISB=0x03;                     // RB0 e RB1 ia aaia
    OPTION_REG.B7=0;                // iiaee??aiea iiaoyaeaa?ueo ?acenoi?ia
    ADC_Init( );                   // eieoeaeecaoey iiaoey AOI
    Lcd_Init( );                    // eieoeaeecaoey iiaoey ?EA
    Lcd_Cmd(_LCD_CLEAR);            // i?enoea aenieay
    Lcd_Cmd(_LCD_CURSOR_OFF);       // ioee??aiea eo?ni?a

}

void  invite( )
{
    Lcd_Out(1, 2, "Press only one");       // auaia oaenoa a 1-? no?ieo
    Lcd_Out(2, 2, "key: SB1 or SB2");     // auaia oaenoa ai 2-? no?ie
    Delay_ms(2000);                        // caaa??ea auaiaa ia aenieae 2n
    Lcd_Cmd(_LCD_CLEAR);                  // i?enoea aenieay

}

void  out_U1( )            // ooieoey ecia?aiey iai?y?aiey U1 e auaiaa
{                                   // ?acoeuoaoa ia ?EA
    res_ADC = ADC_Read( 0 );                                // ?oaiea eiaa AOI
    mvolts = ((long)res_ADC  * 5000) / 0x03FF;     // i?aia?aciaaiea
    // eiaa AOI a ieeeeaieuou
    Lcd_Cmd(_LCD_CLEAR);


    Lcd_Out(1, 4, "U1 = ");
    num2 = mvolts / 1000;                 // ecaea?aiea aaeieo aieuo
    Lcd_Chr_Cp(48 + num2);          // ioia?a?aiea ?enea a eiaa ASCII
                      // ioia?a?aiea aanyoe?iie oi?ee
    num2 = (mvolts / 100) % 10;      // ecaea?aiea aanyouo aieae aieuo
    Lcd_Chr_Cp(48 + num2);           // ioia?a?aiea ?enea a eiaa ASCII
    num2 = (mvolts / 10) % 10;        // ecaea?aiea niouo aieae aieuo
    Lcd_Chr_Cp(48 + num2);          //  ioia?a?aiea ?enea a eiaa ASCII
    num2 = mvolts % 10;                  // ecaea?aiea ouny?iuo aieae aieuo
    Lcd_Chr_Cp(48 + num2);           // ioia?a?aiea ?enea a eiaa ASCII
    Lcd_Out_Cp(" mV");


    Lcd_Out(2, 4, "U1 = ");
    num = mvolts / 1000;                 // ecaea?aiea aaeieo aieuo
    Lcd_Chr_Cp(48 + num);          // ioia?a?aiea ?enea a eiaa ASCII
    Lcd_Chr_Cp('.');                      // ioia?a?aiea aanyoe?iie oi?ee
    num = (mvolts / 100) % 10;      // ecaea?aiea aanyouo aieae aieuo
    Lcd_Chr_Cp(48 + num);           // ioia?a?aiea ?enea a eiaa ASCII
    num = (mvolts / 10) % 10;        // ecaea?aiea niouo aieae aieuo
    Lcd_Chr_Cp(48 + num);          //  ioia?a?aiea ?enea a eiaa ASCII
    num = mvolts % 10;                  // ecaea?aiea ouny?iuo aieae aieuo
    Lcd_Chr_Cp(48 + num);           // ioia?a?aiea ?enea a eiaa ASCII
    Lcd_Out_Cp(" V");
    Delay_ms(2000);
}

void  out_U2( )             // ooieoey ecia?aiey iai?y?aiey U2 e auaiaa
{                                    // ?acoeuoaoa ia ?EA

    res_ADC = ADC_Read(1 );                                // ?oaiea eiaa AOI
    mvolts = ((long)res_ADC  * 5000) / 0x03FF;     // i?aia?aciaaiea
    // eiaa AOI a ieeeeaieuou
    Lcd_Cmd(_LCD_CLEAR);


//    Lcd_Out(1, 4, "U2 = ");


    Lcd_Out(1, 4, "U2 = ");
    num2 = mvolts / 1000;                 // ecaea?aiea aaeieo aieuo
    Lcd_Chr_Cp(48 + num2);          // ioia?a?aiea ?enea a eiaa ASCII
                      // ioia?a?aiea aanyoe?iie oi?ee
    num2 = (mvolts / 100) % 10;      // ecaea?aiea aanyouo aieae aieuo
    Lcd_Chr_Cp(48 + num2);           // ioia?a?aiea ?enea a eiaa ASCII
    num2 = (mvolts / 10) % 10;        // ecaea?aiea niouo aieae aieuo
    Lcd_Chr_Cp(48 + num2);          //  ioia?a?aiea ?enea a eiaa ASCII
    num2 = mvolts % 10;                  // ecaea?aiea ouny?iuo aieae aieuo
    Lcd_Chr_Cp(48 + num2);           // ioia?a?aiea ?enea a eiaa ASCII
    Lcd_Out_Cp(" mV");


    Lcd_Out(2, 4, "U2 = ");


    num = mvolts / 1000;                 // ecaea?aiea aaeieo aieuo
    Lcd_Chr_Cp(48 + num);          // ioia?a?aiea ?enea a eiaa ASCII
    Lcd_Chr_Cp('.');                      // ioia?a?aiea aanyoe?iie oi?ee
    num = (mvolts / 100) % 10;      // ecaea?aiea aanyouo aieae aieuo
    Lcd_Chr_Cp(48 + num);           // ioia?a?aiea ?enea a eiaa ASCII
    num = (mvolts / 10) % 10;        // ecaea?aiea niouo aieae aieuo
    Lcd_Chr_Cp(48 + num);          //  ioia?a?aiea ?enea a eiaa ASCII
    num = mvolts % 10;                  // ecaea?aiea ouny?iuo aieae aieuo
    Lcd_Chr_Cp(48 + num);           // ioia?a?aiea ?enea a eiaa ASCII
    Lcd_Out_Cp(" V");
    Delay_ms(2000);
}

void  out_error( )
{
    Lcd_Out(1, 2, "Error! Two keys");     // auaia oaenoa a 1-? no?ieo
    Lcd_Out(2, 2, "are pressed");       // auaia oaenoa ai 2-? no?ieo
    Delay_ms(2000);                     // caaa??ea auaiaa ia aenieae 2n
    Lcd_Cmd(_LCD_CLEAR);              // i?enoea aenieay

}
