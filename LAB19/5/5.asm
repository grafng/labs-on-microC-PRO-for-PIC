
_timer0_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;5.c,4 :: 		void  timer0_ISR( )  org  IVT_ADDR_TIMER0_OVF
;5.c,6 :: 		PORTC0_bit = ~PORTC0_bit;               // �������������  ����� ����� PC0
	IN         R0, PORTC0_bit+0
	LDI        R27, BitMask(PORTC0_bit+0)
	EOR        R0, R27
	OUT        PORTC0_bit+0, R0
;5.c,7 :: 		TCNT0 = 287;                            // ������������� ������ T/C0
	LDI        R27, 31
	OUT        TCNT0+0, R27
;5.c,8 :: 		}
L_end_timer0_ISR:
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _timer0_ISR

_timer1_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;5.c,10 :: 		void  timer1_ISR( )  org  IVT_ADDR_TIMER1_OVF
;5.c,12 :: 		PORTC3_bit = ~PORTC3_bit;               // ������������� ����� ����� PC3
	IN         R0, PORTC3_bit+0
	LDI        R27, BitMask(PORTC3_bit+0)
	EOR        R0, R27
	OUT        PORTC3_bit+0, R0
;5.c,13 :: 		TCNT1H = 0xF0;                          // ������������� ������� �������
	LDI        R27, 240
	OUT        TCNT1H+0, R27
;5.c,14 :: 		TCNT1L = 0xBE;                          // ������������� ������� �������
	LDI        R27, 190
	OUT        TCNT1L+0, R27
;5.c,15 :: 		}
L_end_timer1_ISR:
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _timer1_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;5.c,17 :: 		void  main( )
;5.c,19 :: 		DDC0_bit = 1;                           // ��������� ����� PC0 �� �����
	IN         R27, DDC0_bit+0
	SBR        R27, BitMask(DDC0_bit+0)
	OUT        DDC0_bit+0, R27
;5.c,20 :: 		DDC3_bit = 1;                            // ��������� ����� PC3 �� �����
	IN         R27, DDC3_bit+0
	SBR        R27, BitMask(DDC3_bit+0)
	OUT        DDC3_bit+0, R27
;5.c,21 :: 		TCCR1A = 0;
	LDI        R27, 0
	OUT        TCCR1A+0, R27
;5.c,22 :: 		TCNT0 = 287;                            // ��������� ��������� �������� � T/C0
	LDI        R27, 31
	OUT        TCNT0+0, R27
;5.c,23 :: 		TCCR0 = 0b00000101;                     // ���������� � = 1024
	LDI        R27, 5
	OUT        TCCR0+0, R27
;5.c,24 :: 		TCCR1B = 0b00000101;                     // ���������� � = 1024
	LDI        R27, 5
	OUT        TCCR1B+0, R27
;5.c,25 :: 		TCNT1H = 0xF0;                           // ��������� � ������� ��������
	LDI        R27, 240
	OUT        TCNT1H+0, R27
;5.c,26 :: 		TCNT1L = 0xBE;                           // ��������� ��������
	LDI        R27, 190
	OUT        TCNT1L+0, R27
;5.c,27 :: 		TOIE1_bit = 1;                           // ��������� ���������� �� ������������ T/C1
	IN         R27, TOIE1_bit+0
	SBR        R27, BitMask(TOIE1_bit+0)
	OUT        TOIE1_bit+0, R27
;5.c,28 :: 		SREG_I_bit = 1;                          // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;5.c,29 :: 		TOIE0_bit = 1;                          // ��������� ���������� �� ������������ T/C0
	IN         R27, TOIE0_bit+0
	SBR        R27, BitMask(TOIE0_bit+0)
	OUT        TOIE0_bit+0, R27
;5.c,30 :: 		SREG_I_bit = 1;                         // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;5.c,31 :: 		while(1);
L_main0:
	JMP        L_main0
;5.c,32 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
