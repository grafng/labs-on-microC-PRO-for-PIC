
_timer1_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;4.c,4 :: 		void  timer1_ISR( )  org  IVT_ADDR_TIMER1_OVF
;4.c,6 :: 		PORTC3_bit = ~PORTC3_bit;   // ������������� ����� ����� PC3
	IN         R0, PORTC3_bit+0
	LDI        R27, BitMask(PORTC3_bit+0)
	EOR        R0, R27
	OUT        PORTC3_bit+0, R0
;4.c,7 :: 		TCNT1H = 0xF0;                        // ������������� ������� �������
	LDI        R27, 240
	OUT        TCNT1H+0, R27
;4.c,8 :: 		TCNT1L = 0xBE;                       // ������������� ������� �������
	LDI        R27, 190
	OUT        TCNT1L+0, R27
;4.c,9 :: 		}
L_end_timer1_ISR:
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _timer1_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;4.c,10 :: 		void  main( )
;4.c,12 :: 		DDC3_bit = 1;                            // ��������� ����� PC3 �� �����
	IN         R27, DDC3_bit+0
	SBR        R27, BitMask(DDC3_bit+0)
	OUT        DDC3_bit+0, R27
;4.c,13 :: 		TCCR1A = 0;
	LDI        R27, 0
	OUT        TCCR1A+0, R27
;4.c,14 :: 		TCCR1B = 0b00000101;           // ���������� � = 1024
	LDI        R27, 5
	OUT        TCCR1B+0, R27
;4.c,15 :: 		TCNT1H = 0xF0;                       // ��������� � ������� ��������
	LDI        R27, 240
	OUT        TCNT1H+0, R27
;4.c,16 :: 		TCNT1L = 0xBE;                       // ��������� ��������
	LDI        R27, 190
	OUT        TCNT1L+0, R27
;4.c,17 :: 		TOIE1_bit = 1;           // ��������� ���������� �� ������������ T/C1
	IN         R27, TOIE1_bit+0
	SBR        R27, BitMask(TOIE1_bit+0)
	OUT        TOIE1_bit+0, R27
;4.c,18 :: 		SREG_I_bit = 1;                        // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;4.c,19 :: 		while(1);
L_main0:
	JMP        L_main0
;4.c,20 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
