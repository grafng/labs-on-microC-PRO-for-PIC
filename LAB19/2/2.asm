
_int1_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;2.c,4 :: 		void  int1_ISR( )  org  IVT_ADDR_INT1                    // �������-���������� �������-���
;2.c,6 :: 		PORTC3_bit = ~PORTC3_bit;                           // ����������� ��������� D2
	IN         R0, PORTC3_bit+0
	LDI        R27, BitMask(PORTC3_bit+0)
	EOR        R0, R27
	OUT        PORTC3_bit+0, R0
;2.c,7 :: 		}
L_end_int1_ISR:
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _int1_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;2.c,8 :: 		void  main( )
;2.c,10 :: 		DDRC = 0xFF;                                        // ��������� ����� ����� � �� �����
	LDI        R27, 255
	OUT        DDRC+0, R27
;2.c,11 :: 		PORTC = 0;                                          // �������� ����������
	LDI        R27, 0
	OUT        PORTC+0, R27
;2.c,12 :: 		DDD3_bit = 0;                                       // ��������� ����� PD2 (INT0) �� ����
	IN         R27, DDD3_bit+0
	CBR        R27, BitMask(DDD3_bit+0)
	OUT        DDD3_bit+0, R27
;2.c,13 :: 		PORTD3_bit = 1;                                     // ���������� � PD2 ������������� ��������
	IN         R27, PORTD3_bit+0
	SBR        R27, BitMask(PORTD3_bit+0)
	OUT        PORTD3_bit+0, R27
;2.c,14 :: 		INT1_bit = 1;                                       // ��������� ���������� �� ����� INT0
	IN         R27, INT1_bit+0
	SBR        R27, BitMask(INT1_bit+0)
	OUT        INT1_bit+0, R27
;2.c,15 :: 		MCUCR = 0b00001010;                                 // ���������� �� �������� 1 -> 0 ��� INT0
	LDI        R27, 10
	OUT        MCUCR+0, R27
;2.c,16 :: 		SREG_I_bit = 1;                                     // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;2.c,17 :: 		while(1)
L_main0:
;2.c,19 :: 		PORTC0_bit = ~PORTC0_bit;                     // ����������� ��������� D1
	IN         R0, PORTC0_bit+0
	LDI        R27, BitMask(PORTC0_bit+0)
	EOR        R0, R27
	OUT        PORTC0_bit+0, R0
;2.c,20 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
;2.c,21 :: 		}
	JMP        L_main0
;2.c,22 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
