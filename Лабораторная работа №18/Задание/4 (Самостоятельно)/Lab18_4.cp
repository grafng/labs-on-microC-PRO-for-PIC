#line 1 "H:/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/������������ ������ �18/�������/4 (��������������)/Lab18_4.c"
#line 5 "H:/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/������������ ������ �18/�������/4 (��������������)/Lab18_4.c"
sbit LCD_RS at PORTC2_bit;
sbit LCD_EN at PORTC3_bit;
sbit LCD_D4 at PORTC4_bit;
sbit LCD_D5 at PORTC5_bit;
sbit LCD_D6 at PORTC6_bit;
sbit LCD_D7 at PORTC7_bit;

sbit LCD_RS_Direction at DDC2_bit;
sbit LCD_EN_Direction at DDC3_bit;
sbit LCD_D4_Direction at DDC4_bit;
sbit LCD_D5_Direction at DDC5_bit;
sbit LCD_D6_Direction at DDC6_bit;
sbit LCD_D7_Direction at DDC7_bit;

char displayRAM[4];


void init( );
void get_volts(char channel);
void out_display1( );
void out_display2( );
void out_display3( );
void out_error();
void out_welcome();
void main( )
{
 init( );
 while(1)
 {


 switch (PINB) {
 case 0x07:
 out_welcome();
 break;
 case 0x06:
 get_volts(0);
 out_display1( );
 Delay_ms(500);
 break;
 case 0x05:
 get_volts(1);
 out_display2( );
 Delay_ms(500);
 break;
 case 0x03:
 get_volts(2);
 out_display3( );
 Delay_ms(500);
 break;
 default:

 out_error();
 break;
 }
 }
}


void out_error(){
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(2, 4, "ERROR !!! ");
 Delay_ms(500);
}
void out_welcome(){
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(2, 4, "Press one key");
 Delay_ms(500);
}
void init( )
{
 DDRB = 0x00;
 PORTB = 0x07;

 Lcd_Init( );
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);
}
 void get_volts(char channel)
{
 int res_ADC;
 int mvolts;
 res_ADC = ADC_Read(channel);
 mvolts = ((long)res_ADC * 5000) / 0x03FF;

 {
 char volts[4];
 char j;
 volts[0] = mvolts / 1000;
 volts[1] = (mvolts / 100) % 10;
 volts[2] = (mvolts / 10) % 10;
 volts[3] = mvolts % 10;
 for(j = 0; j < 4; j++)
 displayRAM[j] = volts[j];
 }
}

void out_display1( )
{
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(2, 4, "U1 = ");
 Lcd_Chr_Cp(48 + displayRAM[0]);
 Lcd_Chr_Cp('.');
 Lcd_Chr_Cp(48 + displayRAM[1]);
 Lcd_Chr_Cp(48 + displayRAM[2]);
 Lcd_Chr_Cp(48 + displayRAM[3]);
 Lcd_Out_Cp(" V");
}

 void out_display2( )
{
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(2, 4, "U2 = ");
 Lcd_Chr_Cp(48 + displayRAM[0]);
 Lcd_Chr_Cp('.');
 Lcd_Chr_Cp(48 + displayRAM[1]);
 Lcd_Chr_Cp(48 + displayRAM[2]);
 Lcd_Chr_Cp(48 + displayRAM[3]);
 Lcd_Out_Cp(" V");
}

 void out_display3( )
{
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(2, 4, "U3 = ");
 Lcd_Chr_Cp(48 + displayRAM[0]);
 Lcd_Chr_Cp('.');
 Lcd_Chr_Cp(48 + displayRAM[1]);
 Lcd_Chr_Cp(48 + displayRAM[2]);
 Lcd_Chr_Cp(48 + displayRAM[3]);
 Lcd_Out_Cp(" V");
}
