
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab18_4.c,29 :: 		void  main( )
;Lab18_4.c,31 :: 		init( );                                   // ����� ������� �������������
	PUSH       R2
	CALL       _init+0
;Lab18_4.c,32 :: 		while(1)
L_main0:
;Lab18_4.c,36 :: 		switch (PINB) {
	JMP        L_main2
;Lab18_4.c,37 :: 		case 0x07:
L_main4:
;Lab18_4.c,38 :: 		out_welcome();
	CALL       _out_welcome+0
;Lab18_4.c,39 :: 		break;
	JMP        L_main3
;Lab18_4.c,40 :: 		case 0x06:
L_main5:
;Lab18_4.c,41 :: 		get_volts(0);                               // ����� ������� ��������� ����������
	CLR        R2
	CALL       _get_volts+0
;Lab18_4.c,42 :: 		out_display1( );                // ����� ������� ������ �� ���
	CALL       _out_display1+0
;Lab18_4.c,43 :: 		Delay_ms(500);           // �������� �� 1  �
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_main6:
	DEC        R16
	BRNE       L_main6
	DEC        R17
	BRNE       L_main6
	DEC        R18
	BRNE       L_main6
	NOP
;Lab18_4.c,44 :: 		break;
	JMP        L_main3
;Lab18_4.c,45 :: 		case 0x05:
L_main8:
;Lab18_4.c,46 :: 		get_volts(1);                                   // ����� ������� ��������� ����������
	LDI        R27, 1
	MOV        R2, R27
	CALL       _get_volts+0
;Lab18_4.c,47 :: 		out_display2( );                // ����� ������� ������ �� ���
	CALL       _out_display2+0
;Lab18_4.c,48 :: 		Delay_ms(500);           // �������� �� 1  �
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_main9:
	DEC        R16
	BRNE       L_main9
	DEC        R17
	BRNE       L_main9
	DEC        R18
	BRNE       L_main9
	NOP
;Lab18_4.c,49 :: 		break;
	JMP        L_main3
;Lab18_4.c,50 :: 		case 0x03:
L_main11:
;Lab18_4.c,51 :: 		get_volts(2);                                   // ����� ������� ��������� ����������
	LDI        R27, 2
	MOV        R2, R27
	CALL       _get_volts+0
;Lab18_4.c,52 :: 		out_display3( );                // ����� ������� ������ �� ���
	CALL       _out_display3+0
;Lab18_4.c,53 :: 		Delay_ms(500);           // �������� �� 1  �
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_main12:
	DEC        R16
	BRNE       L_main12
	DEC        R17
	BRNE       L_main12
	DEC        R18
	BRNE       L_main12
	NOP
;Lab18_4.c,54 :: 		break;
	JMP        L_main3
;Lab18_4.c,55 :: 		default:
L_main14:
;Lab18_4.c,57 :: 		out_error();
	CALL       _out_error+0
;Lab18_4.c,58 :: 		break;
	JMP        L_main3
;Lab18_4.c,59 :: 		}
L_main2:
	IN         R16, PINB+0
	CPI        R16, 7
	BRNE       L__main23
	JMP        L_main4
L__main23:
	IN         R16, PINB+0
	CPI        R16, 6
	BRNE       L__main24
	JMP        L_main5
L__main24:
	IN         R16, PINB+0
	CPI        R16, 5
	BRNE       L__main25
	JMP        L_main8
L__main25:
	IN         R16, PINB+0
	CPI        R16, 3
	BRNE       L__main26
	JMP        L_main11
L__main26:
	JMP        L_main14
L_main3:
;Lab18_4.c,60 :: 		}
	JMP        L_main0
;Lab18_4.c,61 :: 		}
L_end_main:
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main

_out_error:

;Lab18_4.c,64 :: 		void out_error(){
;Lab18_4.c,65 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_4.c,66 :: 		Lcd_Out(2, 4, "ERROR !!! ");
	LDI        R27, #lo_addr(?lstr1_Lab18_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr1_Lab18_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;Lab18_4.c,67 :: 		Delay_ms(500);
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_out_error15:
	DEC        R16
	BRNE       L_out_error15
	DEC        R17
	BRNE       L_out_error15
	DEC        R18
	BRNE       L_out_error15
	NOP
;Lab18_4.c,68 :: 		}
L_end_out_error:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_error

_out_welcome:

;Lab18_4.c,69 :: 		void out_welcome(){
;Lab18_4.c,70 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_4.c,71 :: 		Lcd_Out(2, 4, "Press one key");
	LDI        R27, #lo_addr(?lstr2_Lab18_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr2_Lab18_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;Lab18_4.c,72 :: 		Delay_ms(500);
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_out_welcome17:
	DEC        R16
	BRNE       L_out_welcome17
	DEC        R17
	BRNE       L_out_welcome17
	DEC        R18
	BRNE       L_out_welcome17
	NOP
;Lab18_4.c,73 :: 		}
L_end_out_welcome:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_welcome

_init:

;Lab18_4.c,74 :: 		void  init( )                                               // ������� �������������
;Lab18_4.c,76 :: 		DDRB = 0x00;
	PUSH       R2
	LDI        R27, 0
	OUT        DDRB+0, R27
;Lab18_4.c,77 :: 		PORTB = 0x07;
	LDI        R27, 7
	OUT        PORTB+0, R27
;Lab18_4.c,79 :: 		Lcd_Init( );                                          // ������������� ������ ���
	CALL       _Lcd_Init+0
;Lab18_4.c,80 :: 		Lcd_Cmd(_LCD_CLEAR);
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_4.c,81 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	LDI        R27, 12
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_4.c,82 :: 		}
L_end_init:
	POP        R2
	RET
; end of _init

_get_volts:
	PUSH       R28
	PUSH       R29
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 10
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;Lab18_4.c,83 :: 		void  get_volts(char  channel)         // ������� ��������� ����������
;Lab18_4.c,87 :: 		res_ADC = ADC_Read(channel);                                // ������ ���� ���
	CALL       _ADC_Read+0
;Lab18_4.c,88 :: 		mvolts = ((long)res_ADC  * 5000) / 0x03FF;     // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
	STD        Y+4, R16
	STD        Y+5, R17
;Lab18_4.c,93 :: 		volts[0] = mvolts / 1000;                 // ���������� ������ �����
	MOVW       R20, R28
	STD        Y+8, R20
	STD        Y+9, R21
	LDI        R20, 232
	LDI        R21, 3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_4.c,94 :: 		volts[1] = (mvolts / 100) % 10;      // ���������� ������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 255
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_4.c,95 :: 		volts[2] = (mvolts / 10) % 10;        // ���������� ����� ����� �����
	MOVW       R16, R28
	SUBI       R16, 254
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_4.c,96 :: 		volts[3] = mvolts % 10;                  // ���������� �������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 253
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_4.c,97 :: 		for(j = 0; j < 4; j++)                 // ������ ���� ���������� � ��� �������
; j start address is: 20 (R20)
	LDI        R20, 0
; j end address is: 20 (R20)
L_get_volts19:
; j start address is: 20 (R20)
	CPI        R20, 4
	BRLO       L__get_volts32
	JMP        L_get_volts20
L__get_volts32:
;Lab18_4.c,98 :: 		displayRAM[j] = volts[j];
	LDI        R16, #lo_addr(_displayRAM+0)
	LDI        R17, hi_addr(_displayRAM+0)
	MOV        R18, R20
	LDI        R19, 0
	ADD        R18, R16
	ADC        R19, R17
	MOVW       R16, R28
	MOV        R30, R20
	LDI        R31, 0
	ADD        R30, R16
	ADC        R31, R17
	LD         R16, Z
	MOVW       R30, R18
	ST         Z, R16
;Lab18_4.c,97 :: 		for(j = 0; j < 4; j++)                 // ������ ���� ���������� � ��� �������
	MOV        R16, R20
	SUBI       R16, 255
; j end address is: 20 (R20)
; j start address is: 16 (R16)
;Lab18_4.c,98 :: 		displayRAM[j] = volts[j];
	MOV        R20, R16
; j end address is: 16 (R16)
	JMP        L_get_volts19
L_get_volts20:
;Lab18_4.c,100 :: 		}
L_end_get_volts:
	ADIW       R28, 9
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	POP        R29
	POP        R28
	RET
; end of _get_volts

_out_display1:

;Lab18_4.c,102 :: 		void  out_display1( )                            // ������� ������ ���������� �� ���
;Lab18_4.c,104 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_4.c,105 :: 		Lcd_Out(2, 4, "U1 = ");
	LDI        R27, #lo_addr(?lstr3_Lab18_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr3_Lab18_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;Lab18_4.c,106 :: 		Lcd_Chr_Cp(48 + displayRAM[0]);   // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,107 :: 		Lcd_Chr_Cp('.');                                // ����������� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,108 :: 		Lcd_Chr_Cp(48 + displayRAM[1]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,109 :: 		Lcd_Chr_Cp(48 + displayRAM[2]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,110 :: 		Lcd_Chr_Cp(48 + displayRAM[3]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,111 :: 		Lcd_Out_Cp(" V");
	LDI        R27, #lo_addr(?lstr4_Lab18_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr4_Lab18_4+0)
	MOV        R3, R27
	CALL       _Lcd_Out_CP+0
;Lab18_4.c,112 :: 		}
L_end_out_display1:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_display1

_out_display2:

;Lab18_4.c,114 :: 		void  out_display2( )                            // ������� ������ ���������� �� ���
;Lab18_4.c,116 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_4.c,117 :: 		Lcd_Out(2, 4, "U2 = ");
	LDI        R27, #lo_addr(?lstr5_Lab18_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr5_Lab18_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;Lab18_4.c,118 :: 		Lcd_Chr_Cp(48 + displayRAM[0]);   // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,119 :: 		Lcd_Chr_Cp('.');                                // ����������� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,120 :: 		Lcd_Chr_Cp(48 + displayRAM[1]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,121 :: 		Lcd_Chr_Cp(48 + displayRAM[2]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,122 :: 		Lcd_Chr_Cp(48 + displayRAM[3]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,123 :: 		Lcd_Out_Cp(" V");
	LDI        R27, #lo_addr(?lstr6_Lab18_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr6_Lab18_4+0)
	MOV        R3, R27
	CALL       _Lcd_Out_CP+0
;Lab18_4.c,124 :: 		}
L_end_out_display2:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_display2

_out_display3:

;Lab18_4.c,126 :: 		void  out_display3( )                            // ������� ������ ���������� �� ���
;Lab18_4.c,128 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_4.c,129 :: 		Lcd_Out(2, 4, "U3 = ");
	LDI        R27, #lo_addr(?lstr7_Lab18_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr7_Lab18_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;Lab18_4.c,130 :: 		Lcd_Chr_Cp(48 + displayRAM[0]);   // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,131 :: 		Lcd_Chr_Cp('.');                                // ����������� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,132 :: 		Lcd_Chr_Cp(48 + displayRAM[1]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,133 :: 		Lcd_Chr_Cp(48 + displayRAM[2]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,134 :: 		Lcd_Chr_Cp(48 + displayRAM[3]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_4.c,135 :: 		Lcd_Out_Cp(" V");
	LDI        R27, #lo_addr(?lstr8_Lab18_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr8_Lab18_4+0)
	MOV        R3, R27
	CALL       _Lcd_Out_CP+0
;Lab18_4.c,136 :: 		}
L_end_out_display3:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_display3
