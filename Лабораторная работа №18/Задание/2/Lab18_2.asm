
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab18_2.c,26 :: 		void  main( )
;Lab18_2.c,28 :: 		init( );                                   // ����� ������� �������������
	PUSH       R2
	CALL       _init+0
;Lab18_2.c,29 :: 		while(1)
L_main0:
;Lab18_2.c,31 :: 		get_volts(channel);         // ����� ������� ��������� ����������
	LDS        R2, _channel+0
	CALL       _get_volts+0
;Lab18_2.c,32 :: 		out_display( );                // ����� ������� ������ �� ���
	CALL       _out_display+0
;Lab18_2.c,33 :: 		Delay_ms(1000);           // �������� �� 1  �
	LDI        R18, 51
	LDI        R17, 187
	LDI        R16, 224
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
	NOP
;Lab18_2.c,34 :: 		}
	JMP        L_main0
;Lab18_2.c,35 :: 		}
L_end_main:
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main

_init:

;Lab18_2.c,37 :: 		void  init( )                                               // ������� �������������
;Lab18_2.c,39 :: 		Lcd_Init( );                                          // ������������� ������ ���
	PUSH       R2
	CALL       _Lcd_Init+0
;Lab18_2.c,40 :: 		Lcd_Cmd(_LCD_CLEAR);
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_2.c,41 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	LDI        R27, 12
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_2.c,42 :: 		}
L_end_init:
	POP        R2
	RET
; end of _init

_get_volts:
	PUSH       R28
	PUSH       R29
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 10
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;Lab18_2.c,43 :: 		void  get_volts(char  channel)         // ������� ��������� ����������
;Lab18_2.c,47 :: 		res_ADC = ADC_Read(0);                                // ������ ���� ���
	PUSH       R2
	CLR        R2
	CALL       _ADC_Read+0
;Lab18_2.c,48 :: 		mvolts = ((long)res_ADC  * 5000) / 0x03FF;     // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
	STD        Y+4, R16
	STD        Y+5, R17
;Lab18_2.c,53 :: 		volts[0] = mvolts / 1000;                 // ���������� ������ �����
	MOVW       R20, R28
	STD        Y+8, R20
	STD        Y+9, R21
	LDI        R20, 232
	LDI        R21, 3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_2.c,54 :: 		volts[1] = (mvolts / 100) % 10;      // ���������� ������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 255
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_2.c,55 :: 		volts[2] = (mvolts / 10) % 10;        // ���������� ����� ����� �����
	MOVW       R16, R28
	SUBI       R16, 254
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_2.c,56 :: 		volts[3] = mvolts % 10;                  // ���������� �������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 253
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;Lab18_2.c,57 :: 		for(j = 0; j < 4; j++)                 // ������ ���� ���������� � ��� �������
; j start address is: 20 (R20)
	LDI        R20, 0
; j end address is: 20 (R20)
L_get_volts4:
; j start address is: 20 (R20)
	CPI        R20, 4
	BRLO       L__get_volts11
	JMP        L_get_volts5
L__get_volts11:
;Lab18_2.c,58 :: 		displayRAM[j] = volts[j];
	LDI        R16, #lo_addr(_displayRAM+0)
	LDI        R17, hi_addr(_displayRAM+0)
	MOV        R18, R20
	LDI        R19, 0
	ADD        R18, R16
	ADC        R19, R17
	MOVW       R16, R28
	MOV        R30, R20
	LDI        R31, 0
	ADD        R30, R16
	ADC        R31, R17
	LD         R16, Z
	MOVW       R30, R18
	ST         Z, R16
;Lab18_2.c,57 :: 		for(j = 0; j < 4; j++)                 // ������ ���� ���������� � ��� �������
	MOV        R16, R20
	SUBI       R16, 255
; j end address is: 20 (R20)
; j start address is: 16 (R16)
;Lab18_2.c,58 :: 		displayRAM[j] = volts[j];
	MOV        R20, R16
; j end address is: 16 (R16)
	JMP        L_get_volts4
L_get_volts5:
;Lab18_2.c,60 :: 		}
L_end_get_volts:
	POP        R2
	ADIW       R28, 9
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	POP        R29
	POP        R28
	RET
; end of _get_volts

_out_display:

;Lab18_2.c,62 :: 		void  out_display( )                            // ������� ������ ���������� �� ���
;Lab18_2.c,64 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;Lab18_2.c,65 :: 		Lcd_Out(2, 4, "U = ");
	LDI        R27, #lo_addr(?lstr1_Lab18_2+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr1_Lab18_2+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;Lab18_2.c,66 :: 		Lcd_Chr_Cp(48 + displayRAM[0]);   // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_2.c,67 :: 		Lcd_Chr_Cp('.');                                // ����������� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _Lcd_Chr_CP+0
;Lab18_2.c,68 :: 		Lcd_Chr_Cp(48 + displayRAM[1]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_2.c,69 :: 		Lcd_Chr_Cp(48 + displayRAM[2]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_2.c,70 :: 		Lcd_Chr_Cp(48 + displayRAM[3]);  // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;Lab18_2.c,71 :: 		Lcd_Out_Cp(" V");
	LDI        R27, #lo_addr(?lstr2_Lab18_2+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr2_Lab18_2+0)
	MOV        R3, R27
	CALL       _Lcd_Out_CP+0
;Lab18_2.c,72 :: 		}
L_end_out_display:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_display
