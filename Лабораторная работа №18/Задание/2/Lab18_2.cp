#line 1 "D:/���� ��. �.�. ������/6 ������� (�� - 31)/���/������������ ������/������������ ������ �18/�������/2/Lab18_2.c"
#line 5 "D:/���� ��. �.�. ������/6 ������� (�� - 31)/���/������������ ������/������������ ������ �18/�������/2/Lab18_2.c"
sbit LCD_RS at PORTC2_bit;
sbit LCD_EN at PORTC3_bit;
sbit LCD_D4 at PORTC4_bit;
sbit LCD_D5 at PORTC5_bit;
sbit LCD_D6 at PORTC6_bit;
sbit LCD_D7 at PORTC7_bit;

sbit LCD_RS_Direction at DDC2_bit;
sbit LCD_EN_Direction at DDC3_bit;
sbit LCD_D4_Direction at DDC4_bit;
sbit LCD_D5_Direction at DDC5_bit;
sbit LCD_D6_Direction at DDC6_bit;
sbit LCD_D7_Direction at DDC7_bit;

char displayRAM[4];
char channel = 0;

void init( );
void get_volts(char channel);
void out_display( );

void main( )
{
 init( );
 while(1)
 {
 get_volts(channel);
 out_display( );
 Delay_ms(1000);
 }
}

void init( )
{
 Lcd_Init( );
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);
}
 void get_volts(char channel)
{
 int res_ADC;
 int mvolts;
 res_ADC = ADC_Read(0);
 mvolts = ((long)res_ADC * 5000) / 0x03FF;

 {
 char volts[4];
 char j;
 volts[0] = mvolts / 1000;
 volts[1] = (mvolts / 100) % 10;
 volts[2] = (mvolts / 10) % 10;
 volts[3] = mvolts % 10;
 for(j = 0; j < 4; j++)
 displayRAM[j] = volts[j];
 }
}

void out_display( )
{
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(2, 4, "U = ");
 Lcd_Chr_Cp(48 + displayRAM[0]);
 Lcd_Chr_Cp('.');
 Lcd_Chr_Cp(48 + displayRAM[1]);
 Lcd_Chr_Cp(48 + displayRAM[2]);
 Lcd_Chr_Cp(48 + displayRAM[3]);
 Lcd_Out_Cp(" V");
}
