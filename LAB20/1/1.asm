
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;1.c,4 :: 		void main( )
;1.c,6 :: 		UART1_Init(9600);                                           // �������������  UART
	PUSH       R2
	PUSH       R3
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;1.c,7 :: 		UART1_Write_Text( "Hello, student!");                       // ����� ������ � UART
	LDI        R27, #lo_addr(?lstr1_1+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_1+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;1.c,8 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
