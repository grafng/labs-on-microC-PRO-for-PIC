#line 1 "C:/Users/�����/Desktop/LAB20/10/10.c"
#line 5 "C:/Users/�����/Desktop/LAB20/10/10.c"
char volts[4];
char channel;
void get_volts(char channel);
void out_UART( );

void uart_ISR( ) org IVT_ADDR_USART_RXC
{
 char symbol;
 symbol = UART1_Read( );
 UART1_Write(symbol);
 out_UART( );
}

void main( )
{
 UART1_Init(9600);
 RXCIE_bit = 1;
 SREG_I_bit = 1;
 while(1)
 {
 RXCIE_bit = 0;
 get_volts(0);

 RXCIE_bit = 1;
 Delay_ms(2000);
 }
}

void get_volts(char channel)
{
 int code_ADC;
 int mvolts;
 code_ADC = ADC_Read(channel);
 mvolts = ((long)code_ADC * 5000) / 0x03FF;

 volts[0] = mvolts / 1000;
 volts[1] = (mvolts / 100) % 10;
 volts[2] = (mvolts / 10) % 10;
 volts[3] = mvolts % 10;
}

void out_UART( )
{
 UART1_Write_Text("U = ");
 UART1_Write(48 + volts[0]);
 UART1_Write('.');
 UART1_Write(48 + volts[1]);
 UART1_Write(48 + volts[2]);
 UART1_Write(48 + volts[3]);
 UART1_Write_Text("  V");
 UART1_Write(13);
}
