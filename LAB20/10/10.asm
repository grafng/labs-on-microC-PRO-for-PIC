
_uart_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;10.c,10 :: 		void  uart_ISR( )  org  IVT_ADDR_USART_RXC  // ���������� ����������
;10.c,13 :: 		symbol = UART1_Read( );              // ������ ������� �� UART
	PUSH       R2
	CALL       _UART1_Read+0
;10.c,14 :: 		UART1_Write(symbol);                  // ���
	MOV        R2, R16
	CALL       _UART1_Write+0
;10.c,15 :: 		out_UART( );                                   // ����� ������� ������ � UART
	CALL       _out_UART+0
;10.c,16 :: 		}
L_end_uart_ISR:
	POP        R2
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _uart_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;10.c,18 :: 		void  main( )
;10.c,20 :: 		UART1_Init(9600);
	PUSH       R2
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;10.c,21 :: 		RXCIE_bit = 1;               // ��������� ���������� �� ��������� UART
	IN         R27, RXCIE_bit+0
	SBR        R27, BitMask(RXCIE_bit+0)
	OUT        RXCIE_bit+0, R27
;10.c,22 :: 		SREG_I_bit = 1;             // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;10.c,23 :: 		while(1)
L_main0:
;10.c,25 :: 		RXCIE_bit = 0;                     // ��������� ����������
	IN         R27, RXCIE_bit+0
	CBR        R27, BitMask(RXCIE_bit+0)
	OUT        RXCIE_bit+0, R27
;10.c,26 :: 		get_volts(0);                          // ����� ������� ��������� ����������
	CLR        R2
	CALL       _get_volts+0
;10.c,28 :: 		RXCIE_bit = 1;                    // ��������� ����������
	IN         R27, RXCIE_bit+0
	SBR        R27, BitMask(RXCIE_bit+0)
	OUT        RXCIE_bit+0, R27
;10.c,29 :: 		Delay_ms(2000);
	LDI        R18, 82
	LDI        R17, 43
	LDI        R16, 0
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
	NOP
	NOP
	NOP
;10.c,30 :: 		}
	JMP        L_main0
;10.c,31 :: 		}
L_end_main:
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main

_get_volts:
	PUSH       R28
	PUSH       R29
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 2
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;10.c,33 :: 		void  get_volts(char  channel)               // ������� ��������� ����������
;10.c,37 :: 		code_ADC = ADC_Read(channel);                                // ������ ���� ���
	CALL       _ADC_Read+0
;10.c,38 :: 		mvolts = ((long)code_ADC  * 5000) / 0x03FF;     // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
	STD        Y+0, R16
	STD        Y+1, R17
;10.c,40 :: 		volts[0] = mvolts / 1000;                 // ���������� ������ �����
	LDI        R20, 232
	LDI        R21, 3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	STS        _volts+0, R16
;10.c,41 :: 		volts[1] = (mvolts / 100) % 10;      // ���������� ������� ����� ������
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	STS        _volts+1, R16
;10.c,42 :: 		volts[2] = (mvolts / 10) % 10;        // ���������� ����� ����� ������
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	STS        _volts+2, R16
;10.c,43 :: 		volts[3] = mvolts % 10;                  // ���������� �������� ����� ������
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	STS        _volts+3, R16
;10.c,44 :: 		}
L_end_get_volts:
	ADIW       R28, 1
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	POP        R29
	POP        R28
	RET
; end of _get_volts

_out_UART:

;10.c,46 :: 		void  out_UART( )                       // ������� ������ ���������� � UART
;10.c,48 :: 		UART1_Write_Text("U = ");
	PUSH       R2
	PUSH       R3
	LDI        R27, #lo_addr(?lstr1_10+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_10+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;10.c,49 :: 		UART1_Write(48 + volts[0]);   // ����� ������ ����� (� ���� ASCII)
	LDS        R16, _volts+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;10.c,50 :: 		UART1_Write('.');                             // ����� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _UART1_Write+0
;10.c,51 :: 		UART1_Write(48 + volts[1]);  // ����� ����� � ���� ASCII
	LDS        R16, _volts+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;10.c,52 :: 		UART1_Write(48 + volts[2]);  // ����� ����� � ���� ASCII
	LDS        R16, _volts+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;10.c,53 :: 		UART1_Write(48 + volts[3]);  // ����� ����� � ���� ASCII
	LDS        R16, _volts+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;10.c,54 :: 		UART1_Write_Text("  V");
	LDI        R27, #lo_addr(?lstr2_10+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr2_10+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;10.c,55 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;10.c,56 :: 		}
L_end_out_UART:
	POP        R3
	POP        R2
	RET
; end of _out_UART
