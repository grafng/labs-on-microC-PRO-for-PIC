
_uart_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;7.c,5 :: 		void  uart_ISR( )  org  IVT_ADDR_USART_RXC   // ���������� ����������
;7.c,8 :: 		symbol = UART1_Read( );                            // ������ ��������� �������
	PUSH       R2
	PUSH       R3
	CALL       _UART1_Read+0
;7.c,9 :: 		UART1_Write(symbol);                                // ���
	MOV        R2, R16
	CALL       _UART1_Write+0
;7.c,10 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;7.c,11 :: 		UART1_Write_Text("Hello!");                    // ����� ����� �� ��������
	LDI        R27, #lo_addr(?lstr1_7+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_7+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;7.c,12 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;7.c,13 :: 		}
L_end_uart_ISR:
	POP        R3
	POP        R2
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _uart_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;7.c,14 :: 		void  main( )
;7.c,16 :: 		UART1_Init(9600);
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;7.c,17 :: 		RXCIE_bit = 1;             // ��������� ���������� �� ��������� UART
	IN         R27, RXCIE_bit+0
	SBR        R27, BitMask(RXCIE_bit+0)
	OUT        RXCIE_bit+0, R27
;7.c,18 :: 		SREG_I_bit = 1;           // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;7.c,19 :: 		while(1);                       // ������������ ���������
L_main0:
	JMP        L_main0
;7.c,20 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
