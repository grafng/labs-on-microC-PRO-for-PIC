
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;9.c,9 :: 		void  main( )
;9.c,11 :: 		UART1_Init(9600);
	PUSH       R2
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;9.c,12 :: 		while(1)
L_main0:
;9.c,14 :: 		get_volts(0);                    // ����� ������� ��������� ����������
	CLR        R2
	CALL       _get_volts+0
;9.c,15 :: 		out_UART( );                 // ����� ������� ������ � UART
	CALL       _out_UART+0
;9.c,16 :: 		Delay_ms(2000);            // �������� �� 2  �
	LDI        R18, 82
	LDI        R17, 43
	LDI        R16, 0
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
	NOP
	NOP
	NOP
;9.c,17 :: 		}
	JMP        L_main0
;9.c,18 :: 		}
L_end_main:
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main

_get_volts:
	PUSH       R28
	PUSH       R29
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 2
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;9.c,20 :: 		void  get_volts(char  channel)               // ������� ��������� ����������
;9.c,24 :: 		code_ADC = ADC_Read(channel);                                // ������ ���� ���
	CALL       _ADC_Read+0
;9.c,25 :: 		mvolts = ((long)code_ADC  * 5000) / 0x03FF;     // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
	STD        Y+0, R16
	STD        Y+1, R17
;9.c,27 :: 		volts[0] = mvolts / 1000;                 // ���������� ������ �����
	LDI        R20, 232
	LDI        R21, 3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	STS        _volts+0, R16
;9.c,28 :: 		volts[1] = (mvolts / 100) % 10;      // ���������� ������� ����� ������
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	STS        _volts+1, R16
;9.c,29 :: 		volts[2] = (mvolts / 10) % 10;        // ���������� ����� ����� ������
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	STS        _volts+2, R16
;9.c,30 :: 		volts[3] = mvolts % 10;                  // ���������� �������� ����� ������
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	STS        _volts+3, R16
;9.c,31 :: 		}
L_end_get_volts:
	ADIW       R28, 1
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	POP        R29
	POP        R28
	RET
; end of _get_volts

_out_UART:

;9.c,33 :: 		void  out_UART( )                       // ������� ������ ���������� � UART
;9.c,35 :: 		UART1_Write_Text("U = ");
	PUSH       R2
	PUSH       R3
	LDI        R27, #lo_addr(?lstr1_9+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_9+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;9.c,36 :: 		UART1_Write(48 + volts[0]);   // ����� ������ ����� (� ���� ASCII)
	LDS        R16, _volts+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;9.c,37 :: 		UART1_Write('.');                             // ����� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _UART1_Write+0
;9.c,38 :: 		UART1_Write(48 + volts[1]);  // ����� ����� � ���� ASCII
	LDS        R16, _volts+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;9.c,39 :: 		UART1_Write(48 + volts[2]);  // ����� ����� � ���� ASCII
	LDS        R16, _volts+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;9.c,40 :: 		UART1_Write(48 + volts[3]);  // ����� ����� � ���� ASCII
	LDS        R16, _volts+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _UART1_Write+0
;9.c,41 :: 		UART1_Write_Text("  V");
	LDI        R27, #lo_addr(?lstr2_9+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr2_9+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;9.c,42 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;9.c,43 :: 		}
L_end_out_UART:
	POP        R3
	POP        R2
	RET
; end of _out_UART
