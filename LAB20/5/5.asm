
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;5.c,4 :: 		void  main( )
;5.c,7 :: 		UART1_Init(9600);
	PUSH       R2
	PUSH       R3
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;5.c,8 :: 		while(1)
L_main0:
;5.c,10 :: 		UART1_Write_Text("To receive message press key A.");
	LDI        R27, #lo_addr(?lstr1_5+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_5+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;5.c,11 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;5.c,12 :: 		Delay_ms(2000);
	LDI        R18, 82
	LDI        R17, 43
	LDI        R16, 0
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
	NOP
	NOP
	NOP
;5.c,13 :: 		if(UART1_Data_Ready() == 1)                           // ���� � UART �������� ������
	CALL       _UART1_Data_Ready+0
	CPI        R16, 1
	BREQ       L__main8
	JMP        L_main4
L__main8:
;5.c,15 :: 		symbol = UART1_Read( );
	CALL       _UART1_Read+0
;5.c,16 :: 		UART1_Write(symbol);                             // ��� - ����� � UART ��������� �������
	MOV        R2, R16
	CALL       _UART1_Write+0
;5.c,17 :: 		UART1_Write(13);                                 // ������� ������� �� ������ ����� ������
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;5.c,18 :: 		UART1_Write_Text("Hello, student!");
	LDI        R27, #lo_addr(?lstr2_5+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr2_5+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;5.c,19 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;5.c,20 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_main5:
	DEC        R16
	BRNE       L_main5
	DEC        R17
	BRNE       L_main5
	DEC        R18
	BRNE       L_main5
	NOP
;5.c,21 :: 		}
L_main4:
;5.c,22 :: 		}
	JMP        L_main0
;5.c,23 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
