
_uart_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;8.c,5 :: 		void  uart_ISR( )  org  IVT_ADDR_USART_RXC               // ���������� ����������
;8.c,8 :: 		if(UART1_Data_Ready( ) == 1)
	PUSH       R2
	PUSH       R3
	CALL       _UART1_Data_Ready+0
	CPI        R16, 1
	BREQ       L__uart_ISR15
	JMP        L_uart_ISR0
L__uart_ISR15:
;8.c,10 :: 		symbol = UART1_Read( );
	CALL       _UART1_Read+0
; symbol start address is: 17 (R17)
	MOV        R17, R16
;8.c,11 :: 		UART1_Write(symbol);                                 // ���
	MOV        R2, R16
	CALL       _UART1_Write+0
;8.c,12 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;8.c,13 :: 		if(symbol == 'A' || symbol == 'a')                   // ���� ������ "����������"
	CPI        R17, 65
	BRNE       L__uart_ISR16
	JMP        L__uart_ISR13
L__uart_ISR16:
	CPI        R17, 97
	BRNE       L__uart_ISR17
	JMP        L__uart_ISR12
L__uart_ISR17:
; symbol end address is: 17 (R17)
	JMP        L_uart_ISR3
L__uart_ISR13:
L__uart_ISR12:
;8.c,15 :: 		UART1_Write_Text("Hello, student!");
	LDI        R27, #lo_addr(?lstr1_8+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr1_8+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;8.c,16 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;8.c,17 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_uart_ISR4:
	DEC        R16
	BRNE       L_uart_ISR4
	DEC        R17
	BRNE       L_uart_ISR4
	DEC        R18
	BRNE       L_uart_ISR4
	NOP
;8.c,18 :: 		}
	JMP        L_uart_ISR6
L_uart_ISR3:
;8.c,21 :: 		UART1_Write_Text("Symbol is wrong! Repeat enter.");
	LDI        R27, #lo_addr(?lstr2_8+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr2_8+0)
	MOV        R3, R27
	CALL       _UART1_Write_Text+0
;8.c,22 :: 		UART1_Write(13);
	LDI        R27, 13
	MOV        R2, R27
	CALL       _UART1_Write+0
;8.c,23 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_uart_ISR7:
	DEC        R16
	BRNE       L_uart_ISR7
	DEC        R17
	BRNE       L_uart_ISR7
	DEC        R18
	BRNE       L_uart_ISR7
	NOP
;8.c,24 :: 		}
L_uart_ISR6:
;8.c,25 :: 		}
L_uart_ISR0:
;8.c,26 :: 		}
L_end_uart_ISR:
	POP        R3
	POP        R2
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _uart_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;8.c,27 :: 		void  main( )
;8.c,29 :: 		UART1_Init(9600);
	LDI        R27, 51
	OUT        UBRRL+0, R27
	LDI        R27, 0
	OUT        UBRRH+0, R27
	CALL       _UART1_Init+0
;8.c,30 :: 		RXCIE_bit = 1;                                      // ��������� ���������� �� ��������� UART
	IN         R27, RXCIE_bit+0
	SBR        R27, BitMask(RXCIE_bit+0)
	OUT        RXCIE_bit+0, R27
;8.c,31 :: 		SREG_I_bit = 1;                                     // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;8.c,32 :: 		while(1);                                           // ������������ ���������
L_main9:
	JMP        L_main9
;8.c,33 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
