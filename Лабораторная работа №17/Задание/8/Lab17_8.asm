
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_8.c,4 :: 		void  main( )
;Lab17_8.c,6 :: 		DDB1_bit = 0;                         // ��������� 1-� ����� ����� � �� ����
	IN         R27, DDB1_bit+0
	CBR        R27, BitMask(DDB1_bit+0)
	OUT        DDB1_bit+0, R27
;Lab17_8.c,7 :: 		PORTB1_bit = 1;  // ���������� ������������� �������� � ������ PB1
	IN         R27, PORTB1_bit+0
	SBR        R27, BitMask(PORTB1_bit+0)
	OUT        PORTB1_bit+0, R27
;Lab17_8.c,8 :: 		DDC0_bit = 1;                         // ��������� 1-� ����� ����� � �� �����
	IN         R27, DDC0_bit+0
	SBR        R27, BitMask(DDC0_bit+0)
	OUT        DDC0_bit+0, R27
;Lab17_8.c,9 :: 		PORTC1_bit = 0;
	IN         R27, PORTC1_bit+0
	CBR        R27, BitMask(PORTC1_bit+0)
	OUT        PORTC1_bit+0, R27
;Lab17_8.c,10 :: 		while(1)                              // ����������� ���� ������
L_main0:
;Lab17_8.c,12 :: 		if(PINB.B1 == 0)          // ���� ������� ������ SB1 �������, ��
	IN         R27, PINB+0
	SBRC       R27, 1
	JMP        L_main2
;Lab17_8.c,13 :: 		PORTC.B0 = 1;     // ������ ���������
	IN         R27, PORTC+0
	SBR        R27, 1
	OUT        PORTC+0, R27
	JMP        L_main3
L_main2:
;Lab17_8.c,15 :: 		PORTC.B0 = 0;      // �������� ���������
	IN         R27, PORTC+0
	CBR        R27, 1
	OUT        PORTC+0, R27
L_main3:
;Lab17_8.c,16 :: 		}
	JMP        L_main0
;Lab17_8.c,17 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
