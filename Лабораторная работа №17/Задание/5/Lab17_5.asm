
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_5.c,4 :: 		void  main( )
;Lab17_5.c,6 :: 		DDC0_bit = 1;                // ��������� 0-� ����� ����� � �� �����
	IN         R27, DDC0_bit+0
	SBR        R27, BitMask(DDC0_bit+0)
	OUT        DDC0_bit+0, R27
;Lab17_5.c,7 :: 		PORTC0_bit = 0;           // �������� ��������� D1
	IN         R27, PORTC0_bit+0
	CBR        R27, BitMask(PORTC0_bit+0)
	OUT        PORTC0_bit+0, R27
;Lab17_5.c,8 :: 		while(1)
L_main0:
;Lab17_5.c,10 :: 		PORTC0_bit = 1;        // ������ D1
	IN         R27, PORTC0_bit+0
	SBR        R27, BitMask(PORTC0_bit+0)
	OUT        PORTC0_bit+0, R27
;Lab17_5.c,11 :: 		Delay_ms(500);
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
;Lab17_5.c,12 :: 		PORTC0_bit = 0;        // �������� D1
	IN         R27, PORTC0_bit+0
	CBR        R27, BitMask(PORTC0_bit+0)
	OUT        PORTC0_bit+0, R27
;Lab17_5.c,13 :: 		Delay_ms(500);
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_main4:
	DEC        R16
	BRNE       L_main4
	DEC        R17
	BRNE       L_main4
	DEC        R18
	BRNE       L_main4
	NOP
;Lab17_5.c,14 :: 		}
	JMP        L_main0
;Lab17_5.c,15 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
