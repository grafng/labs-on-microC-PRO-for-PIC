
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_3.c,1 :: 		void main() {
;Lab17_3.c,3 :: 		while(1){
L_main0:
;Lab17_3.c,4 :: 		DDRD = 0xFF; // ��������� ����� D �� �����
	LDI        R27, 255
	OUT        DDRD+0, R27
;Lab17_3.c,5 :: 		PORTD = 0;              // ����� � ���� D ��� 0
	LDI        R27, 0
	OUT        PORTD+0, R27
;Lab17_3.c,6 :: 		PORTD &= 0b00000111;   // �������� ������� �������
	LDI        R27, 0
	OUT        PORTD+0, R27
;Lab17_3.c,7 :: 		PORTD ^=0b010101100; // ������������� 2,3,5,7 �������
	LDI        R27, 172
	OUT        PORTD+0, R27
;Lab17_3.c,8 :: 		PORTD.B5 = 1; // ������� � ����� 1,6,7  - �������
	IN         R27, PORTD+0
	SBR        R27, 32
	OUT        PORTD+0, R27
;Lab17_3.c,9 :: 		PORTD.B6 = 1;
	IN         R27, PORTD+0
	SBR        R27, 64
	OUT        PORTD+0, R27
;Lab17_3.c,10 :: 		PORTD.B0 = 1;
	IN         R27, PORTD+0
	SBR        R27, 1
	OUT        PORTD+0, R27
;Lab17_3.c,11 :: 		PORTD <<= 3;     // ����� ����� �� 3 ����
	IN         R16, PORTD+0
	LSL        R16
	LSL        R16
	LSL        R16
	OUT        PORTD+0, R16
;Lab17_3.c,12 :: 		PORTD &= 0b11101100; //  ��������� 1,2, 5 ���� ����� D
	ANDI       R16, 236
	OUT        PORTD+0, R16
;Lab17_3.c,13 :: 		PORTD &= 0x0F;    // ��������� ������� ��������
	ANDI       R16, 15
	OUT        PORTD+0, R16
;Lab17_3.c,14 :: 		PORTD ^= 0b01101001; // ������������� 1,4,6,7 �������
	LDI        R27, 105
	EOR        R16, R27
	OUT        PORTD+0, R16
;Lab17_3.c,15 :: 		}
	JMP        L_main0
;Lab17_3.c,17 :: 		}
L_end_main:
	JMP        L_end_main
; end of _main
