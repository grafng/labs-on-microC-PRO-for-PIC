
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_2.c,4 :: 		void  main( )
;Lab17_2.c,6 :: 		DDRC = 0xFF;                       // ��������� ��� ����� ����� � �� �����
	LDI        R27, 255
	OUT        DDRC+0, R27
;Lab17_2.c,7 :: 		while(1)
L_main0:
;Lab17_2.c,9 :: 		PORTC = 0b00001111;           // ����� ����� 0x0F � ���� �
	LDI        R27, 15
	OUT        PORTC+0, R27
;Lab17_2.c,10 :: 		PINC = PORTC;                     // ������ ��������� ������� ����� �
	LDI        R27, 15
	OUT        PINC+0, R27
;Lab17_2.c,11 :: 		PORTC = 0b11110000;          // ����� ����� 0xF0 � ���� �
	LDI        R27, 240
	OUT        PORTC+0, R27
;Lab17_2.c,12 :: 		PINC = PORTC;                     // ������ ��������� ������� ����� �
	LDI        R27, 240
	OUT        PINC+0, R27
;Lab17_2.c,13 :: 		}
	JMP        L_main0
;Lab17_2.c,14 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
