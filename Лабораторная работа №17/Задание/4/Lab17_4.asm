
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_4.c,4 :: 		void  main( )
;Lab17_4.c,6 :: 		DDRC.B0 = 1;                      // ��������� 0- ����� ����� � �� �����
	IN         R27, DDRC+0
	SBR        R27, 1
	OUT        DDRC+0, R27
;Lab17_4.c,7 :: 		PORTC.B0 = 0;                    // �������� ��������� D1
	IN         R27, PORTC+0
	CBR        R27, 1
	OUT        PORTC+0, R27
;Lab17_4.c,8 :: 		while(1)                                // ����������� ���� ������
L_main0:
;Lab17_4.c,10 :: 		PORTC.B0 = 1;             // ������ ��������� D1
	IN         R27, PORTC+0
	SBR        R27, 1
	OUT        PORTC+0, R27
;Lab17_4.c,11 :: 		Delay_ms(500);             // �������� �� 500 ��
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
	NOP
;Lab17_4.c,12 :: 		PORTC.B0 = 0;             // �������� ��������� D1
	IN         R27, PORTC+0
	CBR        R27, 1
	OUT        PORTC+0, R27
;Lab17_4.c,13 :: 		Delay_ms(500);             // �������� �� 500 ��
	LDI        R18, 26
	LDI        R17, 94
	LDI        R16, 111
L_main4:
	DEC        R16
	BRNE       L_main4
	DEC        R17
	BRNE       L_main4
	DEC        R18
	BRNE       L_main4
	NOP
;Lab17_4.c,14 :: 		}
	JMP        L_main0
;Lab17_4.c,15 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
