
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_9.c,1 :: 		void main(void)
;Lab17_9.c,3 :: 		DDRB = 0x00;
	PUSH       R2
	PUSH       R3
	LDI        R27, 0
	OUT        DDRB+0, R27
;Lab17_9.c,4 :: 		DDRC = 0x07;
	LDI        R27, 7
	OUT        DDRC+0, R27
;Lab17_9.c,5 :: 		PORTB = 0x07;
	LDI        R27, 7
	OUT        PORTB+0, R27
;Lab17_9.c,6 :: 		while (1)
L_main0:
;Lab17_9.c,8 :: 		switch (PINB) {
	JMP        L_main2
;Lab17_9.c,9 :: 		case 0x07:
L_main4:
;Lab17_9.c,10 :: 		break;
	JMP        L_main3
;Lab17_9.c,11 :: 		case 0x06:
L_main5:
;Lab17_9.c,12 :: 		PORTC = 0x01;
	LDI        R27, 1
	OUT        PORTC+0, R27
;Lab17_9.c,13 :: 		VDelay_ms(50);
	LDI        R27, 50
	MOV        R2, R27
	LDI        R27, 0
	MOV        R3, R27
	CALL       _VDelay_ms+0
;Lab17_9.c,14 :: 		PORTC = 0;
	LDI        R27, 0
	OUT        PORTC+0, R27
;Lab17_9.c,15 :: 		break;
	JMP        L_main3
;Lab17_9.c,16 :: 		case 0x05:
L_main6:
;Lab17_9.c,17 :: 		PORTC = 0x02;
	LDI        R27, 2
	OUT        PORTC+0, R27
;Lab17_9.c,18 :: 		VDelay_ms(50);
	LDI        R27, 50
	MOV        R2, R27
	LDI        R27, 0
	MOV        R3, R27
	CALL       _VDelay_ms+0
;Lab17_9.c,19 :: 		PORTC = 0;
	LDI        R27, 0
	OUT        PORTC+0, R27
;Lab17_9.c,20 :: 		break;
	JMP        L_main3
;Lab17_9.c,21 :: 		case 0x03:
L_main7:
;Lab17_9.c,22 :: 		PORTC = 0x04;
	LDI        R27, 4
	OUT        PORTC+0, R27
;Lab17_9.c,23 :: 		VDelay_ms(50);
	LDI        R27, 50
	MOV        R2, R27
	LDI        R27, 0
	MOV        R3, R27
	CALL       _VDelay_ms+0
;Lab17_9.c,24 :: 		PORTC = 0;
	LDI        R27, 0
	OUT        PORTC+0, R27
;Lab17_9.c,25 :: 		break;
	JMP        L_main3
;Lab17_9.c,26 :: 		default:
L_main8:
;Lab17_9.c,27 :: 		PORTC = 0x07;
	LDI        R27, 7
	OUT        PORTC+0, R27
;Lab17_9.c,28 :: 		VDelay_ms(50);
	LDI        R27, 50
	MOV        R2, R27
	LDI        R27, 0
	MOV        R3, R27
	CALL       _VDelay_ms+0
;Lab17_9.c,29 :: 		PORTC = 0;
	LDI        R27, 0
	OUT        PORTC+0, R27
;Lab17_9.c,30 :: 		break;
	JMP        L_main3
;Lab17_9.c,31 :: 		}
L_main2:
	IN         R16, PINB+0
	CPI        R16, 7
	BRNE       L__main10
	JMP        L_main4
L__main10:
	IN         R16, PINB+0
	CPI        R16, 6
	BRNE       L__main11
	JMP        L_main5
L__main11:
	IN         R16, PINB+0
	CPI        R16, 5
	BRNE       L__main12
	JMP        L_main6
L__main12:
	IN         R16, PINB+0
	CPI        R16, 3
	BRNE       L__main13
	JMP        L_main7
L__main13:
	JMP        L_main8
L_main3:
;Lab17_9.c,32 :: 		}
	JMP        L_main0
;Lab17_9.c,33 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
