
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_1.c,4 :: 		void  main( )
;Lab17_1.c,6 :: 		DDRB = 0xFF;             // ��������� ��� ����� ����� � �� �����
	LDI        R27, 255
	OUT        DDRB+0, R27
;Lab17_1.c,7 :: 		PORTB = 0x00;           // ����� ������ (��� ����) � ���� �
	LDI        R27, 0
	OUT        PORTB+0, R27
;Lab17_1.c,8 :: 		PORTB = 0xFF;           // ����� ������ (��� �������) � ���� �
	LDI        R27, 255
	OUT        PORTB+0, R27
;Lab17_1.c,9 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
