
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab17_6.c,4 :: 		void  main( )
;Lab17_6.c,6 :: 		DDRB.B1= 0;                          // ��������� 1-� ����� ����� B �� ����
	IN         R27, DDRB+0
	CBR        R27, 2
	OUT        DDRB+0, R27
;Lab17_6.c,7 :: 		PORTB.B1 = 1;    // ���������� ������������� �������� � ����� PB1
	IN         R27, PORTB+0
	SBR        R27, 2
	OUT        PORTB+0, R27
;Lab17_6.c,8 :: 		DDRC.B0 = 1;                 // ��������� 1-� ����� ����� � �� �����
	IN         R27, DDRC+0
	SBR        R27, 1
	OUT        DDRC+0, R27
;Lab17_6.c,9 :: 		PORTC.B0 = 0;                       // �������� ���������
	IN         R27, PORTC+0
	CBR        R27, 1
	OUT        PORTC+0, R27
;Lab17_6.c,10 :: 		while(1)                              // ����������� ���� ������
L_main0:
;Lab17_6.c,12 :: 		if(PINB.B1 == 0)          // ���� ������� ������ SB1 �������, ��
	IN         R27, PINB+0
	SBRC       R27, 1
	JMP        L_main2
;Lab17_6.c,13 :: 		PORTC.B0 = 1;     // ������ ���������
	IN         R27, PORTC+0
	SBR        R27, 1
	OUT        PORTC+0, R27
	JMP        L_main3
L_main2:
;Lab17_6.c,15 :: 		PORTC.B0 = 0;      // �������� ���������
	IN         R27, PORTC+0
	CBR        R27, 1
	OUT        PORTC+0, R27
L_main3:
;Lab17_6.c,16 :: 		}
	JMP        L_main0
;Lab17_6.c,17 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
