
_main:

;1.c,6 :: 		void main() {
;1.c,8 :: 		char input = 0;
	CLRF       main_input_L0+0
;1.c,9 :: 		TRISB = 0xFF;
	MOVLW      255
	MOVWF      TRISB+0
;1.c,10 :: 		TRISC = 0;
	CLRF       TRISC+0
;1.c,11 :: 		PORTC = 0;
	CLRF       PORTC+0
;1.c,12 :: 		OPTION_REG.B7 = 0;
	BCF        OPTION_REG+0, 7
;1.c,14 :: 		while(1)
L_main0:
;1.c,17 :: 		input = PORTB;
	MOVF       PORTB+0, 0
	MOVWF      main_input_L0+0
;1.c,20 :: 		switch(input)         // анализ номера переключателя
	GOTO       L_main2
;1.c,24 :: 		case 0b11111110:
L_main4:
;1.c,25 :: 		SB1();
	CALL       _SB1+0
;1.c,26 :: 		break;
	GOTO       L_main3
;1.c,30 :: 		case 0b11111101:
L_main5:
;1.c,31 :: 		SB2();
	CALL       _SB2+0
;1.c,32 :: 		break;
	GOTO       L_main3
;1.c,36 :: 		case 0b11111011:
L_main6:
;1.c,37 :: 		SB3();
	CALL       _SB3+0
;1.c,38 :: 		break;
	GOTO       L_main3
;1.c,42 :: 		case 0b11111000:
L_main7:
;1.c,43 :: 		everything();
	CALL       _everything+0
;1.c,44 :: 		break;
	GOTO       L_main3
;1.c,48 :: 		case 0b11111001:
L_main8:
;1.c,49 :: 		everything();
	CALL       _everything+0
;1.c,50 :: 		break;
	GOTO       L_main3
;1.c,55 :: 		case 0b11111010:
L_main9:
;1.c,56 :: 		everything();
	CALL       _everything+0
;1.c,57 :: 		break;
	GOTO       L_main3
;1.c,61 :: 		case 0b11111100:
L_main10:
;1.c,62 :: 		everything();
	CALL       _everything+0
;1.c,63 :: 		break;
	GOTO       L_main3
;1.c,66 :: 		}
L_main2:
	MOVF       main_input_L0+0, 0
	XORLW      254
	BTFSC      STATUS+0, 2
	GOTO       L_main4
	MOVF       main_input_L0+0, 0
	XORLW      253
	BTFSC      STATUS+0, 2
	GOTO       L_main5
	MOVF       main_input_L0+0, 0
	XORLW      251
	BTFSC      STATUS+0, 2
	GOTO       L_main6
	MOVF       main_input_L0+0, 0
	XORLW      248
	BTFSC      STATUS+0, 2
	GOTO       L_main7
	MOVF       main_input_L0+0, 0
	XORLW      249
	BTFSC      STATUS+0, 2
	GOTO       L_main8
	MOVF       main_input_L0+0, 0
	XORLW      250
	BTFSC      STATUS+0, 2
	GOTO       L_main9
	MOVF       main_input_L0+0, 0
	XORLW      252
	BTFSC      STATUS+0, 2
	GOTO       L_main10
L_main3:
;1.c,67 :: 		}
	GOTO       L_main0
;1.c,68 :: 		}
	GOTO       $+0
; end of _main

_SB1:

;1.c,71 :: 		void SB1()
;1.c,73 :: 		PORTC.B0 = 1;
	BSF        PORTC+0, 0
;1.c,74 :: 		Delay_ms (125);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_SB111:
	DECFSZ     R13+0, 1
	GOTO       L_SB111
	DECFSZ     R12+0, 1
	GOTO       L_SB111
	DECFSZ     R11+0, 1
	GOTO       L_SB111
	NOP
	NOP
;1.c,75 :: 		PORTC.B0 = 0;
	BCF        PORTC+0, 0
;1.c,76 :: 		}
	RETURN
; end of _SB1

_SB2:

;1.c,78 :: 		void SB2()
;1.c,80 :: 		PORTC.B1 = 1;
	BSF        PORTC+0, 1
;1.c,81 :: 		Delay_ms (125);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_SB212:
	DECFSZ     R13+0, 1
	GOTO       L_SB212
	DECFSZ     R12+0, 1
	GOTO       L_SB212
	DECFSZ     R11+0, 1
	GOTO       L_SB212
	NOP
	NOP
;1.c,82 :: 		PORTC.B1 = 0;
	BCF        PORTC+0, 1
;1.c,83 :: 		}
	RETURN
; end of _SB2

_SB3:

;1.c,85 :: 		void SB3()
;1.c,87 :: 		PORTC.B2 = 1;
	BSF        PORTC+0, 2
;1.c,88 :: 		Delay_ms (125);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_SB313:
	DECFSZ     R13+0, 1
	GOTO       L_SB313
	DECFSZ     R12+0, 1
	GOTO       L_SB313
	DECFSZ     R11+0, 1
	GOTO       L_SB313
	NOP
	NOP
;1.c,89 :: 		PORTC.B2 = 0;
	BCF        PORTC+0, 2
;1.c,90 :: 		}
	RETURN
; end of _SB3

_everything:

;1.c,93 :: 		void everything()
;1.c,95 :: 		int i = 1;
	MOVLW      1
	MOVWF      everything_i_L0+0
	MOVLW      0
	MOVWF      everything_i_L0+1
	CLRF       everything_UI_L0+0
;1.c,97 :: 		PORTC = 0xFF;
	MOVLW      255
	MOVWF      PORTC+0
;1.c,98 :: 		Delay_ms (1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_everything14:
	DECFSZ     R13+0, 1
	GOTO       L_everything14
	DECFSZ     R12+0, 1
	GOTO       L_everything14
	DECFSZ     R11+0, 1
	GOTO       L_everything14
	NOP
	NOP
;1.c,99 :: 		PORTC = 0;
	CLRF       PORTC+0
;1.c,100 :: 		Delay_ms (100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_everything15:
	DECFSZ     R13+0, 1
	GOTO       L_everything15
	DECFSZ     R12+0, 1
	GOTO       L_everything15
	DECFSZ     R11+0, 1
	GOTO       L_everything15
	NOP
;1.c,102 :: 		while(i)
L_everything16:
	MOVF       everything_i_L0+0, 0
	IORWF      everything_i_L0+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_everything17
;1.c,105 :: 		UI = PORTB;
	MOVF       PORTB+0, 0
	MOVWF      everything_UI_L0+0
;1.c,108 :: 		switch(UI)         // анализ номера переключателя
	GOTO       L_everything18
;1.c,110 :: 		case 0b11111111:
L_everything20:
;1.c,111 :: 		i = 0;
	CLRF       everything_i_L0+0
	CLRF       everything_i_L0+1
;1.c,112 :: 		break;
	GOTO       L_everything19
;1.c,113 :: 		}
L_everything18:
	MOVF       everything_UI_L0+0, 0
	XORLW      255
	BTFSC      STATUS+0, 2
	GOTO       L_everything20
L_everything19:
;1.c,116 :: 		}
	GOTO       L_everything16
L_everything17:
;1.c,118 :: 		}
	RETURN
; end of _everything
