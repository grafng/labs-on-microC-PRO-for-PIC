
_main:

;5.c,34 :: 		void  main( )
;5.c,38 :: 		char poss = 5;
	MOVLW      5
	MOVWF      main_poss_L0+0
;5.c,41 :: 		Lcd_Init( );
	CALL       _Lcd_Init+0
;5.c,42 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;5.c,43 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;5.c,44 :: 		Keypad_Init( );
	CALL       _Keypad_Init+0
;5.c,46 :: 		num[0] = 0;
	CLRF       _num+0
;5.c,47 :: 		num[1] = 0;
	CLRF       _num+1
;5.c,48 :: 		num[2] = 0;
	CLRF       _num+2
;5.c,49 :: 		num[3] = 0;
	CLRF       _num+3
;5.c,51 :: 		while(1)
L_main0:
;5.c,53 :: 		LCD_Cmd(_LCD_CLEAR);                // i?enoeou ye?ai
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;5.c,54 :: 		Lcd_Out(1,2,"Enter password");      // oaeno "i?eaeaoaiey" aey
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,55 :: 		Lcd_Out(2,4,"then click #");        // aaiaa ia?iey
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,56 :: 		Delay_ms(250);                      // caaa??ea 2 n
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;5.c,57 :: 		LCD_Cmd(_LCD_CLEAR);                // i?enoeou ye?ai
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;5.c,59 :: 		for(j = 0; j < 4; j++)              // oeee aaiaa ?enae ia?iey
	CLRF       main_j_L0+0
L_main3:
	MOVLW      4
	SUBWF      main_j_L0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main4
;5.c,61 :: 		num_key = 0;                  // iaioeeo ia?aiaiio?
	CLRF       _num_key+0
;5.c,62 :: 		do
L_main6:
;5.c,64 :: 		num_key = Keypad_Key_Click( );
	CALL       _Keypad_Key_Click+0
	MOVF       R0+0, 0
	MOVWF      _num_key+0
;5.c,67 :: 		while(!num_key);
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main6
;5.c,70 :: 		num[j] = num_key;                  // i?enaieou yeaiaioo ianneaa cia?aiea ia?aoie eeaaeoe
	MOVF       main_j_L0+0, 0
	ADDLW      _num+0
	MOVWF      FSR
	MOVF       _num_key+0, 0
	MOVWF      INDF+0
;5.c,71 :: 		Lcd_Out(2, poss, "*");             // auaanoe '*'
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVF       main_poss_L0+0, 0
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,72 :: 		poss +=2;                          // oaaee?eou cia?aiea iiceoee ia 2, ?oiau iieo?eou i?iaae ia?ao *
	MOVLW      2
	ADDWF      main_poss_L0+0, 1
;5.c,59 :: 		for(j = 0; j < 4; j++)              // oeee aaiaa ?enae ia?iey
	INCF       main_j_L0+0, 1
;5.c,75 :: 		}
	GOTO       L_main3
L_main4:
;5.c,77 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
	NOP
;5.c,79 :: 		if (num[0] == number1 && num[1] == number2 && num[2] == number3 && num[3] == number4)
	MOVF       _num+0, 0
	XORWF      _number1+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	MOVF       _num+1, 0
	XORWF      _number2+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	MOVF       _num+2, 0
	XORWF      _number3+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	MOVF       _num+3, 0
	XORWF      _number4+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_main12
L__main19:
;5.c,82 :: 		LCD_Cmd(_LCD_CLEAR);                 // i?enoea ye?aia
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;5.c,83 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main13:
	DECFSZ     R13+0, 1
	GOTO       L_main13
	DECFSZ     R12+0, 1
	GOTO       L_main13
	DECFSZ     R11+0, 1
	GOTO       L_main13
	NOP
	NOP
;5.c,84 :: 		Lcd_Out(1,1,"Access allowed.");      // auaaiaeo caienu "Ainooi ?ac?ao?i."
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,85 :: 		Lcd_Out(2,1, "You are welcome!");    // auaaiaeo caienu "Aia?i ii?aeiaaou"
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,86 :: 		Delay_ms(500);                       // Caaa??ea a 3 naeoiau.
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	NOP
	NOP
;5.c,88 :: 		}
	GOTO       L_main15
L_main12:
;5.c,91 :: 		LCD_Cmd(_LCD_CLEAR);                 // i?enoea ye?aia
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;5.c,92 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main16:
	DECFSZ     R13+0, 1
	GOTO       L_main16
	DECFSZ     R12+0, 1
	GOTO       L_main16
	DECFSZ     R11+0, 1
	GOTO       L_main16
	NOP
	NOP
;5.c,93 :: 		Lcd_Out(1,4,"Password ");            // auaaiaeo caienu "Iaaa?iue ia?ieu"
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,94 :: 		Lcd_Out(2,4,"is wrong!");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,95 :: 		Delay_ms(500);                       // caaa??ea a 2 n
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
	DECFSZ     R12+0, 1
	GOTO       L_main17
	DECFSZ     R11+0, 1
	GOTO       L_main17
	NOP
	NOP
;5.c,96 :: 		LCD_Cmd(_LCD_CLEAR);                 // i?enoea ye?aia
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;5.c,97 :: 		Lcd_Out(1,2,"Access denied!");       // auaaiaeo caienu "A ainooia ioeacaii"
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;5.c,98 :: 		Delay_ms(500);                       // caaa??ea a 2 n
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main18:
	DECFSZ     R13+0, 1
	GOTO       L_main18
	DECFSZ     R12+0, 1
	GOTO       L_main18
	DECFSZ     R11+0, 1
	GOTO       L_main18
	NOP
	NOP
;5.c,99 :: 		}
L_main15:
;5.c,100 :: 		poss = 5;                                 // aica?auaiea ia?aiaiiie ia?aiia?aeuiiai cia?aiey.
	MOVLW      5
	MOVWF      main_poss_L0+0
;5.c,102 :: 		}
	GOTO       L_main0
;5.c,104 :: 		}
	GOTO       $+0
; end of _main
