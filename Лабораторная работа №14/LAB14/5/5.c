

char  keypadPort  at  PORTD;

char keypadPort_Direction at TRISD;

sbit  LCD_RS  at  RC2_bit;
sbit  LCD_EN  at  RC3_bit;
sbit  LCD_D4  at  RC4_bit;
sbit  LCD_D5  at  RC5_bit;
sbit  LCD_D6  at  RC6_bit;
sbit  LCD_D7  at  RC7_bit;

sbit  LCD_RS_Direction  at TRISC2_bit;
sbit  LCD_EN_Direction  at TRISC3_bit;
sbit  LCD_D4_Direction  at  TRISC4_bit;
sbit  LCD_D5_Direction  at  TRISC5_bit;
sbit  LCD_D6_Direction  at  TRISC6_bit;
sbit  LCD_D7_Direction  at  TRISC7_bit;



char  number1 = 11;  // ������� "9"
char  number2 = 5;   // ������� "4"
char  number3 = 1;   // ������� "1"
char  number4 = 13;  // ������� "*"


char num[3];
char num_key = 0;



void  main( )
{


    char poss = 5;
    char  j;

    Lcd_Init( );
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Cmd(_LCD_CURSOR_OFF);
    Keypad_Init( );

    num[0] = 0;
    num[1] = 0;
    num[2] = 0;
    num[3] = 0;

    while(1)
    {
        LCD_Cmd(_LCD_CLEAR);                // i?enoeou ye?ai
        Lcd_Out(1,2,"Enter password");      // oaeno "i?eaeaoaiey" aey
        Lcd_Out(2,4,"then click #");        // aaiaa ia?iey
        Delay_ms(250);                      // caaa??ea 2 n
        LCD_Cmd(_LCD_CLEAR);                // i?enoeou ye?ai

        for(j = 0; j < 4; j++)              // oeee aaiaa ?enae ia?iey
        {
            num_key = 0;                  // iaioeeo ia?aiaiio?
            do
            {
                num_key = Keypad_Key_Click( );

            }
            while(!num_key);


            num[j] = num_key;                  // i?enaieou yeaiaioo ianneaa cia?aiea ia?aoie eeaaeoe
            Lcd_Out(2, poss, "*");             // auaanoe '*'
            poss +=2;                          // oaaee?eou cia?aiea iiceoee ia 2, ?oiau iieo?eou i?iaae ia?ao *


        }

        Delay_ms(500);

        if (num[0] == number1 && num[1] == number2 && num[2] == number3 && num[3] == number4)
        {

                        LCD_Cmd(_LCD_CLEAR);                 // i?enoea ye?aia
                        Delay_ms(500);
                        Lcd_Out(1,1,"Access allowed.");      // auaaiaeo caienu "Ainooi ?ac?ao?i."
                        Lcd_Out(2,1, "You are welcome!");    // auaaiaeo caienu "Aia?i ii?aeiaaou"
                        Delay_ms(500);                       // Caaa??ea a 3 naeoiau.
        
        }
        else
        {
            LCD_Cmd(_LCD_CLEAR);                 // i?enoea ye?aia
            Delay_ms(500);
            Lcd_Out(1,4,"Password ");            // auaaiaeo caienu "Iaaa?iue ia?ieu"
            Lcd_Out(2,4,"is wrong!");
            Delay_ms(500);                       // caaa??ea a 2 n
            LCD_Cmd(_LCD_CLEAR);                 // i?enoea ye?aia
            Lcd_Out(1,2,"Access denied!");       // auaaiaeo caienu "A ainooia ioeacaii"
            Delay_ms(500);                       // caaa??ea a 2 n
        }
        poss = 5;                                 // aica?auaiea ia?aiaiiie ia?aiia?aeuiiai cia?aiey.

    }

}
