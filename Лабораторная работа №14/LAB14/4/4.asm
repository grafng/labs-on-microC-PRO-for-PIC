
_main:

;4.c,24 :: 		void  main( )
;4.c,29 :: 		Lcd_Init( );                                           // ������������� ���
	CALL       _Lcd_Init+0
;4.c,30 :: 		Lcd_Cmd(_LCD_CLEAR);                // �������� ���
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;4.c,31 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);    // ��������� ����������� �������
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;4.c,32 :: 		Keypad_Init( );                                    // ������������� ����������
	CALL       _Keypad_Init+0
;4.c,33 :: 		while(1)
L_main0:
;4.c,35 :: 		LCD_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;4.c,36 :: 		Lcd_Out(1,2,"Enter 3 numbers");      // ����� "�����������" ���
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;4.c,37 :: 		Lcd_Out(2,4,"then click #");              // ����� ������
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;4.c,38 :: 		Delay_ms(250);
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;4.c,40 :: 		for(j = 0; j < 4; j++)                          // ���� ����� ����� ������
	CLRF       main_j_L0+0
L_main3:
	MOVLW      4
	SUBWF      main_j_L0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main4
;4.c,42 :: 		num_key = 0;
	CLRF       main_num_key_L0+0
;4.c,43 :: 		do
L_main6:
;4.c,45 :: 		num_key = Keypad_Key_Click( );
	CALL       _Keypad_Key_Click+0
	MOVF       R0+0, 0
	MOVWF      main_num_key_L0+0
;4.c,46 :: 		}while(!num_key);
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main6
;4.c,47 :: 		num[j] = num_key;
	MOVF       main_j_L0+0, 0
	ADDLW      main_num_L0+0
	MOVWF      FSR
	MOVF       main_num_key_L0+0, 0
	MOVWF      INDF+0
;4.c,40 :: 		for(j = 0; j < 4; j++)                          // ���� ����� ����� ������
	INCF       main_j_L0+0, 1
;4.c,48 :: 		}
	GOTO       L_main3
L_main4:
;4.c,50 :: 		if(num[0] == number1 && num[1] == number2 && num[2] == number3)
	MOVF       main_num_L0+0, 0
	XORLW      2
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	MOVF       main_num_L0+1, 0
	XORLW      5
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	MOVF       main_num_L0+2, 0
	XORLW      10
	BTFSS      STATUS+0, 2
	GOTO       L_main11
L__main15:
;4.c,52 :: 		LCD_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;4.c,53 :: 		Lcd_Out(1,1,"You are ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;4.c,54 :: 		Lcd_Out(2,1, "welcome!");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;4.c,55 :: 		Delay_ms(375);
	MOVLW      4
	MOVWF      R11+0
	MOVLW      207
	MOVWF      R12+0
	MOVLW      1
	MOVWF      R13+0
L_main12:
	DECFSZ     R13+0, 1
	GOTO       L_main12
	DECFSZ     R12+0, 1
	GOTO       L_main12
	DECFSZ     R11+0, 1
	GOTO       L_main12
	NOP
	NOP
;4.c,56 :: 		}
	GOTO       L_main13
L_main11:
;4.c,59 :: 		LCD_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;4.c,60 :: 		Lcd_Out(1,4,"Password ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;4.c,61 :: 		Lcd_Out(2,4,"is wrong!");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;4.c,62 :: 		Delay_ms(375);
	MOVLW      4
	MOVWF      R11+0
	MOVLW      207
	MOVWF      R12+0
	MOVLW      1
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	NOP
	NOP
;4.c,63 :: 		}
L_main13:
;4.c,64 :: 		}
	GOTO       L_main0
;4.c,65 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
