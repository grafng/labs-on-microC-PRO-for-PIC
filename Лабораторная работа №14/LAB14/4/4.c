/*****************************************************************
password.c - ��������� ����� � ������� ������
*****************************************************************/
       // ������������� ������� ��� � ����������������
sbit  LCD_RS  at  RC2_bit;
sbit  LCD_EN  at  RC3_bit;
sbit  LCD_D4  at  RC4_bit;
sbit  LCD_D5  at  RC5_bit;
sbit  LCD_D6  at  RC6_bit;
sbit  LCD_D7  at  RC7_bit;
sbit  LCD_RS_Direction  at TRISC2_bit;
sbit  LCD_EN_Direction  at TRISC3_bit;
sbit  LCD_D4_Direction  at  TRISC4_bit;
sbit  LCD_D5_Direction  at  TRISC5_bit;
sbit  LCD_D6_Direction  at  TRISC6_bit;
sbit  LCD_D7_Direction  at  TRISC7_bit;
       // ������������� ���������� � ����� D
char  keypadPort  at  PORTD;
      // ���������� ����� (������� ������) ��� "������" ��� ��������,
      // ���������� � ������ ��������
code  const  char  number1 = 2;
code  const  char  number2 = 5;
code  const  char  number3 = 10;
void  main( )
{
     char   num_key;                                    // ����� ������� �������
     char  j;
     char  num[4];           // ������ ��� �������� ������� ������� ������
     Lcd_Init( );                                           // ������������� ���
     Lcd_Cmd(_LCD_CLEAR);                // �������� ���
     Lcd_Cmd(_LCD_CURSOR_OFF);    // ��������� ����������� �������
     Keypad_Init( );                                    // ������������� ����������
     while(1)
     {
     LCD_Cmd(_LCD_CLEAR);
     Lcd_Out(1,2,"Enter 3 numbers");      // ����� "�����������" ���
     Lcd_Out(2,4,"then click #");              // ����� ������
     Delay_ms(250);

     for(j = 0; j < 4; j++)                          // ���� ����� ����� ������
     {
           num_key = 0;
           do
           {
                 num_key = Keypad_Key_Click( );
            }while(!num_key);
            num[j] = num_key;
      }
          // �������� ������������ ������� ������� ������ � "������"
      if(num[0] == number1 && num[1] == number2 && num[2] == number3)
      {
                  LCD_Cmd(_LCD_CLEAR);
                  Lcd_Out(1,1,"You are ");
                  Lcd_Out(2,1, "welcome!");
                  Delay_ms(375);
       }
       else
       {
               LCD_Cmd(_LCD_CLEAR);
               Lcd_Out(1,4,"Password ");
               Lcd_Out(2,4,"is wrong!");
               Delay_ms(375);
        }
        }
}
