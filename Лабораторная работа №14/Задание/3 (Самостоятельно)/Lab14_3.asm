
_main:

	CALL       _Lcd_Init+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	CALL       _Keypad_Init+0
L_main0:
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_Lab14_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_Lab14_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	CLRF       main_j_L0+0
L_main3:
	MOVLW      4
	SUBWF      main_j_L0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main4
	CLRF       main_num_key_L0+0
L_main6:
	CALL       _Keypad_Key_Click+0
	MOVF       R0+0, 0
	MOVWF      main_num_key_L0+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main6
	MOVF       main_j_L0+0, 0
	ADDLW      main_num_L0+0
	MOVWF      FSR
	MOVF       main_num_key_L0+0, 0
	MOVWF      INDF+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVF       main_j_L0+0, 0
	MOVWF      R0+0
	RLF        R0+0, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_Lab14_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	INCF       main_j_L0+0, 1
	GOTO       L_main3
L_main4:
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVF       main_num_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	MOVF       main_num_L0+1, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	MOVF       main_num_L0+2, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
L__main13:
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_Lab14_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_Lab14_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      232
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVLW      3
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	GOTO       L_main12
L_main11:
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_Lab14_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_Lab14_3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      232
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVLW      3
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
L_main12:
	GOTO       L_main0
L_end_main:
	GOTO       $+0
; end of _main
