/***************************************************************
keypad_phone.c - ��������� ������ ���������� ����������
****************************************************************/
       // ������������� ������� ��� � ����������������
sbit  LCD_RS  at  RC2_bit;
sbit  LCD_EN  at  RC3_bit;
sbit  LCD_D4  at  RC4_bit;
sbit  LCD_D5  at  RC5_bit;
sbit  LCD_D6  at  RC6_bit;
sbit  LCD_D7  at  RC7_bit;
sbit  LCD_RS_Direction  at TRISC2_bit;
sbit  LCD_EN_Direction  at TRISC3_bit;
sbit  LCD_D4_Direction  at  TRISC4_bit;
sbit  LCD_D5_Direction  at  TRISC5_bit;
sbit  LCD_D6_Direction  at  TRISC6_bit;
sbit  LCD_D7_Direction  at  TRISC7_bit;
       // ������������� ���������� � ����� D
char  keypadPort  at  PORTD;
void  main( )
{
      char   num_key;                                    // ����� ������� �������
      Lcd_Init( );                                           // ������������� ���
      Lcd_Cmd(_LCD_CLEAR);                // �������� ���
      Lcd_Cmd(_LCD_CURSOR_OFF);    // ��������� ����������� �������
      Keypad_Init( );                                    // ������������� ����������

      while(1)
      {
            num_key = 0;
            do
            {
                  num_key = Keypad_Key_Click( );
             }while(!num_key);
             switch(num_key)
             {
                    case  1 :
                    {
                          Lcd_Out( 1, 1, "Key:  1");
                          Lcd_Out(2, 1,  "Number:  1");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                     }
                       case  2 :
                     {
                          Lcd_Out(1, 1,  "Key:   2");
                          Lcd_Out(2, 1,  "Number:  2");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  3 :
                     {
                          Lcd_Out(1, 1,  "Key:   3");
                          Lcd_Out(2, 1,  "Number:  3");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  5 :
                     {
                          Lcd_Out(1, 1, "Key:   4");
                          Lcd_Out(2, 1,  "Number:  5");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  6 :
                      {
                          Lcd_Out(1, 1, "Key:   5");
                          Lcd_Out(2, 1, "Number:  6");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  7 :
                     {
                          Lcd_Out(1, 1, "Key:   6");
                          Lcd_Out(2, 1, "Number:  7");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  9 :
                     {
                          Lcd_Out(1, 1, "Key:  7");
                          Lcd_Out(2, 1, "Number:  9");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  10 :
                     {
                          Lcd_Out(1, 1, "Key:  8");
                          Lcd_Out(2, 1, "Number:  10");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  11 :
                      {
                          Lcd_Out(1, 1, "Key:  9");
                          Lcd_Out(2, 1, "Number:  11");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  13 :
                      {
                          Lcd_Out(1, 1,  "Key:  *");
                          Lcd_Out(2, 1, "Number:  13");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  14:
                     {
                          Lcd_Out(1, 1, "Key:  0");
                          Lcd_Out(2, 1, "Number:  14");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                      case  15 :
                     {
                          Lcd_Out(1, 1, "Key:  #");
                          Lcd_Out(2, 1, "Number:  15");
                          Delay_ms(1000);
                          Lcd_Cmd(_LCD_CLEAR);
                          break;
                      }
                 }
           }
}
