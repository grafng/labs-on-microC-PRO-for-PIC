
_main:

	MOVLW      112
	MOVWF      TRISB+0
	CLRF       PORTB+0
	CLRF       PORTC+0
L_main0:
	GOTO       L_main2
L_main4:
	GOTO       L_main3
L_main5:
	BSF        PORTC+0, 0
	MOVLW      232
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVLW      3
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
	BCF        PORTB+0, 0
	GOTO       L_main3
L_main6:
	BSF        PORTB+0, 1
	MOVLW      232
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVLW      3
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
	BCF        PORTB+0, 1
	GOTO       L_main3
L_main7:
	BSF        PORTB+0, 2
	MOVLW      232
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVLW      3
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
	BCF        PORTB+0, 2
	GOTO       L_main3
L_main8:
	MOVLW      224
	MOVWF      PORTB+0
	MOVLW      232
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVLW      3
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
	CLRF       PORTB+0
	GOTO       L_main3
L_main2:
	MOVF       PORTB+0, 0
	XORLW      0
	BTFSC      STATUS+0, 2
	GOTO       L_main4
	MOVF       PORTB+0, 0
	XORLW      6
	BTFSC      STATUS+0, 2
	GOTO       L_main5
	MOVF       PORTB+0, 0
	XORLW      5
	BTFSC      STATUS+0, 2
	GOTO       L_main6
	MOVF       PORTB+0, 0
	XORLW      3
	BTFSC      STATUS+0, 2
	GOTO       L_main7
	GOTO       L_main8
L_main3:
	GOTO       L_main0
L_end_main:
	GOTO       $+0
; end of _main
