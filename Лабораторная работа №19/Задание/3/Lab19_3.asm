
_timer0_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;Lab19_3.c,4 :: 		void  timer0_ISR( )  org  IVT_ADDR_TIMER0_OVF
;Lab19_3.c,6 :: 		PORTC0_bit = ~PORTC0_bit;  // �������������  ����� ����� PC0
	IN         R0, PORTC0_bit+0
	LDI        R27, BitMask(PORTC0_bit+0)
	EOR        R0, R27
	OUT        PORTC0_bit+0, R0
;Lab19_3.c,7 :: 		PORTC1_bit = ~PORTC1_bit;
	IN         R0, PORTC1_bit+0
	LDI        R27, BitMask(PORTC1_bit+0)
	EOR        R0, R27
	OUT        PORTC1_bit+0, R0
;Lab19_3.c,8 :: 		TCNT0 = 178;                           // ������������� ������ T/C0
	LDI        R27, 178
	OUT        TCNT0+0, R27
;Lab19_3.c,9 :: 		}
L_end_timer0_ISR:
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _timer0_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab19_3.c,10 :: 		void  main( )
;Lab19_3.c,12 :: 		DDC0_bit = 1;
	IN         R27, DDC0_bit+0
	SBR        R27, BitMask(DDC0_bit+0)
	OUT        DDC0_bit+0, R27
;Lab19_3.c,13 :: 		DDC1_bit = 1;                        // ��������� ����� PC0 �� �����
	IN         R27, DDC1_bit+0
	SBR        R27, BitMask(DDC1_bit+0)
	OUT        DDC1_bit+0, R27
;Lab19_3.c,14 :: 		TCNT0 = 178;                          // ��������� ��������� �������� � T/C0
	LDI        R27, 178
	OUT        TCNT0+0, R27
;Lab19_3.c,15 :: 		TCCR0 = 0b00000101;            // ���������� � = 1024
	LDI        R27, 5
	OUT        TCCR0+0, R27
;Lab19_3.c,16 :: 		TOIE0_bit = 1;               // ��������� ���������� �� ������������ T/C0
	IN         R27, TOIE0_bit+0
	SBR        R27, BitMask(TOIE0_bit+0)
	OUT        TOIE0_bit+0, R27
;Lab19_3.c,17 :: 		SREG_I_bit = 1;                      // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, BitMask(SREG_I_bit+0)
	OUT        SREG_I_bit+0, R27
;Lab19_3.c,18 :: 		while(1);
L_main0:
	JMP        L_main0
;Lab19_3.c,19 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
