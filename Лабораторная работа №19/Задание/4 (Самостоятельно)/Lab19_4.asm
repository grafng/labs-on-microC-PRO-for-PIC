
_timer1_ISR:
	PUSH       R30
	PUSH       R31
	PUSH       R27
	IN         R27, SREG+0
	PUSH       R27

;Lab19_4.c,4 :: 		void  timer1_ISR( )  org  IVT_ADDR_TIMER1_OVF
;Lab19_4.c,6 :: 		PORTC0_bit = ~PORTC0_bit;   // ������������� ����� ����� PC3
	IN         R0, PORTC0_bit+0
	LDI        R27, 1
	EOR        R0, R27
	OUT        PORTC0_bit+0, R0
;Lab19_4.c,8 :: 		counter++;
	LDS        R16, _counter+0
	LDS        R17, _counter+1
	SUBI       R16, 255
	SBCI       R17, 255
	STS        _counter+0, R16
	STS        _counter+1, R17
;Lab19_4.c,10 :: 		if ( counter == 4 )
	CPI        R17, 0
	BRNE       L__timer1_ISR6
	CPI        R16, 4
L__timer1_ISR6:
	BREQ       L__timer1_ISR7
	JMP        L_timer1_ISR0
L__timer1_ISR7:
;Lab19_4.c,12 :: 		PORTC3_bit = ~PORTC3_bit;
	IN         R0, PORTC3_bit+0
	LDI        R27, 8
	EOR        R0, R27
	OUT        PORTC3_bit+0, R0
;Lab19_4.c,13 :: 		counter = 0;
	LDI        R27, 0
	STS        _counter+0, R27
	STS        _counter+1, R27
;Lab19_4.c,14 :: 		}
L_timer1_ISR0:
;Lab19_4.c,16 :: 		TCNT1H = 0xF0;                        // ������������� ������� �������
	LDI        R27, 240
	OUT        TCNT1H+0, R27
;Lab19_4.c,17 :: 		TCNT1L = 0xBE;                       // ������������� ������� �������
	LDI        R27, 190
	OUT        TCNT1L+0, R27
;Lab19_4.c,18 :: 		}
L_end_timer1_ISR:
	POP        R27
	OUT        SREG+0, R27
	POP        R27
	POP        R31
	POP        R30
	RETI
; end of _timer1_ISR

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;Lab19_4.c,20 :: 		void  main( )
;Lab19_4.c,23 :: 		DDC0_bit = 1;                            // ��������� ����� PC3 �� �����
	IN         R27, DDC0_bit+0
	SBR        R27, 1
	OUT        DDC0_bit+0, R27
;Lab19_4.c,24 :: 		DDC3_bit = 1;
	IN         R27, DDC3_bit+0
	SBR        R27, 8
	OUT        DDC3_bit+0, R27
;Lab19_4.c,25 :: 		TCCR1A = 0;
	LDI        R27, 0
	OUT        TCCR1A+0, R27
;Lab19_4.c,26 :: 		TCCR1B = 0b00000101;           // ���������� � = 1024
	LDI        R27, 5
	OUT        TCCR1B+0, R27
;Lab19_4.c,27 :: 		TCNT1H = 0xF0;                       // ��������� � ������� ��������
	LDI        R27, 240
	OUT        TCNT1H+0, R27
;Lab19_4.c,28 :: 		TCNT1L = 0xBE;                       // ��������� ��������
	LDI        R27, 190
	OUT        TCNT1L+0, R27
;Lab19_4.c,29 :: 		TOIE1_bit = 1;           // ��������� ���������� �� ������������ T/C1
	IN         R27, TOIE1_bit+0
	SBR        R27, 4
	OUT        TOIE1_bit+0, R27
;Lab19_4.c,30 :: 		SREG_I_bit = 1;                        // ���������� ���������� ����������
	IN         R27, SREG_I_bit+0
	SBR        R27, 128
	OUT        SREG_I_bit+0, R27
;Lab19_4.c,31 :: 		while(1);
L_main1:
	JMP        L_main1
;Lab19_4.c,32 :: 		}
L_end_main:
	JMP        L_end_main
; end of _main
