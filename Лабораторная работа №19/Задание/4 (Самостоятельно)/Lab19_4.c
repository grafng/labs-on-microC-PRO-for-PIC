
int counter = 0;

void  timer1_ISR( )  org  IVT_ADDR_TIMER1_OVF
{
    PORTC0_bit = ~PORTC0_bit;   // ������������� ����� ����� PC3

    counter++;

    if ( counter == 4 )
    {
        PORTC3_bit = ~PORTC3_bit;
        counter = 0;
    }

    TCNT1H = 0xF0;                        // ������������� ������� �������
    TCNT1L = 0xBE;                       // ������������� ������� �������
}

void  main( )
{

    DDC0_bit = 1;                            // ��������� ����� PC3 �� �����
    DDC3_bit = 1;
    TCCR1A = 0;
    TCCR1B = 0b00000101;           // ���������� � = 1024
    TCNT1H = 0xF0;                       // ��������� � ������� ��������
    TCNT1L = 0xBE;                       // ��������� ��������
    TOIE1_bit = 1;           // ��������� ���������� �� ������������ T/C1
    SREG_I_bit = 1;                        // ���������� ���������� ����������
    while(1);
}
