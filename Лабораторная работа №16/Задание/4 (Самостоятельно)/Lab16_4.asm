
_interrupt:

	BTFSS       TMR0IF_bit+0, BitPos(TMR0IF_bit+0) 
	GOTO        L_interrupt0
	BTG         PORTC+0, 0 
	BCF         TMR0IF_bit+0, BitPos(TMR0IF_bit+0) 
L_interrupt0:
	BTFSS       INT0IF_bit+0, BitPos(INT0IF_bit+0) 
	GOTO        L_interrupt1
	BSF         PORTC+0, 7 
	BCF         INT0IF_bit+0, BitPos(INT0IF_bit+0) 
	MOVLW       136
	MOVWF       FARG_VDelay_ms_Time_ms+0 
	MOVLW       19
	MOVWF       FARG_VDelay_ms_Time_ms+1 
	CALL        _VDelay_ms+0, 0
	BCF         PORTC+0, 7 
L_interrupt1:
L_end_interrupt:
L__interrupt5:
	RETFIE      1
; end of _interrupt

_main:

	CALL        _init+0, 0
L_main2:
	GOTO        L_main2
L_end_main:
	GOTO        $+0
; end of _main

_init:

	BSF         TRISB+0, 0 
	CLRF        TRISC+0 
	CLRF        PORTC+0 
	MOVLW       132
	MOVWF       T0CON+0 
	CLRF        INTCON2+0 
	MOVLW       176
	MOVWF       INTCON+0 
L_end_init:
	RETURN      0
; end of _init
