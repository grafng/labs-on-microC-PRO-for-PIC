
_interrupt:

	INCF        _counter+0, 1 
	MOVF        _counter+0, 0 
	XORLW       31
	BTFSS       STATUS+0, 2 
	GOTO        L_interrupt0
	BTG         PORTC+0, 0 
	BCF         TMR0IF_bit+0, BitPos(TMR0IF_bit+0) 
L_interrupt0:
L_end_interrupt:
L__interrupt4:
	RETFIE      1
; end of _interrupt

_main:

	CALL        _init+0, 0
L_main1:
	GOTO        L_main1
L_end_main:
	GOTO        $+0
; end of _main

_init:

	CLRF        TRISC+0 
	CLRF        PORTC+0 
	MOVLW       132
	MOVWF       T0CON+0 
	BSF         INTCON+0, 5 
	BSF         INTCON+0, 7 
L_end_init:
	RETURN      0
; end of _init
