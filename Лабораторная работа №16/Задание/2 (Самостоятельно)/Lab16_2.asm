
_interrupt:

;Lab16_2.c,3 :: 		void interrupt(){            // �������-���������� ����������
;Lab16_2.c,4 :: 		counter++;
	INCF        _counter+0, 1 
;Lab16_2.c,5 :: 		if(counter == 131){
	MOVF        _counter+0, 0 
	XORLW       131
	BTFSS       STATUS+0, 2 
	GOTO        L_interrupt0
;Lab16_2.c,6 :: 		PORTC ^= 0x01;          // ������������� ������ RC0 ����� �
	BTG         PORTC+0, 0 
;Lab16_2.c,7 :: 		TMR0IF_bit = 0;         // �������� ���� ���������� �� TMR0
	BCF         TMR0IF_bit+0, 2 
;Lab16_2.c,8 :: 		}
L_interrupt0:
;Lab16_2.c,9 :: 		}
L__interrupt3:
	RETFIE      1
; end of _interrupt

_main:

;Lab16_2.c,14 :: 		void   main( )
;Lab16_2.c,16 :: 		init( );                            // ����� ������� ������������� ��
	CALL        _init+0, 0
;Lab16_2.c,17 :: 		while(1);                                   // ����������� ����
L_main1:
	GOTO        L_main1
;Lab16_2.c,18 :: 		}
	GOTO        $+0
; end of _main

_init:

;Lab16_2.c,20 :: 		void  init( )                                   // ������� ������������� ��
;Lab16_2.c,22 :: 		TRISC = 0;
	CLRF        TRISC+0 
;Lab16_2.c,23 :: 		PORTC = 0;              // ��������  ���������
	CLRF        PORTC+0 
;Lab16_2.c,24 :: 		T0CON = 0b11000111;     // ��������� ������, 8-���������, ����������� ������� 128
	MOVLW       199
	MOVWF       T0CON+0 
;Lab16_2.c,25 :: 		INTCON.B5 = 1;          // ��������� ���������� �� ������� TMR0
	BSF         INTCON+0, 5 
;Lab16_2.c,26 :: 		INTCON.B7 = 1;          // ����� ���������� ����������
	BSF         INTCON+0, 7 
;Lab16_2.c,28 :: 		}
	RETURN      0
; end of _init
