#line 1 "E:/Univ/Dropbox/���/����/12/1/MyProject.c"
#line 5 "E:/Univ/Dropbox/���/����/12/1/MyProject.c"
sbit LCD_RS at RC2_bit;
sbit LCD_EN at RC3_bit;
sbit LCD_D4 at RC4_bit;
sbit LCD_D5 at RC5_bit;
sbit LCD_D6 at RC6_bit;
sbit LCD_D7 at RC7_bit;

sbit LCD_RS_Direction at TRISC2_bit;
sbit LCD_EN_Direction at TRISC3_bit;
sbit LCD_D4_Direction at TRISC4_bit;
sbit LCD_D5_Direction at TRISC5_bit;
sbit LCD_D6_Direction at TRISC6_bit;
sbit LCD_D7_Direction at TRISC7_bit;

void main(void)
{
 int res_ADC;
 int mvolts;
 char string[10];
 TRISA = 0x01;
 ADC_Init( );
 Lcd_Init( );
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);
 while( 1 )
 {
 res_ADC = ADC_Read( 0 );
 mvolts = ((long)res_ADC * 5000) / 0x03FF;

 IntToStr( mvolts, string );
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(1, 4, "U1 =");
 Lcd_Out_Cp( string );
 Lcd_Out_Cp( " mV");
 Delay_ms(2000) ;
 }
}
