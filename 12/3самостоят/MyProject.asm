
_main:

;MyProject.c,20 :: 		void  main( )
;MyProject.c,25 :: 		TRISA = 0x01;            // ��������� ����� RA0 �� ����
	MOVLW      1
	MOVWF      TRISA+0
;MyProject.c,26 :: 		TRISB = 0;
	CLRF       TRISB+0
;MyProject.c,27 :: 		OPTION_REG.B0=0;
	BCF        OPTION_REG+0, 0
;MyProject.c,28 :: 		OPTION_REG.B1=0;
	BCF        OPTION_REG+0, 1
;MyProject.c,29 :: 		ADC_Init( );                   // ������������� ������ ���
	CALL       _ADC_Init+0
;MyProject.c,30 :: 		Lcd_Init( );                    // ������������� ������ ���
	CALL       _Lcd_Init+0
;MyProject.c,31 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,32 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,33 :: 		while(1)
L_main0:
;MyProject.c,35 :: 		res_ADC = ADC_Read( 0 );                                // ������ ���� ���
	CLRF       FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
;MyProject.c,36 :: 		mvolts = ((long)res_ADC  * 5000) / 0x03FF;     // ��������������
	MOVLW      0
	BTFSC      R0+1, 7
	MOVLW      255
	MOVWF      R0+2
	MOVWF      R0+3
	MOVLW      136
	MOVWF      R4+0
	MOVLW      19
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_S+0
	MOVF       R0+0, 0
	MOVWF      main_mvolts_L0+0
	MOVF       R0+1, 0
	MOVWF      main_mvolts_L0+1
;MyProject.c,38 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,39 :: 		Lcd_Out(2, 4, "U = ");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,40 :: 		num = mvolts / 1000;                 // ���������� ������ �����
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	MOVF       main_mvolts_L0+0, 0
	MOVWF      R0+0
	MOVF       main_mvolts_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
;MyProject.c,41 :: 		Lcd_Chr_Cp(48 + num);          // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;MyProject.c,42 :: 		Lcd_Chr_Cp('.');                      // ����������� ���������� �����
	MOVLW      46
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;MyProject.c,43 :: 		num = (mvolts / 100) % 10;      // ���������� ������� ����� �����
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_mvolts_L0+0, 0
	MOVWF      R0+0
	MOVF       main_mvolts_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
;MyProject.c,44 :: 		Lcd_Chr_Cp(48 + num);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;MyProject.c,45 :: 		num = (mvolts / 10) % 10;        // ���������� ����� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_mvolts_L0+0, 0
	MOVWF      R0+0
	MOVF       main_mvolts_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
;MyProject.c,46 :: 		Lcd_Chr_Cp(48 + num);          //  ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;MyProject.c,47 :: 		num = mvolts % 10;                  // ���������� �������� ����� �����
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_mvolts_L0+0, 0
	MOVWF      R0+0
	MOVF       main_mvolts_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
;MyProject.c,48 :: 		Lcd_Chr_Cp(48 + num);           // ����������� ����� � ���� ASCII
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      FARG_Lcd_Chr_CP_out_char+0
	CALL       _Lcd_Chr_CP+0
;MyProject.c,49 :: 		Lcd_Out_Cp(" V");
	MOVLW      ?lstr2_MyProject+0
	MOVWF      FARG_Lcd_Out_CP_text+0
	CALL       _Lcd_Out_CP+0
;MyProject.c,50 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;MyProject.c,51 :: 		}
	GOTO       L_main0
;MyProject.c,52 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
