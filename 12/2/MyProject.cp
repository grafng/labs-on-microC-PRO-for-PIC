#line 1 "E:/Univ/Dropbox/���/����/12/2/MyProject.c"
#line 6 "E:/Univ/Dropbox/���/����/12/2/MyProject.c"
sbit LCD_RS at RC2_bit;
sbit LCD_EN at RC3_bit;
sbit LCD_D4 at RC4_bit;
sbit LCD_D5 at RC5_bit;
sbit LCD_D6 at RC6_bit;
sbit LCD_D7 at RC7_bit;

sbit LCD_RS_Direction at TRISC2_bit;
sbit LCD_EN_Direction at TRISC3_bit;
sbit LCD_D4_Direction at TRISC4_bit;
sbit LCD_D5_Direction at TRISC5_bit;
sbit LCD_D6_Direction at TRISC6_bit;
sbit LCD_D7_Direction at TRISC7_bit;

void main( )
{
 int res_ADC;
 int mvolts;
 char num;
 TRISA = 0x01;
 ADC_Init( );
 Lcd_Init( );
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);
 while(1)
 {
 res_ADC = ADC_Read( 0 );
 mvolts = ((long)res_ADC * 5000) / 0x03FF;

 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(2, 4, "U = ");
 num = mvolts / 1000;
 Lcd_Chr_Cp(48 + num);
 Lcd_Chr_Cp('.');
 num = (mvolts / 100) % 10;
 Lcd_Chr_Cp(48 + num);
 num = (mvolts / 10) % 10;
 Lcd_Chr_Cp(48 + num);
 num = mvolts % 10;
 Lcd_Chr_Cp(48 + num);
 Lcd_Out_Cp(" V");
 Delay_ms(2000);
 }
}
