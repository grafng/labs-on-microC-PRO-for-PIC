
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;1.c,8 :: 		void  main( )
;1.c,10 :: 		DDRC = 0xFF;                           // ��������� ���� � �� �����
	LDI        R27, 255
	OUT        DDRC+0, R27
;1.c,11 :: 		PORTC = 0;                             // �������� ����������
	LDI        R27, 0
	OUT        PORTC+0, R27
;1.c,12 :: 		while(1)
L_main0:
;1.c,16 :: 		mvolts = get_mvolts( );          // ����� ������� ��������� ����������
	CALL       _get_mvolts+0
; mvolts start address is: 20 (R20)
	MOVW       R20, R16
;1.c,17 :: 		PORTC = 0;
	LDI        R27, 0
	OUT        PORTC+0, R27
;1.c,18 :: 		if(mvolts > MAX_U)               // ���� ���������� ������ �������������
	LDI        R18, 137
	LDI        R19, 13
	CP         R18, R16
	CPC        R19, R17
	BRLT       L__main13
	JMP        L_main2
L__main13:
; mvolts end address is: 20 (R20)
;1.c,20 :: 		PORTC.B0 = 1;              // ������ ������� ��������� D1
	IN         R27, PORTC+0
	SBR        R27, 1
	OUT        PORTC+0, R27
;1.c,21 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_main3:
	DEC        R16
	BRNE       L_main3
	DEC        R17
	BRNE       L_main3
	DEC        R18
	BRNE       L_main3
	NOP
;1.c,22 :: 		}
	JMP        L_main5
L_main2:
;1.c,23 :: 		else  if(mvolts < MIN_U)         // ���� ������ ������������ ��������
; mvolts start address is: 20 (R20)
	LDI        R16, 63
	LDI        R17, 12
	CP         R20, R16
	CPC        R21, R17
	BRLT       L__main14
	JMP        L_main6
L__main14:
; mvolts end address is: 20 (R20)
;1.c,25 :: 		PORTC.B2 = 1;               // ������ ������ ��������� D3
	IN         R27, PORTC+0
	SBR        R27, 4
	OUT        PORTC+0, R27
;1.c,26 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_main7:
	DEC        R16
	BRNE       L_main7
	DEC        R17
	BRNE       L_main7
	DEC        R18
	BRNE       L_main7
	NOP
;1.c,27 :: 		}
	JMP        L_main9
L_main6:
;1.c,30 :: 		PORTC.B1 = 1;                              // ������ ������� ��������� D2 (�����)
	IN         R27, PORTC+0
	SBR        R27, 2
	OUT        PORTC+0, R27
;1.c,31 :: 		Delay_ms(500);
	LDI        R18, 21
	LDI        R17, 75
	LDI        R16, 191
L_main10:
	DEC        R16
	BRNE       L_main10
	DEC        R17
	BRNE       L_main10
	DEC        R18
	BRNE       L_main10
	NOP
;1.c,32 :: 		}
L_main9:
L_main5:
;1.c,33 :: 		}                                                   // ����� ����� while
	JMP        L_main0
;1.c,34 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main

_get_mvolts:

;1.c,36 :: 		int  get_mvolts( )                                         // ������� ��������� ����������
;1.c,39 :: 		res_ADC = ADC_Read(0);                                // ������ ���� ���
	PUSH       R2
	CLR        R2
	CALL       _ADC_Read+0
;1.c,40 :: 		return   ((long)res_ADC * 5000) / 0x03FF;             // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
;1.c,42 :: 		}
;1.c,40 :: 		return   ((long)res_ADC * 5000) / 0x03FF;             // ��������������
;1.c,42 :: 		}
L_end_get_mvolts:
	POP        R2
	RET
; end of _get_mvolts
