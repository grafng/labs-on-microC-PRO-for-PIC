
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;4.c,32 :: 		void  main( )
;4.c,34 :: 		init( );                                                   // ����� ������� �������������
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	CALL       _init+0
;4.c,35 :: 		while(1)
L_main0:
;4.c,37 :: 		Lcd_Cmd(_LCD_CLEAR);
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;4.c,38 :: 		Lcd_Out(1, 5, "Press key");                    // ����� �� ����� "Prees key to measure U"
	LDI        R27, #lo_addr(?lstr1_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr1_4+0)
	MOV        R5, R27
	LDI        R27, 5
	MOV        R3, R27
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,39 :: 		Lcd_Out(2, 3,"to measure U");
	LDI        R27, #lo_addr(?lstr2_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr2_4+0)
	MOV        R5, R27
	LDI        R27, 3
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,40 :: 		Delay_ms(100);
	LDI        R18, 5
	LDI        R17, 15
	LDI        R16, 242
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	DEC        R18
	BRNE       L_main2
;4.c,41 :: 		while(PINB.B0 == 0)
L_main4:
	IN         R27, PINB+0
	SBRC       R27, 0
	JMP        L_main5
;4.c,55 :: 		get_volts(channel);                               // ����� ������� ��������� ����������
	LDS        R2, _channel+0
	CALL       _get_volts+0
;4.c,56 :: 		out_display( );                                   // ����� ������� ������ �� ���
	CALL       _out_display+0
;4.c,57 :: 		Delay_ms(1000);                                   // �������� �� 1  �
	LDI        R18, 41
	LDI        R17, 150
	LDI        R16, 128
L_main6:
	DEC        R16
	BRNE       L_main6
	DEC        R17
	BRNE       L_main6
	DEC        R18
	BRNE       L_main6
;4.c,58 :: 		}
	JMP        L_main4
L_main5:
;4.c,59 :: 		while(PINB.B1 == 0)
L_main8:
	IN         R27, PINB+0
	SBRC       R27, 1
	JMP        L_main9
;4.c,61 :: 		while(PINB.B2 == 0)
L_main10:
	IN         R27, PINB+0
	SBRC       R27, 2
	JMP        L_main11
;4.c,64 :: 		}
	JMP        L_main10
L_main11:
;4.c,65 :: 		get_volts1(channel);                                  // ����� ������� ��������� ����������
	LDS        R2, _channel+0
	CALL       _get_volts1+0
;4.c,66 :: 		out_display1( );                                      // ����� ������� ������ �� ���
	CALL       _out_display1+0
;4.c,67 :: 		Delay_ms(1000);                                       // �������� �� 1  �
	LDI        R18, 41
	LDI        R17, 150
	LDI        R16, 128
L_main12:
	DEC        R16
	BRNE       L_main12
	DEC        R17
	BRNE       L_main12
	DEC        R18
	BRNE       L_main12
;4.c,68 :: 		}
	JMP        L_main8
L_main9:
;4.c,69 :: 		while(PINB.B2 == 0)
L_main14:
	IN         R27, PINB+0
	SBRC       R27, 2
	JMP        L_main15
;4.c,71 :: 		get_volts2(channel);                                  // ����� ������� ��������� ����������
	LDS        R2, _channel+0
	CALL       _get_volts2+0
;4.c,72 :: 		out_display2( );                                      // ����� ������� ������ �� ���
	CALL       _out_display2+0
;4.c,73 :: 		Delay_ms(1000);                                      // �������� �� 1
	LDI        R18, 41
	LDI        R17, 150
	LDI        R16, 128
L_main16:
	DEC        R16
	BRNE       L_main16
	DEC        R17
	BRNE       L_main16
	DEC        R18
	BRNE       L_main16
;4.c,74 :: 		}
	JMP        L_main14
L_main15:
;4.c,75 :: 		}
	JMP        L_main0
;4.c,76 :: 		}
L_end_main:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main

_init:

;4.c,78 :: 		void  init( )                                                    // ������� �������������
;4.c,80 :: 		DDB0_bit = 0;                                               // ��������� ����� �� ���� (����������� ������)
	PUSH       R2
	IN         R27, DDB0_bit+0
	CBR        R27, BitMask(DDB0_bit+0)
	OUT        DDB0_bit+0, R27
;4.c,81 :: 		DDB1_bit = 0;
	IN         R27, DDB1_bit+0
	CBR        R27, BitMask(DDB1_bit+0)
	OUT        DDB1_bit+0, R27
;4.c,82 :: 		DDB2_bit = 0;
	IN         R27, DDB2_bit+0
	CBR        R27, BitMask(DDB2_bit+0)
	OUT        DDB2_bit+0, R27
;4.c,83 :: 		PORTB = 0xFF;                                               // ����������� �������������� ���������
	LDI        R27, 255
	OUT        PORTB+0, R27
;4.c,84 :: 		Lcd_Init( );                                                // ������������� ������ ���
	CALL       _Lcd_Init+0
;4.c,85 :: 		Lcd_Cmd(_LCD_CLEAR);
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;4.c,86 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	LDI        R27, 12
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;4.c,87 :: 		}
L_end_init:
	POP        R2
	RET
; end of _init

_get_volts:
	PUSH       R28
	PUSH       R29
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 10
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;4.c,88 :: 		void  get_volts(char  channel)                                  // ������� ��������� ����������
;4.c,92 :: 		res_ADC = ADC_Read(0);                                      // ������ ���� ���
	PUSH       R2
	CLR        R2
	CALL       _ADC_Read+0
;4.c,93 :: 		mvolts = ((long)res_ADC  * 5000) / 0x03FF;                  // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
	STD        Y+4, R16
	STD        Y+5, R17
;4.c,98 :: 		volts[0] = mvolts / 1000;                             // ���������� ������ �����
	MOVW       R20, R28
	STD        Y+8, R20
	STD        Y+9, R21
	LDI        R20, 232
	LDI        R21, 3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,99 :: 		volts[1] = (mvolts / 100) % 10;                       // ���������� ������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 255
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,100 :: 		volts[2] = (mvolts / 10) % 10;                        // ���������� ����� ����� �����
	MOVW       R16, R28
	SUBI       R16, 254
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,101 :: 		volts[3] = mvolts % 10;                               // ���������� �������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 253
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,102 :: 		for(j = 0; j < 4; j++)                                // ������ ���� ���������� � ��� �������
; j start address is: 20 (R20)
	LDI        R20, 0
; j end address is: 20 (R20)
L_get_volts18:
; j start address is: 20 (R20)
	CPI        R20, 4
	BRLO       L__get_volts33
	JMP        L_get_volts19
L__get_volts33:
;4.c,103 :: 		displayRAM[j] = volts[j];
	LDI        R16, #lo_addr(_displayRAM+0)
	LDI        R17, hi_addr(_displayRAM+0)
	MOV        R18, R20
	LDI        R19, 0
	ADD        R18, R16
	ADC        R19, R17
	MOVW       R16, R28
	MOV        R30, R20
	LDI        R31, 0
	ADD        R30, R16
	ADC        R31, R17
	LD         R16, Z
	MOVW       R30, R18
	ST         Z, R16
;4.c,102 :: 		for(j = 0; j < 4; j++)                                // ������ ���� ���������� � ��� �������
	MOV        R16, R20
	SUBI       R16, 255
; j end address is: 20 (R20)
; j start address is: 16 (R16)
;4.c,103 :: 		displayRAM[j] = volts[j];
	MOV        R20, R16
; j end address is: 16 (R16)
	JMP        L_get_volts18
L_get_volts19:
;4.c,105 :: 		}
L_end_get_volts:
	POP        R2
	ADIW       R28, 9
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	POP        R29
	POP        R28
	RET
; end of _get_volts

_get_volts1:
	PUSH       R28
	PUSH       R29
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 10
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;4.c,106 :: 		void  get_volts1(char  channel)                                  // ������� ��������� ����������
;4.c,110 :: 		res_ADC1 = ADC_Read(1);                                      // ������ ���� ���
	PUSH       R2
	LDI        R27, 1
	MOV        R2, R27
	CALL       _ADC_Read+0
;4.c,111 :: 		mvolts1 = ((long)res_ADC1  * 5000) / 0x03FF;                  // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
	STD        Y+4, R16
	STD        Y+5, R17
;4.c,116 :: 		volts[0] = mvolts1 / 1000;                             // ���������� ������ �����
	MOVW       R20, R28
	STD        Y+8, R20
	STD        Y+9, R21
	LDI        R20, 232
	LDI        R21, 3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,117 :: 		volts[1] = (mvolts1 / 100) % 10;                       // ���������� ������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 255
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,118 :: 		volts[2] = (mvolts1 / 10) % 10;                        // ���������� ����� ����� �����
	MOVW       R16, R28
	SUBI       R16, 254
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,119 :: 		volts[3] = mvolts1 % 10;                               // ���������� �������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 253
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,120 :: 		for(j = 0; j < 4; j++)                                // ������ ���� ���������� � ��� �������
; j start address is: 20 (R20)
	LDI        R20, 0
; j end address is: 20 (R20)
L_get_volts121:
; j start address is: 20 (R20)
	CPI        R20, 4
	BRLO       L__get_volts135
	JMP        L_get_volts122
L__get_volts135:
;4.c,121 :: 		displayRAM[j] = volts[j];
	LDI        R16, #lo_addr(_displayRAM+0)
	LDI        R17, hi_addr(_displayRAM+0)
	MOV        R18, R20
	LDI        R19, 0
	ADD        R18, R16
	ADC        R19, R17
	MOVW       R16, R28
	MOV        R30, R20
	LDI        R31, 0
	ADD        R30, R16
	ADC        R31, R17
	LD         R16, Z
	MOVW       R30, R18
	ST         Z, R16
;4.c,120 :: 		for(j = 0; j < 4; j++)                                // ������ ���� ���������� � ��� �������
	MOV        R16, R20
	SUBI       R16, 255
; j end address is: 20 (R20)
; j start address is: 16 (R16)
;4.c,121 :: 		displayRAM[j] = volts[j];
	MOV        R20, R16
; j end address is: 16 (R16)
	JMP        L_get_volts121
L_get_volts122:
;4.c,123 :: 		}
L_end_get_volts1:
	POP        R2
	ADIW       R28, 9
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	POP        R29
	POP        R28
	RET
; end of _get_volts1

_get_volts2:
	PUSH       R28
	PUSH       R29
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 10
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;4.c,124 :: 		void  get_volts2(char  channel)                                  // ������� ��������� ����������
;4.c,128 :: 		res_ADC2 = ADC_Read(2);                                      // ������ ���� ���
	PUSH       R2
	LDI        R27, 2
	MOV        R2, R27
	CALL       _ADC_Read+0
;4.c,129 :: 		mvolts2 = ((long)res_ADC2  * 5000) / 0x03FF;                  // ��������������
	LDI        R18, 0
	SBRC       R17, 7
	LDI        R18, 255
	MOV        R19, R18
	LDI        R20, 136
	LDI        R21, 19
	LDI        R22, 0
	LDI        R23, 0
	CALL       _HWMul_32x32+0
	LDI        R20, 255
	LDI        R21, 3
	LDI        R22, 0
	LDI        R23, 0
	CALL       _Div_32x32_S+0
	MOVW       R16, R18
	MOVW       R18, R20
	STD        Y+4, R16
	STD        Y+5, R17
;4.c,134 :: 		volts[0] = mvolts2 / 1000;                             // ���������� ������ �����
	MOVW       R20, R28
	STD        Y+8, R20
	STD        Y+9, R21
	LDI        R20, 232
	LDI        R21, 3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,135 :: 		volts[1] = (mvolts2 / 100) % 10;                       // ���������� ������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 255
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,136 :: 		volts[2] = (mvolts2 / 10) % 10;                        // ���������� ����� ����� �����
	MOVW       R16, R28
	SUBI       R16, 254
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,137 :: 		volts[3] = mvolts2 % 10;                               // ���������� �������� ����� �����
	MOVW       R16, R28
	SUBI       R16, 253
	SBCI       R17, 255
	STD        Y+8, R16
	STD        Y+9, R17
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+4
	LDD        R17, Y+5
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	LDD        R18, Y+8
	LDD        R19, Y+9
	MOVW       R30, R18
	ST         Z, R16
;4.c,138 :: 		for(j = 0; j < 4; j++)                                // ������ ���� ���������� � ��� �������
; j start address is: 20 (R20)
	LDI        R20, 0
; j end address is: 20 (R20)
L_get_volts224:
; j start address is: 20 (R20)
	CPI        R20, 4
	BRLO       L__get_volts237
	JMP        L_get_volts225
L__get_volts237:
;4.c,139 :: 		displayRAM[j] = volts[j];
	LDI        R16, #lo_addr(_displayRAM+0)
	LDI        R17, hi_addr(_displayRAM+0)
	MOV        R18, R20
	LDI        R19, 0
	ADD        R18, R16
	ADC        R19, R17
	MOVW       R16, R28
	MOV        R30, R20
	LDI        R31, 0
	ADD        R30, R16
	ADC        R31, R17
	LD         R16, Z
	MOVW       R30, R18
	ST         Z, R16
;4.c,138 :: 		for(j = 0; j < 4; j++)                                // ������ ���� ���������� � ��� �������
	MOV        R16, R20
	SUBI       R16, 255
; j end address is: 20 (R20)
; j start address is: 16 (R16)
;4.c,139 :: 		displayRAM[j] = volts[j];
	MOV        R20, R16
; j end address is: 16 (R16)
	JMP        L_get_volts224
L_get_volts225:
;4.c,141 :: 		}
L_end_get_volts2:
	POP        R2
	ADIW       R28, 9
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	POP        R29
	POP        R28
	RET
; end of _get_volts2

_out_display:

;4.c,143 :: 		void  out_display( )                                             // ������� ������ ���������� �� ���
;4.c,145 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;4.c,146 :: 		Lcd_Out(1, 4, "Key SB1: U1");
	LDI        R27, #lo_addr(?lstr3_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr3_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,147 :: 		Lcd_Out(2, 4, "U = ");
	LDI        R27, #lo_addr(?lstr4_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr4_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,148 :: 		Lcd_Chr_Cp(48 + displayRAM[0]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,149 :: 		Lcd_Chr_Cp('.');                                          // ����������� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _Lcd_Chr_CP+0
;4.c,150 :: 		Lcd_Chr_Cp(48 + displayRAM[1]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,151 :: 		Lcd_Chr_Cp(48 + displayRAM[2]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,152 :: 		Lcd_Chr_Cp(48 + displayRAM[3]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,153 :: 		Lcd_Out_Cp(" V");
	LDI        R27, #lo_addr(?lstr5_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr5_4+0)
	MOV        R3, R27
	CALL       _Lcd_Out_CP+0
;4.c,154 :: 		}
L_end_out_display:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_display

_out_display1:

;4.c,156 :: 		void  out_display1( )                                             // ������� ������ ���������� �� ���
;4.c,158 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;4.c,159 :: 		Lcd_Out(1, 4, "Key SB2: U2");
	LDI        R27, #lo_addr(?lstr6_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr6_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,160 :: 		Lcd_Out(2, 4, "U = ");
	LDI        R27, #lo_addr(?lstr7_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr7_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,161 :: 		Lcd_Chr_Cp(48 + displayRAM[0]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,162 :: 		Lcd_Chr_Cp('.');                                          // ����������� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _Lcd_Chr_CP+0
;4.c,163 :: 		Lcd_Chr_Cp(48 + displayRAM[1]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,164 :: 		Lcd_Chr_Cp(48 + displayRAM[2]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,165 :: 		Lcd_Chr_Cp(48 + displayRAM[3]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,166 :: 		Lcd_Out_Cp(" V");
	LDI        R27, #lo_addr(?lstr8_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr8_4+0)
	MOV        R3, R27
	CALL       _Lcd_Out_CP+0
;4.c,167 :: 		}
L_end_out_display1:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_display1

_out_display2:

;4.c,169 :: 		void  out_display2( )                                             // ������� ������ ���������� �� ���
;4.c,171 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;4.c,172 :: 		Lcd_Out(1, 4, "Key SB3: U3");
	LDI        R27, #lo_addr(?lstr9_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr9_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,173 :: 		Lcd_Out(2, 4, "U = ");
	LDI        R27, #lo_addr(?lstr10_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr10_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 2
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,174 :: 		Lcd_Chr_Cp(48 + displayRAM[0]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+0
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,175 :: 		Lcd_Chr_Cp('.');                                          // ����������� ���������� �����
	LDI        R27, 46
	MOV        R2, R27
	CALL       _Lcd_Chr_CP+0
;4.c,176 :: 		Lcd_Chr_Cp(48 + displayRAM[1]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+1
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,177 :: 		Lcd_Chr_Cp(48 + displayRAM[2]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+2
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,178 :: 		Lcd_Chr_Cp(48 + displayRAM[3]);                           // ����������� ����� � ���� ASCII
	LDS        R16, _displayRAM+3
	SUBI       R16, 208
	MOV        R2, R16
	CALL       _Lcd_Chr_CP+0
;4.c,179 :: 		Lcd_Out_Cp(" V");
	LDI        R27, #lo_addr(?lstr11_4+0)
	MOV        R2, R27
	LDI        R27, hi_addr(?lstr11_4+0)
	MOV        R3, R27
	CALL       _Lcd_Out_CP+0
;4.c,180 :: 		}
L_end_out_display2:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _out_display2

_error:

;4.c,181 :: 		void error ()
;4.c,183 :: 		Lcd_Cmd(_LCD_CLEAR);
	PUSH       R2
	PUSH       R3
	PUSH       R4
	PUSH       R5
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Cmd+0
;4.c,184 :: 		Lcd_Out(1, 4, "ERROR!");
	LDI        R27, #lo_addr(?lstr12_4+0)
	MOV        R4, R27
	LDI        R27, hi_addr(?lstr12_4+0)
	MOV        R5, R27
	LDI        R27, 4
	MOV        R3, R27
	LDI        R27, 1
	MOV        R2, R27
	CALL       _Lcd_Out+0
;4.c,185 :: 		Delay_ms(1000);
	LDI        R18, 41
	LDI        R17, 150
	LDI        R16, 128
L_error27:
	DEC        R16
	BRNE       L_error27
	DEC        R17
	BRNE       L_error27
	DEC        R18
	BRNE       L_error27
;4.c,186 :: 		}
L_end_error:
	POP        R5
	POP        R4
	POP        R3
	POP        R2
	RET
; end of _error
