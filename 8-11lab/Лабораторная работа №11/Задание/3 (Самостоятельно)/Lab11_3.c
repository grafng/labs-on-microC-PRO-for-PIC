/******************************************************************
lcd.c - ��������� ������ ������ �� ���
******************************************************************/
// ������������� ������� ���
sbit  LCD_RS  at  RC2_bit;
sbit  LCD_EN  at  RC3_bit;
sbit  LCD_D4  at  RC4_bit;
sbit  LCD_D5  at  RC5_bit;
sbit  LCD_D6  at  RC6_bit;
sbit  LCD_D7  at  RC7_bit;
          // ����������� �������
sbit  LCD_RS_Direction  at TRISC2_bit;
sbit  LCD_EN_Direction  at TRISC3_bit;
sbit  LCD_D4_Direction  at  TRISC4_bit;
sbit  LCD_D5_Direction  at  TRISC5_bit;
sbit  LCD_D6_Direction  at  TRISC6_bit;
sbit  LCD_D7_Direction  at  TRISC7_bit;

char  j;                                                      // ����������-�������
int  time = 50;                                        // �������� ��������� ��������

void  main( )
{
      TRISB = 0xFF;
      OPTION_REG.B7 = 0;      // ���������� � ����� � ����������
      Lcd_Init( );                                           // ������������� ���
      Lcd_Cmd(_LCD_CLEAR);                // �������� ���
      Lcd_Cmd(_LCD_CURSOR_OFF);    // ��������� ����������� �������
              // ����� �� ��� ������ �����������

      

      while(1){
               Lcd_Out(1, 2, "Press b1 or b2 ");          // ����� ������ � 1-� ������
             //  VDelay_ms(500);

               if (PORTB.B0 == 0 && PORTB.B1 != 0){
                  Lcd_Cmd(_LCD_CLEAR);
                  Lcd_Out(1, 2, "Button 1 pressed");          // ����� ������ � 1-� ������
                  VDelay_ms(time);
                  Lcd_Cmd(_LCD_CLEAR);
               }
               if (PORTB.B1 == 0 && PORTB.B0 != 0 ){
                  Lcd_Cmd(_LCD_CLEAR);
                  Lcd_Out(1, 2, "Button 2 pressed");          // ����� ������ � 1-� ������
                  VDelay_ms(time);
                  Lcd_Cmd(_LCD_CLEAR);
               }
               if (PORTB.B1 == 0 && PORTB.B0 == 0 ){
                  Lcd_Cmd(_LCD_CLEAR);
                  Lcd_Out(1, 2, "ERROR !!!");          // ����� ������ � 1-� ������
                  VDelay_ms(time);
                  Lcd_Cmd(_LCD_CLEAR);
               }
     }
 }