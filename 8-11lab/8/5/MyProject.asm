
_main:

;MyProject.c,25 :: 		void  main( )
;MyProject.c,27 :: 		Lcd_Init( );                                           // ������������� ���
	CALL       _Lcd_Init+0
;MyProject.c,28 :: 		Lcd_Cmd(_LCD_CLEAR);                // �������� ���
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,29 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);    // ��������� ����������� �������
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,31 :: 		Lcd_Out(1, 2, "Hello, world!");          // ����� ������ � 1-� ������
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,32 :: 		Lcd_Out(2, 2, "I am PIC16F877");     // ����� ������ �� 2-� ������
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,33 :: 		Delay_ms(2000);                                 // �������� �� ����� 2 �
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main0:
	DECFSZ     R13+0, 1
	GOTO       L_main0
	DECFSZ     R12+0, 1
	GOTO       L_main0
	DECFSZ     R11+0, 1
	GOTO       L_main0
	NOP
;MyProject.c,34 :: 		Lcd_Cmd(_LCD_CLEAR);                // �������� ���
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,36 :: 		Lcd_Out(1, 1, text1);                           // ����� ������ � 1-� ������
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _text1+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,37 :: 		Lcd_Out(2, 5, text2);                           // ����� ������ �� 2-� ������
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      5
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _text2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,38 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main1:
	DECFSZ     R13+0, 1
	GOTO       L_main1
	DECFSZ     R12+0, 1
	GOTO       L_main1
	DECFSZ     R11+0, 1
	GOTO       L_main1
	NOP
;MyProject.c,40 :: 		for(j = 0; j < 4; j++)
	CLRF       _j+0
L_main2:
	MOVLW      4
	SUBWF      _j+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;MyProject.c,42 :: 		Lcd_Cmd(_LCD_SHIFT_RIGHT);
	MOVLW      28
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,43 :: 		Vdelay_ms(time);                                        // �������� �� ����� 0,5 �
	MOVF       _time+0, 0
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVF       _time+1, 0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
;MyProject.c,40 :: 		for(j = 0; j < 4; j++)
	INCF       _j+0, 1
;MyProject.c,44 :: 		}
	GOTO       L_main2
L_main3:
;MyProject.c,46 :: 		while(1)
L_main5:
;MyProject.c,48 :: 		for(j = 0; j < 10; j++)
	CLRF       _j+0
L_main7:
	MOVLW      10
	SUBWF      _j+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main8
;MyProject.c,50 :: 		Lcd_Cmd(_LCD_SHIFT_LEFT);
	MOVLW      24
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,51 :: 		Vdelay_ms(time);                                 // �������� �� ����� 0,5 �
	MOVF       _time+0, 0
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVF       _time+1, 0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
;MyProject.c,48 :: 		for(j = 0; j < 10; j++)
	INCF       _j+0, 1
;MyProject.c,52 :: 		}
	GOTO       L_main7
L_main8:
;MyProject.c,53 :: 		for(j = 0; j < 10; j++)
	CLRF       _j+0
L_main10:
	MOVLW      10
	SUBWF      _j+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main11
;MyProject.c,55 :: 		Lcd_Cmd(_LCD_SHIFT_RIGHT);
	MOVLW      28
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,56 :: 		Vdelay_ms(time);                               // �������� �� ����� 0,5 �
	MOVF       _time+0, 0
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVF       _time+1, 0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
;MyProject.c,53 :: 		for(j = 0; j < 10; j++)
	INCF       _j+0, 1
;MyProject.c,57 :: 		}
	GOTO       L_main10
L_main11:
;MyProject.c,58 :: 		}
	GOTO       L_main5
;MyProject.c,59 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
