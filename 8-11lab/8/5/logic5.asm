
_main:

;logic5.c,4 :: 		void  main( )
;logic5.c,7 :: 		TRISB = 0xFF;
	MOVLW      255
	MOVWF      TRISB+0
;logic5.c,8 :: 		TRISC = 0;
	CLRF       TRISC+0
;logic5.c,9 :: 		PORTC = 0;
	CLRF       PORTC+0
;logic5.c,10 :: 		OPTION_REG.B7 = 0;
	BCF        OPTION_REG+0, 7
;logic5.c,11 :: 		while(1)
L_main0:
;logic5.c,13 :: 		Y = X1 || ((!(X2))&&(!(X1||X3)));
	BTFSC      RB1_bit+0, 1
	GOTO       L_main7
	BTFSC      RB2_bit+0, 2
	GOTO       L__main10
	BTFSC      RB1_bit+0, 1
	GOTO       L_main3
	BTFSC      RB3_bit+0, 3
	GOTO       L_main3
	CLRF       R0+0
	GOTO       L_main2
L_main3:
	MOVLW      1
	MOVWF      R0+0
L_main2:
	MOVF       R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main10
	GOTO       L_main7
L__main10:
	CLRF       R0+0
	GOTO       L_main6
L_main7:
	MOVLW      1
	MOVWF      R0+0
L_main6:
	BTFSC      R0+0, 0
	GOTO       L__main11
	BCF        main_Y_L0+0, BitPos(main_Y_L0+0)
	GOTO       L__main12
L__main11:
	BSF        main_Y_L0+0, BitPos(main_Y_L0+0)
L__main12:
;logic5.c,14 :: 		if( Y == 1 )
	BTFSS      main_Y_L0+0, BitPos(main_Y_L0+0)
	GOTO       L_main8
;logic5.c,17 :: 		PORTC.B0 = 1;
	BSF        PORTC+0, 0
;logic5.c,18 :: 		PORTC.B7 = 0;
	BCF        PORTC+0, 7
;logic5.c,19 :: 		}
	GOTO       L_main9
L_main8:
;logic5.c,23 :: 		PORTC.B0 = 0;
	BCF        PORTC+0, 0
;logic5.c,24 :: 		PORTC.B7 = 1;
	BSF        PORTC+0, 7
;logic5.c,25 :: 		}
L_main9:
;logic5.c,26 :: 		}
	GOTO       L_main0
;logic5.c,27 :: 		}
	GOTO       $+0
; end of _main
