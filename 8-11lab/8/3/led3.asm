
_main:

;led3.c,5 :: 		void  main( )
;led3.c,7 :: 		TRISB2_bit = 1;                     // ��������� 2-� �����  ����� � �� ����
	BSF        TRISB2_bit+0, BitPos(TRISB2_bit+0)
;led3.c,8 :: 		TRISC2_bit = 0;                     // ��������� 2-� �����  ����� � �� ��-���
	BCF        TRISC2_bit+0, BitPos(TRISC2_bit+0)
;led3.c,9 :: 		RC2_bit = 1;                           // �������� ���������
	BSF        RC2_bit+0, BitPos(RC2_bit+0)
;led3.c,10 :: 		OPTION_REG.B7 = 0;          // ���������� ���������� �������������
	BCF        OPTION_REG+0, 7
;led3.c,12 :: 		while(1)                                  // ����������� ���� ����������
L_main0:
;led3.c,14 :: 		while(RB2_bit == 1);        // �������� ��������� �������� SB1
L_main2:
	BTFSS      RB2_bit+0, BitPos(RB2_bit+0)
	GOTO       L_main3
	GOTO       L_main2
L_main3:
;led3.c,15 :: 		RC2_bit = 0;
	BCF        RC2_bit+0, BitPos(RC2_bit+0)
;led3.c,16 :: 		Delay_ms(5000);        // �������� ���������� �������� SB1
	MOVLW      51
	MOVWF      R11+0
	MOVLW      187
	MOVWF      R12+0
	MOVLW      223
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;led3.c,17 :: 		RC2_bit = 1;                // �������� ���������
	BSF        RC2_bit+0, BitPos(RC2_bit+0)
;led3.c,18 :: 		}
	GOTO       L_main0
;led3.c,19 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
