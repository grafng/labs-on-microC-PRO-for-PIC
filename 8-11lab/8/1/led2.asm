
_main:

;led2.c,2 :: 		void  main( )
;led2.c,4 :: 		TRISC.B0 = 0;
	BCF        TRISC+0, 0
;led2.c,5 :: 		PORTC.B0 = 0;
	BCF        PORTC+0, 0
;led2.c,6 :: 		for(i=0;i<4;i++)
	CLRF       _i+0
L_main0:
	MOVLW      4
	SUBWF      _i+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main1
;led2.c,8 :: 		PORTC.B0 = 1;
	BSF        PORTC+0, 0
;led2.c,9 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;led2.c,10 :: 		PORTC.B0 = 0;
	BCF        PORTC+0, 0
;led2.c,11 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
;led2.c,6 :: 		for(i=0;i<4;i++)
	INCF       _i+0, 1
;led2.c,12 :: 		}
	GOTO       L_main0
L_main1:
;led2.c,13 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
