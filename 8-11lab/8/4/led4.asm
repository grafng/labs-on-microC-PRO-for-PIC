
_main:

;led4.c,8 :: 		void  main( )
;led4.c,10 :: 		TRISB2_bit = 1;                     // ��������� 2-� �����  ����� � �� ����
	BSF        TRISB2_bit+0, BitPos(TRISB2_bit+0)
;led4.c,11 :: 		TRISC2_bit = 0;                     // ��������� 2-� �����  ����� � �� ��-���
	BCF        TRISC2_bit+0, BitPos(TRISC2_bit+0)
;led4.c,12 :: 		RC2_bit = 0;                           // �������� ���������
	BCF        RC2_bit+0, BitPos(RC2_bit+0)
;led4.c,13 :: 		OPTION_REG.B7 = 0;          // ���������� ���������� �������������
	BCF        OPTION_REG+0, 7
;led4.c,15 :: 		while(1)                                  // ����������� ���� ����������
L_main0:
;led4.c,17 :: 		while(RB2_bit == 0)        // �������� ��������� �������� SB1
L_main2:
	BTFSC      RB2_bit+0, BitPos(RB2_bit+0)
	GOTO       L_main3
;led4.c,19 :: 		RC2_bit = 1;
	BSF        RC2_bit+0, BitPos(RC2_bit+0)
;led4.c,20 :: 		Delay_ms(500);        // �������� ���������� �������� SB1
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;led4.c,21 :: 		RC2_bit = 0;
	BCF        RC2_bit+0, BitPos(RC2_bit+0)
;led4.c,22 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;led4.c,23 :: 		}
	GOTO       L_main2
L_main3:
;led4.c,24 :: 		}
	GOTO       L_main0
;led4.c,25 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
