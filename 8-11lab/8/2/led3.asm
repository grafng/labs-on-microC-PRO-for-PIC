
_main:

;led3.c,6 :: 		void  main( )
;led3.c,8 :: 		TRISC.B0 = 0;
	BCF        TRISC+0, 0
;led3.c,9 :: 		PORTC.B0 = 0;
	BCF        PORTC+0, 0
;led3.c,10 :: 		while (1)
L_main0:
;led3.c,12 :: 		while (i>0)
L_main2:
	MOVF       _i+0, 0
	SUBLW      0
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;led3.c,14 :: 		PORTC.B0 = 1;
	BSF        PORTC+0, 0
;led3.c,15 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;led3.c,16 :: 		PORTC.B0 = 0;
	BCF        PORTC+0, 0
;led3.c,17 :: 		Delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;led3.c,18 :: 		i--;
	DECF       _i+0, 1
;led3.c,19 :: 		}
	GOTO       L_main2
L_main3:
;led3.c,20 :: 		PORTC.B0 = 1;
	BSF        PORTC+0, 0
;led3.c,21 :: 		}
	GOTO       L_main0
;led3.c,22 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
