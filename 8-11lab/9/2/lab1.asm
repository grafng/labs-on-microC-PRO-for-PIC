
_main:

;lab1.c,6 :: 		void  main( )
;lab1.c,8 :: 		unsigned  int  time = 500;    // ����������, ������������ ��������
;lab1.c,12 :: 		init( );                                  // ����� ������� �������������
	CALL       _init+0
;lab1.c,13 :: 		while(1)                              // ����������� ���� ����� ������ �
L_main0:
;lab1.c,16 :: 		in_portB = PORTB;              // ���� ������ � ����� ����� �
	MOVF       PORTB+0, 0
	MOVWF      main_in_portB_L0+0
;lab1.c,17 :: 		switch( in_portB )
	GOTO       L_main2
;lab1.c,19 :: 		case  0b11111101 :        // ���� ������� ������� SB1
L_main4:
;lab1.c,20 :: 		meandr1();           // ����� ������� ������ �����
	CALL       _meandr1+0
;lab1.c,21 :: 		PORTC=128;
	MOVLW      128
	MOVWF      PORTC+0
;lab1.c,22 :: 		break;
	GOTO       L_main3
;lab1.c,23 :: 		case  0b11111011 :        // ���� ������� ������� SB2
L_main5:
;lab1.c,24 :: 		meandr10();         // ����� ������� ������ ������
	CALL       _meandr10+0
;lab1.c,25 :: 		PORTC=128;
	MOVLW      128
	MOVWF      PORTC+0
;lab1.c,26 :: 		break;
	GOTO       L_main3
;lab1.c,27 :: 		case  0b11110111 :        // ���� ������� ������� SB3
L_main6:
;lab1.c,28 :: 		meandr100();                 // ����� ������� "��������"
	CALL       _meandr100+0
;lab1.c,29 :: 		PORTC=128;
	MOVLW      128
	MOVWF      PORTC+0
;lab1.c,30 :: 		break;
	GOTO       L_main3
;lab1.c,31 :: 		case  0b11101111 :        // ���� ������� ������� SB3
L_main7:
;lab1.c,32 :: 		meandr1000();                // ����� ������� "��������"
	CALL       _meandr1000+0
;lab1.c,33 :: 		PORTC=128;
	MOVLW      128
	MOVWF      PORTC+0
;lab1.c,34 :: 		break;
	GOTO       L_main3
;lab1.c,35 :: 		default:                          // ���� ��� �������� ����������
L_main8:
;lab1.c,36 :: 		break;        // ������� ���� �����������
	GOTO       L_main3
;lab1.c,37 :: 		}
L_main2:
	MOVF       main_in_portB_L0+0, 0
	XORLW      253
	BTFSC      STATUS+0, 2
	GOTO       L_main4
	MOVF       main_in_portB_L0+0, 0
	XORLW      251
	BTFSC      STATUS+0, 2
	GOTO       L_main5
	MOVF       main_in_portB_L0+0, 0
	XORLW      247
	BTFSC      STATUS+0, 2
	GOTO       L_main6
	MOVF       main_in_portB_L0+0, 0
	XORLW      239
	BTFSC      STATUS+0, 2
	GOTO       L_main7
	GOTO       L_main8
L_main3:
;lab1.c,38 :: 		}
	GOTO       L_main0
;lab1.c,39 :: 		}
L_end_main:
	GOTO       $+0
; end of _main

_init:

;lab1.c,41 :: 		void  init(  )                                    // ������� �������������
;lab1.c,43 :: 		TRISB = 0xFF;                   // ��������� ��� ����� ����� � �� ����
	MOVLW      255
	MOVWF      TRISB+0
;lab1.c,44 :: 		TRISC = 0;                          // ��������� ��� ����� ����� � �� �����
	CLRF       TRISC+0
;lab1.c,45 :: 		PORTC = 0;                        // �������� ��� ����������
	CLRF       PORTC+0
;lab1.c,46 :: 		OPTION_REG.B7 = 0;      // ���������� � ������ ����� �
	BCF        OPTION_REG+0, 7
;lab1.c,48 :: 		}
L_end_init:
	RETURN
; end of _init

_meandr1:

;lab1.c,49 :: 		void meandr1( )
;lab1.c,51 :: 		while (PORTB.B1==0)
L_meandr19:
	BTFSC      PORTB+0, 1
	GOTO       L_meandr110
;lab1.c,53 :: 		PORTC=66;
	MOVLW      66
	MOVWF      PORTC+0
;lab1.c,54 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_meandr111:
	DECFSZ     R13+0, 1
	GOTO       L_meandr111
	DECFSZ     R12+0, 1
	GOTO       L_meandr111
	DECFSZ     R11+0, 1
	GOTO       L_meandr111
	NOP
	NOP
;lab1.c,55 :: 		PORTC=64;
	MOVLW      64
	MOVWF      PORTC+0
;lab1.c,56 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_meandr112:
	DECFSZ     R13+0, 1
	GOTO       L_meandr112
	DECFSZ     R12+0, 1
	GOTO       L_meandr112
	DECFSZ     R11+0, 1
	GOTO       L_meandr112
	NOP
	NOP
;lab1.c,57 :: 		}
	GOTO       L_meandr19
L_meandr110:
;lab1.c,58 :: 		}
L_end_meandr1:
	RETURN
; end of _meandr1

_meandr10:

;lab1.c,59 :: 		void meandr10( )
;lab1.c,61 :: 		PORTC=4+64;
	MOVLW      68
	MOVWF      PORTC+0
;lab1.c,62 :: 		Delay_ms(50);
	MOVLW      130
	MOVWF      R12+0
	MOVLW      221
	MOVWF      R13+0
L_meandr1013:
	DECFSZ     R13+0, 1
	GOTO       L_meandr1013
	DECFSZ     R12+0, 1
	GOTO       L_meandr1013
	NOP
	NOP
;lab1.c,63 :: 		PORTC=64;
	MOVLW      64
	MOVWF      PORTC+0
;lab1.c,64 :: 		Delay_ms(50);
	MOVLW      130
	MOVWF      R12+0
	MOVLW      221
	MOVWF      R13+0
L_meandr1014:
	DECFSZ     R13+0, 1
	GOTO       L_meandr1014
	DECFSZ     R12+0, 1
	GOTO       L_meandr1014
	NOP
	NOP
;lab1.c,65 :: 		}
L_end_meandr10:
	RETURN
; end of _meandr10

_meandr100:

;lab1.c,66 :: 		void meandr100( )
;lab1.c,68 :: 		PORTC=8+64;
	MOVLW      72
	MOVWF      PORTC+0
;lab1.c,69 :: 		Delay_ms(5);
	MOVLW      13
	MOVWF      R12+0
	MOVLW      251
	MOVWF      R13+0
L_meandr10015:
	DECFSZ     R13+0, 1
	GOTO       L_meandr10015
	DECFSZ     R12+0, 1
	GOTO       L_meandr10015
	NOP
	NOP
;lab1.c,70 :: 		PORTC=64;
	MOVLW      64
	MOVWF      PORTC+0
;lab1.c,71 :: 		Delay_ms(5);
	MOVLW      13
	MOVWF      R12+0
	MOVLW      251
	MOVWF      R13+0
L_meandr10016:
	DECFSZ     R13+0, 1
	GOTO       L_meandr10016
	DECFSZ     R12+0, 1
	GOTO       L_meandr10016
	NOP
	NOP
;lab1.c,72 :: 		}
L_end_meandr100:
	RETURN
; end of _meandr100

_meandr1000:

;lab1.c,73 :: 		void meandr1000( )
;lab1.c,75 :: 		PORTC=80;
	MOVLW      80
	MOVWF      PORTC+0
;lab1.c,76 :: 		Delay_us(500);
	MOVLW      2
	MOVWF      R12+0
	MOVLW      75
	MOVWF      R13+0
L_meandr100017:
	DECFSZ     R13+0, 1
	GOTO       L_meandr100017
	DECFSZ     R12+0, 1
	GOTO       L_meandr100017
;lab1.c,77 :: 		PORTC=64;
	MOVLW      64
	MOVWF      PORTC+0
;lab1.c,78 :: 		Delay_us(500);
	MOVLW      2
	MOVWF      R12+0
	MOVLW      75
	MOVWF      R13+0
L_meandr100018:
	DECFSZ     R13+0, 1
	GOTO       L_meandr100018
	DECFSZ     R12+0, 1
	GOTO       L_meandr100018
;lab1.c,79 :: 		}
L_end_meandr1000:
	RETURN
; end of _meandr1000
