
_main:

;lab1.c,3 :: 		void  main( )
;lab1.c,5 :: 		TRISC = 0;                          // ��������� ��� ����� ����� � �� �����
	CLRF       TRISC+0
;lab1.c,6 :: 		PORTC = 0;                         // �������� ��� ����������
	CLRF       PORTC+0
;lab1.c,7 :: 		while(1)
L_main0:
;lab1.c,8 :: 		shift_right( );                    // ����� ������� shift_left( )
	CALL       _shift_right+0
	GOTO       L_main0
;lab1.c,9 :: 		}
L_end_main:
	GOTO       $+0
; end of _main

_shift_right:

;lab1.c,11 :: 		void  shift_right( )                       // ������� ������ �����
;lab1.c,13 :: 		PORTC.B7 = 1;
	BSF        PORTC+0, 7
;lab1.c,14 :: 		Vdelay_ms(time);
	MOVF       _time+0, 0
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVF       _time+1, 0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
;lab1.c,15 :: 		PORTC.B7 = 0;
	BCF        PORTC+0, 7
;lab1.c,16 :: 		PORTC.B3 = 1;
	BSF        PORTC+0, 3
;lab1.c,17 :: 		Vdelay_ms(time);
	MOVF       _time+0, 0
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVF       _time+1, 0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
;lab1.c,18 :: 		PORTC.B3 = 0;
	BCF        PORTC+0, 3
;lab1.c,19 :: 		PORTC.B0 = 1;
	BSF        PORTC+0, 0
;lab1.c,20 :: 		Vdelay_ms(time);
	MOVF       _time+0, 0
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVF       _time+1, 0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
;lab1.c,21 :: 		PORTC.B0 = 0;
	BCF        PORTC+0, 0
;lab1.c,23 :: 		}
L_end_shift_right:
	RETURN
; end of _shift_right
