
_main:

;MyProject.c,25 :: 		void  main( )
;MyProject.c,27 :: 		Lcd_Init( );                                           // ������������� ���
	CALL       _Lcd_Init+0
;MyProject.c,28 :: 		Lcd_Cmd(_LCD_CLEAR);                // �������� ���
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,29 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);    // ��������� ����������� �������
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,32 :: 		while (1)
L_main0:
;MyProject.c,34 :: 		Lcd_Out(1, 16, "IDE mikroC PRO for PIC");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      16
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,35 :: 		for( j=0;j<16;j++)
	CLRF       _j+0
L_main2:
	MOVLW      16
	SUBWF      _j+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;MyProject.c,37 :: 		Lcd_Cmd(_LCD_SHIFT_LEFT);
	MOVLW      24
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,38 :: 		delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;MyProject.c,35 :: 		for( j=0;j<16;j++)
	INCF       _j+0, 1
;MyProject.c,39 :: 		}
	GOTO       L_main2
L_main3:
;MyProject.c,40 :: 		}
	GOTO       L_main0
;MyProject.c,42 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
