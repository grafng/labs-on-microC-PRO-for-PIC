
_main:

;MyProject.c,22 :: 		void  main( )
;MyProject.c,24 :: 		TRISB = 0;
	CLRF       TRISB+0
;MyProject.c,25 :: 		OPTION_REG.B0=0;
	BCF        OPTION_REG+0, 0
;MyProject.c,26 :: 		OPTION_REG.B1=0;
	BCF        OPTION_REG+0, 1
;MyProject.c,27 :: 		Lcd_Init( );                                           // ������������� ���
	CALL       _Lcd_Init+0
;MyProject.c,28 :: 		Lcd_Cmd(_LCD_CLEAR);                // �������� ���
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,29 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);    // ��������� ����������� �������
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,32 :: 		while (1)
L_main0:
;MyProject.c,34 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,35 :: 		if(PORTB.B0==1&&PORTB.B1==1)
	BTFSS      PORTB+0, 0
	GOTO       L_main4
	BTFSS      PORTB+0, 1
	GOTO       L_main4
L__main18:
;MyProject.c,37 :: 		Lcd_Out(1, 1, "Press only one");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,38 :: 		Lcd_Out(2, 1, "key: SB1 or SB2");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,39 :: 		}
L_main4:
;MyProject.c,40 :: 		if(PORTB.B0==0&&PORTB.B1==1)
	BTFSC      PORTB+0, 0
	GOTO       L_main7
	BTFSS      PORTB+0, 1
	GOTO       L_main7
L__main17:
;MyProject.c,42 :: 		Lcd_Out(1, 1, "Key SB1");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,43 :: 		Lcd_Out(2, 1, "is pressed");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,45 :: 		}
L_main7:
;MyProject.c,46 :: 		if(PORTB.B0==0&&PORTB.B1==0)
	BTFSC      PORTB+0, 0
	GOTO       L_main10
	BTFSC      PORTB+0, 1
	GOTO       L_main10
L__main16:
;MyProject.c,48 :: 		Lcd_Out(1, 1, "Error! Two keys");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,49 :: 		Lcd_Out(2, 1, "are pressed");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,50 :: 		}
L_main10:
;MyProject.c,51 :: 		if(PORTB.B0==1&&PORTB.B1==0)
	BTFSS      PORTB+0, 0
	GOTO       L_main13
	BTFSC      PORTB+0, 1
	GOTO       L_main13
L__main15:
;MyProject.c,53 :: 		Lcd_Out(1, 1, "Key SB2");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,54 :: 		Lcd_Out(2, 1, "is pressed");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,55 :: 		}
L_main13:
;MyProject.c,56 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	NOP
;MyProject.c,57 :: 		}
	GOTO       L_main0
;MyProject.c,59 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
