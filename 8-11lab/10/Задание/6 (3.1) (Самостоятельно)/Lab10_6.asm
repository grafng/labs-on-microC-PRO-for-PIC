
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

	INCF       _counter+0, 1
	MOVF       _counter+0, 0
	XORLW      100
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt0
	MOVLW      1
	MOVWF      _flag_T+0
	CLRF       _counter+0
L_interrupt0:
	MOVLW      178
	MOVWF      TMR0+0
	BCF        T0IF_bit+0, BitPos(T0IF_bit+0)
L_end_interrupt:
L__interrupt6:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

	BCF        TRISC+0, 0
	MOVLW      7
	MOVWF      OPTION_REG+0
	MOVLW      178
	MOVWF      TMR0+0
	BSF        INTCON+0, 5
	BSF        INTCON+0, 7
L_main1:
	MOVF       _flag_T+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main3
	MOVLW      1
	XORWF      PORTC+0, 1
	CLRF       _flag_T+0
	INCF       _end+0, 1
L_main3:
	MOVF       _end+0, 0
	XORLW      10
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BCF        INTCON+0, 7
	BSF        PORTC+0, 0
L_main4:
	GOTO       L_main1
L_end_main:
	GOTO       $+0
; end of _main
