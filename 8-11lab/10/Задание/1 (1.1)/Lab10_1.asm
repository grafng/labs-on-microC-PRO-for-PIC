
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

	MOVLW      1
	XORWF      PORTC+0, 1
	BCF        INTF_bit+0, BitPos(INTF_bit+0)
L_end_interrupt:
L__interrupt3:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

	BSF        TRISB+0, 0
	BCF        TRISC+0, 0
	BCF        PORTC+0, 0
	BCF        OPTION_REG+0, 7
	BCF        OPTION_REG+0, 6
	BSF        INTCON+0, 4
	BSF        INTCON+0, 7
L_main0:
	GOTO       L_main0
L_end_main:
	GOTO       $+0
; end of _main
