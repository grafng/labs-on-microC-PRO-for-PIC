
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Lab10_4.c,6 :: 		void  interrupt(){                      // �������-���������� ���������� �� TMR0
;Lab10_4.c,7 :: 		PORTC.B0 ^= 1;                    // ������������� ������ �� ������ RC1
	MOVLW      1
	XORWF      PORTC+0, 1
;Lab10_4.c,8 :: 		TMR0 = 6;                         // ������������� ������
	MOVLW      6
	MOVWF      TMR0+0
;Lab10_4.c,9 :: 		T0IF_bit = 0;                     // �������� ���� ���������� T0IF
	BCF        T0IF_bit+0, BitPos(T0IF_bit+0)
;Lab10_4.c,10 :: 		}
L_end_interrupt:
L__interrupt3:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;Lab10_4.c,11 :: 		void  main(){
;Lab10_4.c,12 :: 		TRISC.B0 = 0;                     // ��������� ����� RC1 �� �����
	BCF        TRISC+0, 0
;Lab10_4.c,13 :: 		OPTION_REG = 0x03;                // ������� ����������� 16 ������������
	MOVLW      3
	MOVWF      OPTION_REG+0
;Lab10_4.c,14 :: 		TMR0 = 3;                         // ���������� ������������ ������ 2 ��
	MOVLW      3
	MOVWF      TMR0+0
;Lab10_4.c,15 :: 		INTCON.B5 = 1;                    // ��������� ���������� �� TMR0
	BSF        INTCON+0, 5
;Lab10_4.c,16 :: 		INTCON.B7 = 1;                    // ����� ���������� ����������
	BSF        INTCON+0, 7
;Lab10_4.c,17 :: 		while(1);                         // ������������ ���������
L_main0:
	GOTO       L_main0
;Lab10_4.c,18 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
