
_main:

;Lab10_8.c,6 :: 		void  main( )
;Lab10_8.c,8 :: 		TRISC = 0;                       // ��������� ����� RC1 �� �����
	CLRF       TRISC+0
;Lab10_8.c,9 :: 		PORTC=0;
	CLRF       PORTC+0
;Lab10_8.c,11 :: 		while( 1 )
L_main0:
;Lab10_8.c,13 :: 		for(i=1;i<501;i++)
	MOVLW      1
	MOVWF      _i+0
	MOVLW      0
	MOVWF      _i+1
L_main2:
	MOVLW      128
	XORWF      _i+1, 0
	MOVWF      R0+0
	MOVLW      128
	XORLW      1
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main10
	MOVLW      245
	SUBWF      _i+0, 0
L__main10:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;Lab10_8.c,15 :: 		if(i%10==0)
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _i+0, 0
	MOVWF      R0+0
	MOVF       _i+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      0
	XORWF      R0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main11
	MOVLW      0
	XORWF      R0+0, 0
L__main11:
	BTFSS      STATUS+0, 2
	GOTO       L_main5
;Lab10_8.c,16 :: 		PORTC.B0 ^= 1;
	MOVLW      1
	XORWF      PORTC+0, 1
L_main5:
;Lab10_8.c,17 :: 		if(i%25==0)
	MOVLW      25
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _i+0, 0
	MOVWF      R0+0
	MOVF       _i+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      0
	XORWF      R0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main12
	MOVLW      0
	XORWF      R0+0, 0
L__main12:
	BTFSS      STATUS+0, 2
	GOTO       L_main6
;Lab10_8.c,18 :: 		PORTC.B2 ^= 1;
	MOVLW      4
	XORWF      PORTC+0, 1
L_main6:
;Lab10_8.c,19 :: 		if(i%100==0)
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _i+0, 0
	MOVWF      R0+0
	MOVF       _i+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      0
	XORWF      R0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main13
	MOVLW      0
	XORWF      R0+0, 0
L__main13:
	BTFSS      STATUS+0, 2
	GOTO       L_main7
;Lab10_8.c,20 :: 		PORTC.B4 ^= 1;
	MOVLW      16
	XORWF      PORTC+0, 1
L_main7:
;Lab10_8.c,21 :: 		if(i%500==0)
	MOVLW      244
	MOVWF      R4+0
	MOVLW      1
	MOVWF      R4+1
	MOVF       _i+0, 0
	MOVWF      R0+0
	MOVF       _i+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      0
	XORWF      R0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main14
	MOVLW      0
	XORWF      R0+0, 0
L__main14:
	BTFSS      STATUS+0, 2
	GOTO       L_main8
;Lab10_8.c,22 :: 		PORTC.B6 ^= 1;
	MOVLW      64
	XORWF      PORTC+0, 1
L_main8:
;Lab10_8.c,13 :: 		for(i=1;i<501;i++)
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;Lab10_8.c,23 :: 		}
	GOTO       L_main2
L_main3:
;Lab10_8.c,24 :: 		}                              // ������������ ���������
	GOTO       L_main0
;Lab10_8.c,25 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
