
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

	BTFSS      T0IF_bit+0, BitPos(T0IF_bit+0)
	GOTO       L_interrupt0
	INCF       _counter+0, 1
	MOVF       _counter+0, 0
	XORLW      100
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
	MOVLW      1
	MOVWF      _flag_T+0
	CLRF       _counter+0
L_interrupt1:
	MOVLW      178
	MOVWF      TMR0+0
	BCF        T0IF_bit+0, BitPos(T0IF_bit+0)
L_interrupt0:
	BTFSS      INTF_bit+0, BitPos(INTF_bit+0)
	GOTO       L_interrupt2
	MOVLW      128
	XORWF      PORTC+0, 1
	BCF        INTF_bit+0, BitPos(INTF_bit+0)
L_interrupt2:
L_end_interrupt:
L__interrupt7:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

	BSF        TRISB+0, 0
	CLRF       TRISC+0
	CLRF       PORTC+0
	MOVLW      7
	MOVWF      OPTION_REG+0
	MOVLW      178
	MOVWF      TMR0+0
	MOVLW      176
	MOVWF      INTCON+0
L_main3:
	MOVF       _flag_T+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main5
	MOVLW      1
	XORWF      PORTC+0, 1
	CLRF       _flag_T+0
L_main5:
	GOTO       L_main3
L_end_main:
	GOTO       $+0
; end of _main
