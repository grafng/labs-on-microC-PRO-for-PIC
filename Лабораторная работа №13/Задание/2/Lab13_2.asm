
_main:

	MOVLW      127
	MOVWF      main_dc_L0+0
	MOVLW      255
	MOVWF      TRISB+0
	CLRF       TRISC+0
	BCF        OPTION_REG+0, 7
	BSF        T2CON+0, 0
	BCF        T2CON+0, 1
	MOVLW      99
	MOVWF      PR2+0
	CALL       _PWM1_Init+0
	CALL       _PWM1_Start+0
	MOVF       main_dc_L0+0, 0
	MOVWF      FARG_PWM1_Set_Duty_new_duty+0
	CALL       _PWM1_Set_Duty+0
L_main0:
	BTFSC      RB0_bit+0, BitPos(RB0_bit+0)
	GOTO       L_main2
	MOVLW      52
	MOVWF      R12+0
	MOVLW      241
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	NOP
	NOP
	INCF       main_dc_L0+0, 1
	MOVF       main_dc_L0+0, 0
	MOVWF      FARG_PWM1_Set_Duty_new_duty+0
	CALL       _PWM1_Set_Duty+0
L_main2:
	BTFSC      RB1_bit+0, BitPos(RB1_bit+0)
	GOTO       L_main4
	MOVLW      52
	MOVWF      R12+0
	MOVLW      241
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	NOP
	NOP
	DECF       main_dc_L0+0, 1
	MOVF       main_dc_L0+0, 0
	MOVWF      FARG_PWM1_Set_Duty_new_duty+0
	CALL       _PWM1_Set_Duty+0
L_main4:
	GOTO       L_main0
L_end_main:
	GOTO       $+0
; end of _main
