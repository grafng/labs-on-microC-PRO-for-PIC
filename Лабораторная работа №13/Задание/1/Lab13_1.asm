
_main:

	MOVLW      127
	MOVWF      main_dc_L0+0
	CLRF       TRISC+0
	BSF        T2CON+0, 0
	BCF        T2CON+0, 1
	MOVLW      99
	MOVWF      PR2+0
	CALL       _PWM1_Init+0
	CALL       _PWM1_Start+0
	MOVF       main_dc_L0+0, 0
	MOVWF      FARG_PWM1_Set_Duty_new_duty+0
	CALL       _PWM1_Set_Duty+0
L_end_main:
	GOTO       $+0
; end of _main
